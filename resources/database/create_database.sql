-- db version 0.2.0
-- do NOT alter the format of the string above, only change the version num
-- don't forget to update the insert statement at the end of this file

PRAGMA page_size = 4096;
PRAGMA automatic_index = OFF;

----------tables
create table if not exists nicks (
    id integer primary key,
    nickname text unique
);

create table if not exists users (
    id integer primary key
);

create table if not exists users_nicks (
    id integer primary key,
    user_id integer not null references users(id),
    nick_id integer not null references nicks(id),
    unique (user_id, nick_id) on conflict ignore
);

create table if not exists room_names (
    id integer primary key,
    name text unique
);

create table if not exists messages (
    msgid integer primary key,
    msg text
);

create table if not exists msgtypes (
    id integer primary key,
    type text not null
);

create table if not exists keys_derivations (
    id integer primary key,
    the_key blob unique not null
);

create table if not exists private_events (
    msgid integer primary key,
    key_derivation integer references keys_derivations(id),
    data blob
);

create table if not exists events (
    id integer primary key,
    type integer references msgtypes(id),
    user_id integer references users(id),
    room_id integer references room_names(id),
    msgid integer references messages(id),
    nick integer references nicks(id),
    time integer
);

-------------extended information
create table if not exists status_text (
    id integer primary key,
    status text unique
);

create table if not exists clans (
    id integer primary key,
    clan_name text unique
);

create table if not exists skins (
    id integer primary key
);

create table if not exists users_skins (
    id integer primary key,
    user_id integer references users(id),
    skin_id integer references skins(id),
    unique (user_id, skin_id) on conflict ignore
);

create table if not exists users_data (
    id integer primary key,
    user_id integer references users(id),
    nick_id integer references nicks(id),
    profile_text_id integer references status_text(id),
    picture_id integer references skins(id),
    exp integer,
    clan integer references clans(id),
    coins integer,
    time integer
);

----------misc
create table if not exists db_version (
    id integer primary key,
    major integer,
    minor integer,
    patch integer
);

----------views
create view if not exists legible_users_data as
    select users_data.id,
           users_data.user_id,
           nickname,
           status_text.status as profile_text,
           picture_id as profile_pic,
           exp,
           clan_name,
           coins,
           datetime(cast(users_data.time as text),"unixepoch", "localtime") as date_time
        from users_data
        inner join nicks on nicks.id = users_data.nick_id
        inner join status_text on status_text.id = users_data.profile_text_id
        inner join clans on clans.id = users_data.clan;


create view if not exists legible_users_nicks as
    select users_nicks.nick_id,
           user_id,
           nickname
        from users_nicks
        inner join nicks on nicks.id = users_nicks.nick_id;

create view if not exists first_events as
    select events.id as min_id,
           date(cast(events.time as text),"unixepoch", "localtime") as the_date
        from events
        group by the_date having min(events.id);

create view if not exists legible_events as
    select events.id,
           datetime(cast(events.time as text),"unixepoch", "localtime") as date_time,
           room_names.name as room_name,
           msgtypes.type,
           nicks.nickname,
           events.user_id as user_id,
           messages.msg
        from events
        inner join msgtypes on msgtypes.id = events.type
        inner join nicks on events.nick = nicks.id
        inner join room_names on room_names.id = events.room_id
        inner join messages on events.msgid = messages.msgid;

create view if not exists legible_events_no_room as
    select events.id,
           datetime(cast(events.time as text),"unixepoch", "localtime") as date_time,
           msgtypes.type,
           nicks.nickname,
           events.user_id,
           messages.msg
        from events
        inner join msgtypes on msgtypes.id = events.type
        inner join nicks on events.nick = nicks.id
        inner join messages on events.msgid = messages.msgid;

insert or ignore into msgtypes (id, type) values (0,'null'), (1, 'enter'), (2, 'leave'), (3, 'chat'),
(4, 'clan'), (5, 'self_enter'), (6, 'self_leave');
insert or ignore into users (id) values (-1);
insert or ignore into nicks (id, nickname) values (0, null);
insert or ignore into messages (msgid, msg) values (0, null);
insert or ignore into events (id, type, user_id, room_id, msgid, nick, time) values (0,0,-1,0,0,0,1466899200);
insert or replace into db_version (id, major, minor, patch) values (0, 0, 2, 0)