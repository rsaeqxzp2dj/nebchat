op = {
    'insert_msg':
        "INSERT INTO messages VALUES(NULL, ?)",

    'insert_room':
        'INSERT INTO room_names VALUES(NULL, ?)',

    'get_room_id':
        'SELECT id from room_names where name = ?',

    'insert_nick':
        'INSERT OR IGNORE INTO nicks VALUES(NULL, ?)',

    'get_nick_id':
        'SELECT id FROM nicks WHERE nickname = ?',

    'get_key_deriv':
        'SELECT id FROM keys_derivations where the_key = ?',

    'insert_uid':
        'INSERT OR IGNORE INTO users VALUES(?)',

    'associate_nick_id':  # (user_id, nick_id)
        'INSERT OR IGNORE INTO users_nicks VALUES(NULL, ?, ?)',

    'insert_event':  # (msgtype, uid, roomid, msgid, nickid, time)
        'INSERT INTO events VALUES(NULL, ?, ?, ?, ?, ?, ?)',

    'insert_private_event':  # (deriv_key, encrypted_bytes)
        'INSERT INTO private_events VALUES(NULL, ?, ?)',

    'insert_key_deriv':
        'INSERT OR IGNORE INTO keys_derivations VALUES(NULL, ?)',

    'associate_skin_id':  # (user_id, skin_id)
        'INSERT OR IGNORE INTO users_skins VALUES (NULL, ?, ?)',

    'insert_skin':  # (skin_id)
        'INSERT OR IGNORE INTO skins VALUES (?)',

    'get_clan_id':
        'SELECT id FROM clans WHERE clan_name = ?',

    'insert_clan':  # (clan_name)
        'INSERT OR IGNORE INTO clans VALUES(NULL, ?)',

    'get_status_text':
        'SELECT id FROM status_text WHERE status = ?',

    'insert_status_text':  # (status)
        'INSERT OR IGNORE INTO status_text VALUES(NULL, ?)',

    'insert_user_profile':  # (user, nick, text, pic, exp, clan, coins, time)
        'INSERT INTO users_data VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?)',
}
msgtype = {
    'null': 0,
    'enter': 1,
    'leave': 2,
    'chat': 3,
    'clan': 4,
    'self_join': 5,
    'self_leave': 6
}
