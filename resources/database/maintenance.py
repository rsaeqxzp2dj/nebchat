
"""
This script does the initial tests on the database and tries to fix any issues
(upgrade, repair, etc). It is based on create_database.sql, and has to be
updated everytime a change is made on create_databse.sql.
"""
import logging
import re
import time
import sqlite3
from sample.misc import utils

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

create_database_sql = utils.get_db_resource("create_database.sql")

sql_struct = re.findall("create (?:table) (?:if not exists) (([A-Za-z_]+) "
                        "(?:\()([\na-zA-Z,\s()_]+));", create_database_sql)

sql_struct_views = re.findall("create view (?:if not exists) (([A-Za-z_]+) as"
                              "([\na-zA-Z,.\"=\s()_]+));", create_database_sql)

sql_tables = {i[1]: i[0] for i in sql_struct}
sql_views = {i[1]: i[2] for i in sql_struct_views}

sql_script_ver = re.search("-- db version (\d+)\.(\d+)\.(\d+)",
                           create_database_sql).groups()
__db_file = ""


def init(cursor: sqlite3.Cursor, conn: sqlite3.Connection, file):
    global __db_file
    __db_file = file
    logger.info("checking database consistency")

    cursor.execute("select * from sqlite_master")
    alls = cursor.fetchall()
    current_tables = []
    current_views = []
    for vtype, name, tbl_name, root, sql in alls:
        if vtype == "table":
            if name in sql_tables:
                process(name, sql, cursor)
            else:
                logger.warning(f"possible obsolete table: {name}")
                delete_empty_obsolete_table(name, cursor)
            current_tables.append(name)
        if vtype == "view":
            if name in sql_views:
                update_view(name, sql, cursor)
            else:
                logger.warning(f"deleting obsolete view: {name}")
                delete_obsolete_view(name, cursor)
            current_views.append(name)

    missingno = 0
    for table in sql_tables:
        if table not in current_tables:
            missingno += 1
    for view in sql_views:
        if view not in current_views:
            missingno += 1
    if missingno:
        entries = len(sql_tables) + len(sql_views)
        logger.info(f"{missingno} missing entries out of {entries}")
        logger.info("running create_database.sql")
        create_database(cursor)

    if "db_version" in current_tables:
        check_version(cursor)
    conn.commit()


def delete_empty_obsolete_table(table, cur: sqlite3.Cursor):
    cur.execute(f"select * from {table}")
    if cur.fetchone():
        logger.warning(f"obsolete table {table} is not empty, deletion aborted")
        return
    logger.info(f"dropping obsolete table {table}, its empty and unused")
    cur.execute(f"drop table {table}")


def delete_obsolete_view(view, cur: sqlite3.Cursor):
    cur.execute(f"drop view {view}")


def create_database(cur: sqlite3.Cursor):
    cur.executescript(create_database_sql)


def create_view(view, sql, cur):
    logger.info(f"creating inexisting view {view}")
    cur.execute(f"create view {view} as {sql}")


# bugfix: carriage return messing up this stuff
# note: this bug might still happen on MacOS, i dont fucking care
def align_sql_string(string: str) -> str:
    return string.replace("\r\n", "\n")


def update_view(view, sql, cur):
    script = align_sql_string(sql_views[view].lower())
    database = align_sql_string(sql.lower())
    if script not in database:
        logger.warning(f"view {view} is different from expected")
        logger.debug("\n".join([script, "!=", database]))
        logger.info(f"deleting and recreating view {view}")
        cur.execute(f"drop view {view}")
        cur.execute(f"create view {view} as {sql_views[view]}")


def process(name, sql, cur):
    script = align_sql_string(sql_tables[name].lower())
    database = align_sql_string(sql.lower())
    if script not in database:
        logger.error(f"table {name} different from expected")
        logger.debug("\n" + script + "\n!=\n" + database)
        logger.info(f"renaming table {name} and creating a new one. "
                    "the old data will need to be copied manually with a patch")
        date = time.strftime("%Y_%m_%d__%H_%M_%S")
        renamed = "_".join((name, date))
        cur.execute(f"alter table {name} rename to {renamed}")
        cur.execute(f"create table {sql_tables[name]}")


def check_version(cur):
    cur.execute("select major, minor, patch from db_version")
    db_ver = cur.fetchone()
    script_ver = tuple(int(i) for i in sql_script_ver)
    if db_ver < script_ver:
        new = ".".join(sql_script_ver)
        old = ".".join(map(str, db_ver))
        logger.warning("database is older than latest version: "
                       f"({new}) > ({old})")
        update_db_version(cur, db_ver)


def __set_version(cur, major, minor, patch):
    cur.execute("insert or replace into db_version "
                "(id, major, minor, patch) values (0, ?, ?, ?)",
                (major, minor, patch))


def update_db_version(cur, version):
    if version < (0, 2, 0):
        logger.info("updating database version to 0.2.0")
        __set_version(cur, 0, 2, 0)
