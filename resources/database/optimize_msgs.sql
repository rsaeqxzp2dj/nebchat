-- this script basically merges duplicated messages in order to decrease the overall space usage.
-- "vacuum" is not must not run to keep allocated space for future events and messages.
pragma foreign_keys=off;
create temp table msgpool (id integer primary key, msg text);
insert into msgpool select msgid, msg from messages group by msg having count(*) > 1;
create index msgpoolidx on msgpool(msg);
create index tmp_events_msgids on events(msgid);
create index tmp_msgs on messages(msg); 
replace into events (id, type, user_id, room_id, msgid, nick, time)
  select events.id, events.type, events.user_id, events.room_id, msgpool.id, events.nick, events.time 
  from events
  inner join messages on messages.msgid = events.msgid
  inner join msgpool on msgpool.msg = messages.msg;
delete from messages where messages.msgid not in (select events.msgid from events);
drop table msgpool;
drop index tmp_events_msgids;
drop index tmp_msgs;