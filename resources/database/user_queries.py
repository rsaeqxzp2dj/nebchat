queries = {
    'user_all_messages':
        """SELECT messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id = ?""",

    'user_all_data_raw':
        "SELECT * from events where events.user_id = ?",

    'user_all_data':
        "SELECT * from legible_events where user_id = ?",

    'many_user_all_data':
        "SELECT * from legible_events where events.user_id in (?)",

    'nicks_from_id_slow':
        """SELECT distinct nickname 
        from nicks inner join events on nicks.id = events.nick 
        where user_id = ?""",

    'nicks_from_id':
        """SELECT distinct nickname 
        from users_nicks inner join nicks on nicks.id = users_nicks.nick_id 
        where users_nicks.user_id = ?""",

    'ids_from_nicks_partial':
        """SELECT distinct nickname, user_id 
        from nicks inner join events on nicks.id = events.nick 
        where nicks.nickname like ?""",

    'ids_from_nick':
        """SELECT distinct nickname, user_id 
        from nicks inner join events on nicks.id = events.nick 
        where nicks.nickname = ?""",

    'ids_from_nick_regex':
        """SELECT distinct nickname, user_id 
        from nicks inner join events on nicks.id = events.nick 
        where nicks.nickname is not null and nicks.nickname REGEXP ?""",

    'user_last_seen':
        """SELECT events.id,datetime(cast(events.time as text),'unixepoch', 'localtime'), room_names.name 
        from events inner join room_names on room_names.id = events.room_id 
        where events.user_id = ? order by events.id desc limit 1""",

    'many_users_last_seen':
        """SELECT events.id,datetime(cast(events.time as text),'unixepoch', 'localtime'), room_names.name, events.user_id 
        from events inner join room_names on room_names.id = events.room_id 
        where events.user_id in (?) order by events.id desc limit 1""",

    'user_phone_search':
        """SELECT events.id, messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id = ? 
        and messages.msg is not null 
        and messages.msg REGEXP '.*(\\+55)?[^\\d]{0,2}0?(1[1-9]|2[12478]|3[1234578]|4[1-9]|5[1345]|6[1-9]|7[134579]|8[1-9]|9[1-9]).{0,3}(9|8)\\d{3,4}.?\\d{4}\\b.*'""",

    'many_users_phone_search':
        """SELECT events.id, events.user_id, messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id in (?) 
        and messages.msg is not null 
        and messages.msg REGEXP '.*(\\+55)?[^\\d]{0,2}0?(1[1-9]|2[12478]|3[1234578]|4[1-9]|5[1345]|6[1-9]|7[134579]|8[1-9]|9[1-9]).{0,3}(9|8)\\d{3,4}.?\\d{4}\\b.*'""",

    'user_location_search':
        """SELECT events.id,messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id = ? 
        and messages.msg is not null 
        and lower(messages.msg) REGEXP '.*(sou? d[aeiou]|(mor[oau]|vivo) [en][amou]).*\\b(ac|al|am|ap|ba|ce|df|es|go|ma|mg|ms|mt|pa|pb|pe|pi|pr|rj|rn|ro|rr|rs|sc|se|sp|to|acre|alagoas|amap[áa]|amazonas|bahia|cear[áa]|distrito federal|esp[íi]rito santo|goi[áa]s|maranh[ãa]o|mato grosso|mato grosso do sul|minas ?gerais|paran[áa]|para[íi]ba|par[áa]|pernambuco|piau[íi]|rio grande do norte|rio grande do sul|rio de janeiro|rond[ôo]nia|roraima|santa catarina|sergipe|s[ãa]o paulo|tocantins|[ms]o[ru]o? [edn][aemo])\\b.*'""",

    'room_users':
        """SELECT events.user_id, nicks.nickname from events 
        inner join nicks on nicks.id = events.nick
        inner join room_names on room_names.id = events.room_id
        where room_names.name = ?""",

    'user_regex_search':
        """SELECT events.id, datetime(cast(events.time as text),'unixepoch', 'localtime'), messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id = ? and messages.msg is not null and lower(messages.msg) REGEXP '?'""",

    'many_users_regex_search':
        """SELECT events.id, datetime(cast(events.time as text),'unixepoch', 'localtime'), events.user_id, messages.msg 
        from events inner join messages on messages.msgid = events.msgid 
        where events.user_id in (?) and messages.msg is not null and lower(messages.msg) REGEXP '?'""",

    'last_x_messages':
        """select * from (select id, date_time, room_name, nickname, user_id, msg from legible_events 
        where room_name=? and type='chat' order by id desc limit ?) order by id""",

    'regex_search':
        "SELECT * from legible_events "
        "where events.user_id in (?) and messages.msg is not null and lower(messages.msg) REGEXP '?'"}
