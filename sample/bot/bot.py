import random
from sample.core.manager import Manager
from sample.core.networking.chatcode import ConnState
from sample.misc import utils
import threading
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Bot:
    MIN_PEOPLE_TO_JOIN = 3
    MIN_PEOPLE_TO_STAY = 1
    MIN_ROOM_SIZE = 3

    def __init__(self, manager: Manager, autostart=False):
        self.manager = manager
        self.random_accounts = True
        self.random_rooms = True
        self.random_nick = True
        self.__variation = 0
        self.__interval = 10
        self.random_nick_type = utils.RandomNickOptions.Both
        self.static_nick = "human"
        self.__thread: threading.Thread = None
        self.__exit_flag = threading.Event()
        if autostart:
            self.start()

    def set_interval(self, value, variation=0):
        self.__interval = value
        self.__variation = variation

    @property
    def interval(self):
        if self.__variation:
            min_var = self.__interval - self.__variation
            max_var = self.__interval + self.__variation
            return random.randrange(min_var, max_var)
        return self.__interval

    @property
    def is_alive(self):
        return self.__thread.isAlive()

    def __get_non_connected_gi(self):
        gilist = []
        for gi in self.manager.accounts.values():
            if not gi.chat.connected:
                gilist.append(gi)
        if gilist and self.random_accounts:
            return random.choice(gilist)
        elif gilist:
            return gilist[0]

    def __get_idle_gi(self):
        for gi in self.manager.accounts.values():
            if gi.chat.connection_state == ConnState.CONNECTED_AWAITING:
                return gi

    def __available_rooms(self, gi):
        occupied = self.manager.occupied_rooms
        gi_rooms = gi.chat.room_list.copy()
        return [gi_rooms[rid] for rid in gi_rooms if rid not in occupied
                and Bot.MIN_PEOPLE_TO_JOIN <= gi_rooms[rid]['current'] <
                gi_rooms[rid]['limit'] > Bot.MIN_ROOM_SIZE]

    def __nick(self):
        if self.random_nick:
            return utils.random_nick(self.random_nick_type)
        return self.static_nick

    def __make_at_least_one_account_idle(self):
        if not any([gi.chat.connection_state == ConnState.CONNECTED_AWAITING
                    for gi in self.manager.accounts.values()]):
            gi = self.__get_non_connected_gi()
            if gi:
                logger.debug(f"non connected account: {gi.id}")
                gi.connect()

    def __enter_in_a_good_room(self):
        idle_gi = self.__get_idle_gi()
        if idle_gi:
            good_rooms = self.__available_rooms(idle_gi)
            if good_rooms and self.random_rooms:
                choice = random.choice(good_rooms)['name']
                nick = self.__nick()
                logger.debug(f"account {idle_gi.id} joining in {choice} with "
                             f"nick {nick}")
                idle_gi.chat.send_room_join_request(choice, nick)
            elif good_rooms:
                logger.debug(f"account {idle_gi.id} joining on room "
                             f"{good_rooms[0]['name']}")
                idle_gi.chat.send_room_join_request(
                    good_rooms[0]['name'], self.__nick())

    def __leave_room_if_too_lonely(self):
        redundancy = []
        for gi in self.manager.accounts.values():
            if gi.chat.connection_state == ConnState.CONNECTED_AWAITING:
                redundancy.append(gi)
            if not gi.chat.connected or not \
                    gi.chat.connection_state == ConnState.CONNECTED_INROOM:
                continue
            if gi.chat.room_status['current_people'] <= Bot.MIN_PEOPLE_TO_STAY:
                logger.debug(f"removing {gi.id} from room "
                             f"{gi.chat.room_status['name']} because current "
                             f"people={gi.chat.room_status['current_people']}")
                gi.chat.send_lobby_leave_request()
        return redundancy

    @staticmethod
    def __remove_unwanted_children(redundant_gi_ses):
        if len(redundant_gi_ses) > 1:
            gi = redundant_gi_ses[-1]
            gi.chat.disconnect()
            logger.debug(f"disconencting redundant account {gi.id}")

    def start(self):
        logger.debug("starting bot")
        self.__exit_flag.clear()
        self.__thread = threading.Thread(target=self.loop)
        self.__thread.setName("bot_thread")
        self.__thread.start()

    def stop(self):
        logger.debug("stopping bot")
        self.__exit_flag.set()

    def step(self):
        self.__make_at_least_one_account_idle()
        argh = self.__leave_room_if_too_lonely()
        self.__enter_in_a_good_room()
        self.__remove_unwanted_children(argh)

    def loop(self):
        while not self.__exit_flag.wait(timeout=self.interval):
            self.step()
