import logging
import sqlite3
from collections import namedtuple

from resources.database import maintenance
import resources.database.user_queries as db_queries
import resources.database.internal_operations as db_ops
from sample.misc import utils

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# noinspection PyUnresolvedReferences
class Database:
    __verified = False
    db_path = utils.user_data("database.db")

    def __init__(self, skip_mapping=False):
        self._db_connection = sqlite3.connect(Database.db_path, 300)
        self._cur = self._db_connection.cursor()
        if not skip_mapping:
            self._operation = self.__map_database_internal_operations()
        self._init_test()

    def close(self):
        self._db_connection.close()

    def query(self, string, *params):
        return self._cur.execute(db_queries.queries[string], params).fetchall()

    def __map_database_internal_operations(self):
        # Transforms all dict keys into callable attributes allowing params
        nt = namedtuple('intern_ops', db_ops.op.keys())
        functions = []
        for i in db_ops.op.values():
            def outer(query):
                def func(*params):
                    return self._cur.execute(query, params)
                return func
            functions.append(outer(i))
        return nt(*functions)

    def _init_test(self):
        if not Database.__verified:
            maintenance.init(self._cur, self._db_connection, self.db_path)
            Database.__verified = True

    def __create_tables(self):
        script = utils.get_db_resource("create_database.sql")
        self._cur.executescript(script)
        self._db_connection.commit()
        return

    def __associate_nick_user(self, nick_id, user_id):
        if nick_id:
            self._operation.associate_nick_id(user_id, nick_id)

    def save_room(self, room):
        self._operation.get_room_id(room)
        res = self._cur.fetchone()
        if res:
            return res[0]
        else:
            self._operation.insert_room(room)
            return self._cur.lastrowid

    def save_nick(self, nick):
        self._operation.get_nick_id(nick)
        res = self._cur.fetchone()
        if res:
            return res[0]
        else:
            self._operation.insert_nick(nick)
            return self._cur.lastrowid

    def save_key_derivation(self, key):
        self._operation.get_key_deriv(key)
        res = self._cur.fetchone()
        if res:
            return res[0]
        else:
            self._operation.insert_key_deriv(key)
            return self._cur.lastrowid

    def save_public_message(self, nick, uid, room, msg, time):
        self.__save_message(nick, uid, room, msg, time, 'chat')

    def save_clan_message(self, uid, nick, msg, time):
        self.__save_message(nick, uid, None, msg, time, 'clan')

    def save_room_enter(self, nick, uid, room, time):
        self.__save_room_event(uid, nick, room, time, 'enter')

    def save_room_leave(self, nick, uid, room, time):
        self.__save_room_event(uid, nick, room, time, 'leave')

    def save_self_enter(self, nick, uid, room, time):
        self.__save_room_event(uid, nick, room, time, 'self_join')

    def save_self_leave(self, nick, uid, room, time):
        self.__save_room_event(uid, nick, room, time, 'self_leave')

    def save_friend_message(self, data, key):
        key = self.save_key_derivation(key)
        self._operation.insert_private_event(key, data)

    def save_private_message(self, data, key):
        key = self.save_key_derivation(key)
        self._operation.insert_private_event(key, data)

    def __save_room_event(self, uid, nick, room, time, event):
        room_id = self.save_room(room)
        nick_id = self.save_nick(nick)
        msgtype = db_ops.msgtype[event]
        self._operation.insert_event(
            msgtype,
            uid,
            room_id,
            0,
            nick_id,
            int(time)
        )
        self._db_connection.commit()

    def __save_message(self, nick, uid, room, msg, time, msgtype):

        self._operation.insert_msg(msg)
        msg_id = self._cur.lastrowid

        room_id = self.save_room(room) if room is not None else 0
        nick_id = self.save_nick(nick) if nick is not None else 0
        msgtype = db_ops.msgtype[msgtype]

        self._operation.insert_uid(uid)
        self.__associate_nick_user(nick_id, uid)

        self._operation.insert_event(
            msgtype,
            uid,
            room_id,
            msg_id,
            nick_id,
            int(time)
        )
        self._db_connection.commit()

    # ######## profile saving

    def save_skin(self, skin_id):
        self._operation.insert_skin(skin_id)

    def __associate_skin_id(self, skin_id, user_id):
        if skin_id and user_id:
            self._operation.associate_skin_id(user_id, skin_id)

    def save_clan(self, clan_name):
        self._operation.insert_clan(clan_name)
        self._operation.get_clan_id(clan_name)
        return self._cur.fetchone()[0]

    # as known as profile text
    def save_status_text(self, status_text):
        self._operation.insert_status_text(status_text)
        self._operation.get_status_text(status_text)
        return self._cur.fetchone()[0]

    def save_user_data(self, uid, nick, text, pic_id, skins, exp, clan, coins,
                       time):
        self._operation.insert_uid(uid)
        self.save_skin(pic_id)
        self.__associate_skin_id(pic_id, uid)
        for skin in skins:
            self.__associate_skin_id(skin, uid)
            self.save_skin(skin)
        nick_id = self.save_nick(nick) if nick else 0
        status_text_id = self.save_status_text(text) if text else 0
        clan_id = self.save_clan(clan) if clan else 0

        self._operation.insert_user_profile(uid, nick_id, status_text_id,
                                            pic_id, exp, clan_id, coins, time)
        self._db_connection.commit()
