import base64
import hashlib
import struct

import bcrypt
from Cryptodome import Random
from Cryptodome.Cipher import AES


class AESCipher(object):
    def __init__(self, key):
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(raw.encode())

    def decrypt(self, enc):
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:]))

    def encrypt_bytes(self, raw_bytes):
        string = base64.b64encode(raw_bytes).decode()
        return self.encrypt(string)

    def decode_bytes(self, enc):
        return base64.b64decode(self.decrypt(enc))

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


class MsgEncryptor:
    def __init__(self, key):
        self.__cipher = AESCipher(key)
        self.__key_deriv = bcrypt.hashpw(key.encode(), bcrypt.gensalt(9))

    @property
    def key_derivation(self):
        return self.__key_deriv

    def pack_encode_msg(self, nick, sender_id, recv_id, msg, time):
        nick = nick.encode()
        msg = msg.encode()
        len_nick = len(nick)
        len_msg = len(msg)
        packed = struct.pack(f'!b{len_nick}sIIb{len_msg}sI', len_nick, nick,
                             sender_id, recv_id, len_msg, msg, time)
        return self.__cipher.encrypt_bytes(packed)

    @staticmethod
    def test_password(password, deriv_key):
        if bcrypt.checkpw(password.encode(), deriv_key):
            return True

    def unpack_decode_msg(self, data):
        data = self.__cipher.decode_bytes(data)
        if not data:
            return
        offset = 0
        len_nick, = struct.unpack_from('b', data, offset)
        offset += 1
        nick, = struct.unpack_from(f'!{len_nick}s', data, offset)
        offset += len_nick
        sid, rid, len_msg = struct.unpack_from('!IIb', data, offset)
        offset += 9
        msg, time = struct.unpack_from(f'!{len_msg}sI', data, offset)
        return nick.decode(), sid, rid, msg.decode(), time
