import enum
import json
import logging

from sample.core.networking.account import Account
from sample.misc import validators
from sample.misc.utils import user_data
from .msg_processing.handler import Handler
from .networking.game_interface import GameInterface

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Events(enum.Enum):
    public_msg = 0
    private_msg = 1
    friend_msg = 2
    clan_msg = 3
    self_enter = 4
    self_leave = 5
    user_join = 6
    user_leave = 7
    connection_change = 8
    lobby_update = 9
    room_enter_error = 10


class Manager:
    """
    This class is the glue, it sticks all parts together.

    Accounts are ready to use once added to _tickets. A new game interface (gi)
    is created for each account

    Sessions are basically all tickets being used from a given moment, all
    sessions are stored inside the sessions.json file, it is never fully loaded
    in memory.
    """
    allow_duplicated_listeners = False
    sessions_filename = "sessions.json"
    default_session_name = "default"

    def __init__(self):
        self.notes = ""
        self.accounts: {str: GameInterface} = {}
        self.__current_session = Manager.default_session_name
        self.__tickets: [] = []
        self.__handler = Handler()
        self.__handler.start()
        self.__listeners: {Events: [(int, function, bool)]} = {
            Events.room_enter_error: [],
            Events.connection_change: [],
            Events.lobby_update: [],
            Events.public_msg: [],
            Events.private_msg: [],
            Events.friend_msg: [],
            Events.clan_msg: [],
            Events.self_enter: [],
            Events.self_leave: [],
            Events.user_join: [],
            Events.user_leave: []
        }

    def cleanup(self):
        for listener in self.__listeners:
            self.__listeners[listener].clear()
        for gi in self.accounts.values():
            gi.disconnect(False)
        self.__handler.force_stop()

    @property
    def occupied_rooms(self):
        """
        used mainly for filtering purposes
        :return:
        """
        ret_dict = {}
        for gi in self.accounts.values():
            if not gi.chat.room_status['room_id']:
                continue
            ret_dict[gi.chat.room_status['room_id']] = gi.chat.room_status
        return ret_dict

    def get_last_x_messages(self, room_name, x=100):
        """
        Load previous messages from a chatroom, it was a feature request.
        :param room_name: room name to get old messages from
        :param x: number of messages to load
        :return: yields each message
        """
        if not x:
            return
        result = self.__handler.query("last_x_messages", room_name, x)
        for msgid, datetime, room, nick, uid, msg in result:
            yield datetime, nick, uid, msg, msgid

    def __ticket_from_id(self, aid):
        for i in self.__tickets:
            if validators.uid_from_ticket(i) == aid:
                return i

    def save_player_profile_data(self, *args):  # wrapper for Handler.user_data()
        self.__handler.users_data(*args)

    def blacklist_player(self, uid):
        self.__handler.messages.blacklist.append(uid)

    def remove_from_blacklist(self, uid):
        if uid in self.__handler.messages.blacklist:
            self.__handler.messages.blacklist.remove(uid)
        else:
            logger.error(f"player {uid} not in blacklist")

    @property
    def blacklist(self):
        return self.__handler.messages.blacklist.copy()

    def register_listener(self, event: Events, aid, receiver, once=False):
        """
        Registers a function to listen to a specific event and receive its data
        from a specific account

        :param event: event type, must be one of the Events class
        :param aid: account id
        :param receiver: function being registered to receive the event's data
        :param once: remove listener after one use
        :return: None
        """
        obj = (aid, receiver, once)
        if obj in self.__listeners[event]:
            if not Manager.allow_duplicated_listeners:
                logger.error("cannot register already registered listener")
                logger.debug(f"tried to register: {obj}")
                return
            else:
                logger.warning("listener already registered")
        self.__listeners[event].append(obj)
        logger.debug(f"registering listener: {event},{aid},{receiver}")

    def remove_listener(self, event: Events, aid, func, once=False):
        """
        Removes a specific listener

        :param event: event type, must be one of the Events class
        :param aid: account id
        :param func: registered listener
        :param once: remove listener after one use
        :return: None
        """
        if (aid, func, once) not in self.__listeners[event]:
            logger.debug("cannot remove unregistred listener "
                         f"aid={aid} func={func}")
            return
        self.__listeners[event].remove((aid, func, once))
        logger.debug(f"removed listener {event},{aid},{func}")

    def has_listener(self, event, aid, func, once=False):
        return (aid, func, once) in self.__listeners[event]

    def __delegate(self, msgtype, aid, *args):
        """
        Forwards *args to registered listeners
        :param msgtype: event type, must be one of the Events class
        :param aid: account id
        :param args: arguments to be forwarded
        :return:
        """
        for listener_aid, listener, once in self.__listeners[msgtype]:
            if aid == listener_aid:
                listener(*args)
                if once:
                    self.remove_listener(msgtype, aid, listener, once)

    def __setup_chat_callbacks(self, gi: GameInterface) -> None:
        def process_public_msg(*args):
            if self.__handler.public_msg(gi.chat.room_status['name'], *args):
                self.__delegate(Events.public_msg, gi.id, *args)
        gi.chat.on_public_msg = process_public_msg

        def process_private_msg(*args):
            self.__handler.private_msg(*args)
            self.__delegate(Events.private_msg, gi.id, *args)
        gi.chat.on_private_msg = process_private_msg

        def process_friend_msg(*args):
            self.__handler.friend_msg(*args)
            self.__delegate(Events.friend_msg, gi.id, *args)
        gi.chat.on_friend_msg = process_friend_msg

        def process_clan_msg(*args):
            self.__handler.clan_msg(*args)
            self.__delegate(Events.clan_msg, gi.id, *args)
        gi.chat.on_clan_msg = process_clan_msg

        def process_self_enter(*args):
            self.__handler.self_enter(*args)
            self.__delegate(Events.self_enter, gi.id, *args)
        gi.chat.on_self_enter = process_self_enter

        def process_self_leave(*args):
            self.__handler.self_leave(*args)
            self.__delegate(Events.self_leave, gi.id, *args)
        gi.chat.on_self_leave = process_self_leave

        def process_user_enter(*args):
            self.__handler.user_enter(*args)
            self.__delegate(Events.user_join, gi.id, *args)
        gi.chat.on_user_join = process_user_enter

        def process_user_leave(*args):
            self.__handler.user_leave(*args)
            self.__delegate(Events.user_leave, gi.id, *args)
        gi.chat.on_user_leave = process_user_leave

        def process_conn_change(*args):
            self.__delegate(Events.connection_change, gi.id, *args)
        gi.chat.on_connection_change = process_conn_change

        def process_lobby_update(*args):
            self.__delegate(Events.lobby_update, gi.id, *args)
        gi.chat.on_lobby_update = process_lobby_update

        def process_room_enter_error(*args):
            self.__delegate(Events.room_enter_error, gi.id, *args)
        gi.chat.on_self_enter_error = process_room_enter_error
        return

    def __add_game_interface(self, ticket):
        interface = GameInterface(ticket)
        self.__setup_chat_callbacks(interface)
        self.accounts[validators.uid_from_ticket(ticket)] = interface
        logger.debug(f"adding interface with ticket {ticket}")

    def __remove_game_interface(self, ticket):
        del self.accounts[validators.uid_from_ticket(ticket)]
        logger.debug(f"removing interface with ticket {ticket}")

    def test_token(self, token):
        """
        Tries to take the connection ticket for a given facebook token and tests
        the ticket for possible duplicates
        :param token: facebook token string
        :return: True and ticket if ok, False and message with reason otherwise
        """
        ticket = Account.api_facebook_login(validators.parse_facebook_token(token))
        if not ticket:
            return False, "Error validating facebook token"
        else:
            return self.test_ticket(ticket)

    def test_ticket(self, ticket):
        """
        This should be called before add_account
        :param ticket: ticket to test
        :return: True and ticket if ok, False and message with reason otherwise
        :rtype: bool, str
        """
        r = []
        if not validators.is_ticket_valid(ticket):
            r.append("Invalid ticket")
        if self.__ticket_from_id(validators.uid_from_ticket(ticket)):
            r.append("Account being used")
        if not GameInterface(ticket).account.api_checkin():
            r.append("Error validating ticket")
        if not r:
            return True, ticket
        else:
            return False, "\n".join(r)

    def add_account(self, ticket, auto_save=True):
        """
        Tries to add connection ticket
        :param ticket: connection ticket to be added, obviously
        :param auto_save: save session right after adding it
        :return:
        """
        if validators.is_ticket_valid(ticket):
            if ticket in self.__tickets:
                logger.error(f"cannot add existing ticket {ticket}")
                return
            uid = validators.uid_from_ticket(ticket)
            if self.__ticket_from_id(uid):
                logger.error(f"cannot add existing account {uid}")
                return
            self.__tickets.append(ticket)
            self.__add_game_interface(ticket)
            # self.activate_account(uid)
            if auto_save:
                self.save_session(self.__current_session)
            return True
        else:
            logger.error(f"trying to add invalid ticket: {ticket}")

    def remove_account(self, ticket, auto_save=True):
        """
        Tries to remove connection ticket
        :param ticket: connection ticket to be removed, obviously
        :param auto_save: save session right after removing it
        :return:
        """
        if ticket in self.__tickets:
            self.__tickets.remove(ticket)
            self.__remove_game_interface(ticket)
            t_uid = validators.uid_from_ticket(ticket)
            for event in self.__listeners:
                for aid, func, once in self.__listeners[event]:
                    if aid == t_uid:
                        logger.warning(f"deleted account {aid} still listening "
                                       f"to {event}")
                        self.__listeners[event].remove((aid, func, once))

            if auto_save:
                self.save_current_session()
            return True
        else:
            logger.error(f"trying to remove inexistent ticket: {ticket}")

    def remove_account_by_id(self, uid, auto_save=True):
        ticket = self.__ticket_from_id(uid)
        if not ticket:
            logger.error(f"cannot remove ticket: ticket not found for id {uid}")
            return
        self.remove_account(ticket, auto_save)

    def __load_interfaces(self, used_tickets, append_only=False):
        for ticket in used_tickets:
            if ticket not in self.accounts:
                self.__add_game_interface(ticket)
        if not append_only:
            for ticket in self.__tickets:
                if ticket not in used_tickets:
                    self.__remove_game_interface(ticket)

    @staticmethod
    def __default_load_session_file(test_sid=None):
        with open(user_data(
                Manager.sessions_filename,
                create_file=True,
                init_content='{"sessions": {}, "default": ""}')) as f:
            try:
                data = json.load(f)
            except json.JSONDecodeError:
                logger.exception("error loading sessions file")
                return
            if not validators.validate_session_data(data, test_sid,
                                                    test_sid is None):
                logger.error("invalid sessions file")
                return
            return data

    def load_session(self, sid=None, append_only=False):
        """
        load saved tickets
        :param sid: session id, default if None
        :param append_only: append to current __tickets, not replace
        :return: True on success, False on fail
        """
        data = self.__default_load_session_file(sid)

        if not data:
            logger.error("unable to load session")
            return False

        # load default session
        sid = data['default'] if sid is None else sid

        self.notes = data['sessions'][sid]['notes']
        old = self.__tickets.copy()
        self.__tickets = data['sessions'][sid]['tickets']
        self.__load_interfaces(self.__tickets, append_only)

        if append_only:
            for i in old:
                if i not in self.__tickets:
                    self.__tickets.append(i)

        self.__current_session = sid
        return True

    def save_session(self, sid: str):
        """
        Saves current activated and unactivated tickets as session sid or
        creates new session.json of previous one was corrupted/invalid
        :param sid: session id
        :return:
        """
        data = self.__default_load_session_file()
        if not data:
            logger.warning("replacing invalid json")
            data = {}

        with open(user_data(Manager.sessions_filename), 'w') as f:
            if not isinstance(data, dict):
                data = {'sessions': {}}
            if "sessions" not in data:
                data['sessions'] = {}
            data['sessions'].update({
                sid: {
                    "tickets": self.__tickets,
                    "notes": self.notes
                }
            })
            if "default" not in data:
                data['default'] = sid
            json.dump(data, f, indent=2)

        self.__current_session = sid
        return

    def save_current_session(self):
        self.save_session(self.__current_session)

    @property
    def default_session(self):
        data = self.__default_load_session_file()
        return data['default']

    def set_default_session(self, sid=None):
        """
        :param sid: session id, use first if None
        """
        data = self.__default_load_session_file(sid)
        if not data:
            logger.error("unable to set default session")
            return
        with open(user_data(Manager.sessions_filename), 'w') as f:
            if len(data['sessions']):
                data['default'] = list(data['sessions'])[0]
            else:
                data['default'] = self.__current_session
            json.dump(data, f, indent=2)

    def delete_session(self, sid: str):
        """
        Deletes the session sid, if this is the default session, change to the
        first available.
        Current session cannot be deleted
        :param sid: session id str
        :return: None
        """
        data = self.__default_load_session_file(sid)

        if sid == self.__current_session:
            logger.error("cannot delete current session")
            return

        with open(user_data(Manager.sessions_filename,
                            create_file=False,
                            init_content="{}"), 'w') as f:
            if data['default'] == sid:
                data['default'] = self.__current_session
            data['sessions'].pop(sid)
            json.dump(data, f, indent=2)

    @property
    def sessions(self):
        data = self.__default_load_session_file()
        if not data:
            logger.error("unable to list sessions")
            return
        else:
            return data['sessions']
