import logging
import time

logger = logging.getLogger(__name__)


class UsersMessages:
    limit = 7
    time_limit = 120  # 2min cooldown, dont block distant future messages
    blacklist = []

    def __init__(self):
        self.filter_multiline = True
        self.msgs: {0: [('', 0)]} = {}  # {uid: (['msg1', ...], last_update)}

    def filter(self, uid, msg):
        if self.filter_multiline:
            if '\n' in msg:
                return None

        if uid in self.blacklist:
            logger.debug(f"blacklisted: {uid}:{msg}")
            return

        current_time = int(time.time())

        if uid not in self.msgs:
            self.msgs[uid] = [(msg, current_time)]
            return msg

        for old_msg, msg_time in self.msgs[uid]:
            if current_time - msg_time > UsersMessages.time_limit or \
                    len(self.msgs[uid]) >= UsersMessages.limit:
                self.msgs[uid].remove((old_msg, msg_time))
            if msg == old_msg:
                logger.debug(f"message filtered: {uid}:{msg}")
                return None
        self.msgs[uid].append((msg, current_time))
        return msg
