import logging
import threading
import time
from queue import Queue
import sys

from sample.core.database.database import Database
from sample.core.database.encryption import MsgEncryptor
from .filtering import UsersMessages

logger = logging.getLogger(__name__)


class Handler(threading.Thread):
    """
    Handles message storing, spam filtering and message encrypting
    """
    def __init__(self):
        super().__init__()
        self.setName("Handler")
        self.__keep_running = True
        self.queue = Queue()
        self.messages = UsersMessages()
        # database connection must be done in the worker thread
        self.database = None
        self.encryptor = MsgEncryptor(str(47/53).replace(".", "¬"))

    def set_password(self, passwd):
        self.encryptor = MsgEncryptor(passwd)

    def stop(self):
        self.__keep_running = False

    def force_stop(self):
        self.stop()
        self.queue.put((lambda x: None, None))

    # noinspection PyUnusedLocal
    def public_msg(self, room, nick, uid, msg, msgid):
        data = self.messages.filter(uid, msg)
        if not data:
            return False
        now = int(time.time())
        self.queue.put((self.database.save_public_message, nick, uid, room, msg, now))
        return True

    # noinspection PyUnusedLocal
    def clan_msg(self, nick, uid, msg, role, msgid):
        now = int(time.time())
        self.queue.put((self.database.save_clan_message, uid, nick, msg, now))

    # noinspection PyUnusedLocal
    def private_msg(self, nick, sender_id, receiv_id, msg, msgid):
        now = int(time.time())
        data = self.encryptor.pack_encode_msg(nick, sender_id, receiv_id, msg, now)
        self.queue.put((self.database.save_private_message,
                        data, self.encryptor.key_derivation))

    # noinspection PyUnusedLocal
    def friend_msg(self, sid, rid, msg, msgid):
        now = int(time.time())
        data = self.encryptor.pack_encode_msg('.', sid, rid, msg, now)
        self.queue.put((self.database.save_friend_message,
                        data, self.encryptor.key_derivation))

    def user_enter(self, uid, nick, room):
        now = int(time.time())
        self.queue.put((self.database.save_room_enter, nick, uid, room, now))

    def user_leave(self, uid, nick, room):
        now = int(time.time())
        self.queue.put((self.database.save_room_leave, nick, uid, room, now))

    def self_enter(self, uid, nick, room):
        now = int(time.time())
        self.queue.put((self.database.save_self_enter, nick, uid, room, now))

    def self_leave(self, uid, nick, room):
        now = int(time.time())
        self.queue.put((self.database.save_self_leave, nick, uid, room, now))

    @staticmethod
    def query(query_str, *params):
        db = Database(True)
        ret = db.query(query_str, *params)
        db.close()
        return ret

    def users_data(self, uid, nick, text, pic_id, skins, exp, clan, coins):
        now = int(time.time())
        self.queue.put((self.database.save_user_data, uid, nick, text, pic_id,
                        skins, exp, clan, coins, now))

    def run(self):
        self.database = Database()
        while self.__keep_running:
            func, *params = self.queue.get()
            try:
                func(*params)
            except Exception as er:
                logger.fatal(f"Error {er} executing {str(func)}({str(params)})")
