import http.client
import json
import logging
import urllib.parse
from math import sqrt

from sample.misc import validators

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Account:
    client_version = "2.1.4.1"
    version_no = 477
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    https_url = "www.simplicialsoftware.com"
    http_url = "52.6.241.44"
    prefix = "/api/account/"
    skins_url_base = "s3.amazonaws.com"
    skins_url_prefix = "/simplicialsoftware.skins/"
    request_timeout = 5000

    class Mail:
        def __init__(self):
            self.from_id = 0
            self.from_name = "?"
            self.is_new = True
            self.msg_id = -1
            self.subject = "?"
            self.time_expires = 0
            self.time_sent = 0
            self.message = ""

    class Friend:
        def __init__(self):
            self.uid = 0
            self.last_played_utc = "0"
            self.name = ""
            self.xp = 0
            self.level = 0
            self.relationship = ""
            self.clan_name = ""

    def __init__(self, ticket):
        self.ticket = ticket
        self.__uid = validators.uid_from_ticket(ticket)
        if not self.__uid:
            logger.warning("invalid ticket")
        self.busy = False
        self.error = None
        self.account_name = "?"
        self.coins = 0
        self.video_reward_count = 0
        self.video_reward_sum = 0
        self.video_reward = 0
        self.spin_count = 0
        self.spin_time = 0
        self.spin_data = ""
        self.checkin_reward = 0
        self.clan_name = "?"
        self.has_friend_requests = False
        self.has_clan_invites = False
        self.has_new_mail = False
        self.mails: [Account.Mail] = []
        self.friends: [Account.Friend] = []
        self.server_message = ""
        self.profile = ""

    @property
    def uid(self):
        return self.__uid

    @staticmethod
    def __get_connection(https=True, https_url=https_url, http_url=http_url):
        try:
            if https:
                return http.client.HTTPSConnection(https_url)
            else:
                return http.client.HTTPConnection(http_url)
        except ConnectionError as er:
            logger.error(er)
            return None

    @staticmethod
    def convert_xp_to_level(xp):
        return round(sqrt(xp / 1000 * 2))

    @staticmethod
    def __request(url, param):
        # doesn't sets busy state
        try:
            conn = Account.__get_connection()
            conn.timeout = Account.request_timeout
            logger.debug(f"__api_generic_request({url}, {param})")
            conn.request("POST", Account.prefix + url, param, Account.headers)
            ret = conn.getresponse()
            conn.sock.close()
        except Exception as er:
            logger.error(f"on __request: {er}")
            ret = None
        return ret

    def __api_generic_request(self, url, alt_param=None):
        # sets busy state
        self.busy = True
        if alt_param:
            param = alt_param
        else:
            param = urllib.parse.urlencode({'Game': 'Nebulous',
                                            'Ticket': self.ticket.encode()})
        resp = self.__request(url, param)
        self.busy = False
        return resp

    def api_get_clan_info(self):
        return self.__api_generic_request("GetClanInfo")

    def api_get_alerts(self):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'ClientVersion': Account.client_version})
        resp = self.__api_generic_request("GetAlerts", param)
        try:
            data = json.loads(resp.read().decode())
            self.coins = data['Coins']
            self.clan_name = data['ClanName']
            self.has_new_mail = data['NewMail']
            self.server_message = data['ServerMessage']
            self.has_clan_invites = data['HasClanInvites']
            self.has_friend_requests = data['HasFriendRequests']
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_alerts: {e}")
        return data

    def api_checkin(self):
        resp = self.__api_generic_request("CheckIn")
        try:
            data = json.loads(resp.read().decode())
            self.coins = data['Coins']
            self.checkin_reward = data['CheckinReward']
            self.video_reward_count = data['RewardVideosRemaining']
            error = data.get('Error')
            if error:
                logger.warning(f"account checkin error: {error}")
                return None
            logger.info("api_checkin in account:" + self.__uid)
        except Exception as e:
            data = None
            logger.debug(f"error on api_checkin: {e}")
        return data

    def api_get_mail_list(self):
        resp = self.__api_generic_request("GetMailList")
        try:
            data = json.loads(resp.read().decode())
            self.mails.clear()
            for x in data['Mail']:
                mail = Account.Mail()
                mail.time_sent = x['TimeSent']
                mail.time_expires = x['TimeExpires']
                mail.from_name = x['FromName']
                mail.msg_id = x['MsgID']
                mail.subject = x['Subject']
                mail.from_id = x['FromAID']
                mail.is_new = x['IsNew']
                self.mails.append(mail)
        except Exception:
            data = None
        return data

    def api_read_mail(self, mail_id):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'MsgID': mail_id})
        resp = self.__api_generic_request("ReadMail", param)
        try:
            data = json.loads(resp.read().decode())
            self.mails.clear()
            mail = Account.Mail()
            mail.time_sent = data['TimeSent'].replace("T", " ")
            mail.time_expires = data['TimeExpires'].replace("T", " ")
            mail.from_name = data['FromName']
            mail.msg_id = data['MsgID']
            mail.subject = data['Subject']
            mail.from_id = data['FromAID']
            mail.is_new = data['IsNew']
            mail.message = data['Message']
            self.mails.append(mail)
        except Exception as e:
            data = None
            logger.debug(f"error on api_read_mail({mail_id}): {e}")
        return data

    def api_get_spin_info(self, do_spin):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Version': Account.version_no,
                                        'Ticket': self.ticket,
                                        'Spin': do_spin})
        resp = self.__api_generic_request("GetSpinInfo", param)
        try:
            data = json.loads(resp.read().decode())
            self.spin_count = data['SpinsRemaining']
            self.spin_data = f"{data['SpinType']} = {data['SpinData']}"
            self.spin_time = data['NextSpinRemainingMs']
            self.error = data['Error']
        except Exception as e:
            data = None
            logger.debug(f"error in api_get_spin_info({do_spin}): {e}")
        return data

    @staticmethod
    def api_get_mods():
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Version': Account.version_no})
        resp = Account.__request("GetMods", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_mods: {e}")
        return data

    def api_get_player_profile(self, uid):
        param = urllib.parse.urlencode({'accountID': uid,
                                        'Version': Account.version_no,
                                        'Ticket': self.ticket})
        resp = self.__api_generic_request("GetPlayerProfile", param,)
        try:
            data = json.loads(resp.read().decode())
            profile = data['profile']
            if str(uid) == str(self.__uid):
                self.profile = profile
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_player_profile({uid}): {e}")
        return data

    def api_get_player_stats(self, uid, mode='ALL'):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'AccountId': uid,
                                        'Version': Account.version_no,
                                        'GameMode': mode})
        resp = self.__api_generic_request("GetPlayerStats", param)
        try:
            data = json.loads(resp.read().decode())
            if str(uid) == str(self.__uid):
                self.account_name = data['AccountName']
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_player_stats({uid}, {mode}): {e}")
        return data

    def get_player_profile(self, uid):
        profile_data = self.api_get_player_profile(uid)
        stats_data = self.api_get_player_stats(uid)
        if not profile_data or not stats_data:
            logger.error(f"error getting data from user {uid}")
            return
        return {'profile_data': profile_data, 'stats_data': stats_data}

    @staticmethod
    def get_player_skin(uid, skin_id) -> (str, bytes):
        try:
            conn = Account.__get_connection(True, Account.skins_url_base)
            conn.timeout = Account.request_timeout
            conn.request("GET", Account.skins_url_prefix + skin_id)
            data = conn.getresponse().read()
            conn.sock.close()
        except Exception as e:
            data = 0, b''
            logger.debug(f"error on api_get_player_skin({uid},{skin_id}): {e}")
        return skin_id, data

    def get_player_skins(self, uid):
        profile_data = self.api_get_player_stats(uid)
        try:
            skins_ids = profile_data['ValidCustomSkinIDs']
            conn = Account.__get_connection(True, Account.skins_url_base)
            conn.timeout = Account.request_timeout
            data = {}
            for skin_id in map(str, skins_ids):
                conn.request("GET", Account.skins_url_prefix + skin_id)
                data[skin_id] = conn.getresponse().read()
            conn.sock.close()
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_player_skins({uid}): {e}")
        return data

    def api_get_friends(self, startidx=0, count=100):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'StartIndex': startidx,
                                        'Count': count})
        resp = self.__api_generic_request("GetFriends", param)
        try:
            data = json.loads(resp.read().decode())
            self.friends.clear()
            for afriend in data['FriendRequests']:
                friend = Account.Friend()
                friend.uid = afriend['Id']
                friend.name = afriend['Name']
                friend.clan_name = afriend['ClanName']
                friend.last_played_utc = afriend['LastPlayedUtc']
                friend.relationship = afriend['Relationship']
                friend.xp = afriend['XP']
                friend.level = Account.convert_xp_to_level(friend.xp)
                self.friends.append(friend)
        except Exception as e:
            data = None
            logger.debug(f"error on api_get_friends({startidx}, {count}): {e}")
        return data

    def api_set_name(self, name):
        param = urllib.parse.urlencode({'Ticket': self.ticket,
                                        'Name': name})
        resp = self.__api_generic_request("SetName", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            logger.debug(f"error on api_set_name({name}): {e}")
            data = None
        return data

    def api_unban(self, uid):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'AccountID': uid})
        return self.__api_generic_request("UnBan", param)

    def api_remove_friend(self, uid):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'FriendAccountID': uid})
        return self.__api_generic_request("RemoveFriend", param)

    def api_add_friend(self, uid):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'FriendAccountID': uid})
        return self.__api_generic_request("AddFriend", param)

    def api_send_mail(self, dest, subject, msg):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'ToAID': dest,
                                        'Subject': subject,
                                        'Message': msg})
        return self.__api_generic_request("SendMail", param)

    def api_delete_mail(self, mail_id):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'MsgID': mail_id})
        return self.__api_generic_request("DeleteMail", param)

    def api_block_mail(self, uid):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'AccountID': uid})
        return self.__api_generic_request("BlockMail", param)

    def api_confirm_reward_video(self):
        resp = self.__api_generic_request("ConfirmRewardVideo")
        try:
            data = json.loads(resp.read().decode())
            self.coins = data['Coins']
            self.video_reward = data['CoinReward']
            self.video_reward_sum += self.video_reward
            self.video_reward_count = data['RewardVideosRemaining']
            self.error = data['Error']
        except Exception:
            data = None
        return data

    def api_set_player_profile(self, txt):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Version': Account.version_no,
                                        'Ticket': self.ticket, 'Profile': txt})
        resp = self.__api_generic_request("SetPlayerProfile", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            data = None
            logger.debug(f"error on api_set_player_profile({txt}): {e}")
        return data

    @staticmethod
    def api_facebook_login(token):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Version': Account.version_no,
                                        'authToken': token})
        resp = Account.__request("FacebookLogin", param)
        try:
            data = json.loads(resp.read().decode())['Ticket']
        except Exception as e:
            data = None
            logger.debug(f"error on api_facebook_login({token}): {e}")
        return data

    def api_coin_purchase(self, itype, iid):
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'Ticket': self.ticket,
                                        'ItemType': itype,
                                        'ItemID': iid})
        resp = self.__api_generic_request("CoinPurchase", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            data = None
            logger.debug(f"error on api_coin_purchase({itype}, {iid}): {e}")
        return data

    def api_purchase_prices(self):
        param = urllib.parse.urlencode({'Game': 'Nebulous'})
        resp = self.__api_generic_request("GetPurchasingPrices", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            data = None
            logger.debug(f"error on api_purchase_prices: {e}")
        return data

    def api_report_account(self, uid, typ='SPAM', aux_data='', msg=''):
        """
        :param uid:
        :param typ:
        :param aux_data:
        :param msg: must be one of those:
        | HARASSMENT
        | THREATS
        | SPAM
        | OTHER
        | INAPPROPRIATE_SKIN
        | MAIL
        :return:
        """
        param = urllib.parse.urlencode({'Game': 'Nebulous',
                                        'AccountId': uid,
                                        'Ticket': self.ticket,
                                        'ReportType': typ,
                                        'AuxData': aux_data,
                                        'Message': msg})
        resp = self.__api_generic_request("Report", param)
        try:
            data = json.loads(resp.read().decode())
        except Exception as e:
            data = None
            logger.debug(f"error on api_report_account({uid}, {typ}, "
                         f"{aux_data}, {msg}): {e}")
        return data
