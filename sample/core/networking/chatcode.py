import binascii
import enum
import logging
import random
import socket
import struct
import threading
import time

from sample.misc.utils import random_chars
from .game_values import *
from .modified_utf8 import utf8m_to_utf8s as from_jstr
from .modified_utf8 import utf8s_to_utf8m as to_jstr
from .statistics import Statistics

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ConnState(enum.Enum):
    NOT_CONNECTED = 0
    DISCONNECTED = 1
    CONNECTED_AWAITING = 2
    CONNECTED_INROOM = 3
    ATTEMPTING_TO_CONNECT = 4
    ATTEMPTING_TO_JOIN = 5


class NebChatcode:
    connection_timeout = 10
    buffsize = 4096
    hidden_room_max_ppl = 20  # there is no way to know. it could be 2 or 20
    server_parser_update_interval = 0.25
    check_player_state_timeout = 10
    duplicate_list_size = 5
    limit_of_inactivity_in_secs = 600

    @staticmethod
    def _on_public_msg(nick, uid, msg, msgid):
        logger.debug(f"public message: {msgid}-{nick}[{uid}]: {msg}")

    @staticmethod
    def _on_private_msg(nick, sid, rid, msg, msgid):
        logger.debug(f"private message: {msgid}-{nick}[{sid}→{rid}]: {msg}")

    @staticmethod
    def _on_friend_msg(sid, rid, msg, msgid):
        logger.debug(f"friend message: ({msgid})[{sid}→{rid}]: {msg}")

    @staticmethod
    def _on_clan_msg(nick, uid, msg, role, msgid):
        logger.debug(f"friend message: {msgid}-{nick}[{role}-{uid}]: {msg}")

    @staticmethod
    def __default_callback_room(uid, nick, room, who='', act=''):
        logger.debug(f"{who}{act}: {nick}[{uid}] on '{room}'")

    @staticmethod
    def _on_user_join(uid, nick, room):
        NebChatcode.__default_callback_room(uid, nick, room, 'player ', 'join')

    @staticmethod
    def _on_user_leave(uid, nick, room):
        NebChatcode.__default_callback_room(uid, nick, room, 'player ', 'leave')

    @staticmethod
    def _on_self_join(uid, nick, room):
        NebChatcode.__default_callback_room(uid, nick, room, '', 'joined')

    @staticmethod
    def _on_self_leave(uid, nick, room):
        NebChatcode.__default_callback_room(uid, nick, room, '', 'left')

    @staticmethod
    def _on_connection_change(state):
        logger.debug(f"connection state: {state}")

    @staticmethod
    def _on_online_users_result(result):
        logger.debug(f"online users: {result}")

    @staticmethod
    def _on_lobby_update(room_status: dict):
        logger.debug(f"lobby: {room_status}")

    @staticmethod
    def _on_room_join_error(reason):
        try:
            logger.debug(f"room join error: {JoinResult(reason)}")
        except ValueError:
            logger.warning(f"room join error: unknown reason {reason}")

    def __init__(self, account_id=0):
        self.server = Servers.SERVER_BR
        self.connected = False
        self.stats = Statistics()
        self.nickname = ""
        self.__busy = False
        self.__uid = account_id
        self.__port = random.randint(27900, 27901)  # port is always 27900 or 27901
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__sock.settimeout(NebChatcode.connection_timeout)
        self.__connection_id = random.randint(0, 0xffffffff)
        self.__conn_id_1 = 0
        self.__conn_id_2 = 0
        self.__connection_state = ConnState.NOT_CONNECTED
        self.__online_users_result: {int: PlayerStatusType} = {}
        self.room_list: {0: {'': ''}} = {}  # {0: {'name': '', 'current': 0, 'limit': 20}}
        self.userlist: {0: ''} = {}  # id, nickname
        self.enable_extra_filtering = False

        self.on_online_users_result = self._on_online_users_result
        self.on_connection_change = self._on_connection_change
        self.on_lobby_update = self._on_lobby_update
        self.on_public_msg = self._on_public_msg
        self.on_private_msg = self._on_private_msg
        self.on_friend_msg = self._on_friend_msg
        self.on_clan_msg = self._on_clan_msg
        self.on_user_join = self._on_user_join
        self.on_user_leave = self._on_user_leave
        self.on_self_enter = self._on_self_join
        self.on_self_leave = self._on_self_leave
        self.on_self_enter_error = self._on_room_join_error

        self.__room_state = (0, "", 0, 0)  # net response, name, max ppl, room id
        self.__last_room_name = ""  # fixes a bug
        self.__pendent_user_status_request = 0, False  # (timeout, bool)
        self.__last_udp_data = []  # used when enable_extra_filtering=True
        self.__packet_callbacks: [(str, MsgType, function, bool)] = []  # ident, type, callback, once
        return

    def __del__(self):
        self.__sock.close()

    @property
    def uid(self):
        return self.__uid

    @property
    def connection_state(self):
        return self.__connection_state

    @connection_state.setter
    def connection_state(self, state: ConnState):
        if state == ConnState.DISCONNECTED and \
                self.__connection_state == ConnState.NOT_CONNECTED:
            return
        if state != self.__connection_state:
            self.__connection_state = state
            self.on_connection_change(state)

    @property
    def online_users_result(self):
        return self.__online_users_result

    @property
    def room_status(self):
        return {"name": self.__room_state[1],
                "last_room": self.__last_room_name,
                "response": self.__room_state[0],
                "max_people": self.__room_state[2],
                "room_id": self.__room_state[3],
                "current_people": len(self.userlist)}

    def __process_packets_callbacks(self, packet_type, data):
        try:
            for ident, mtype, func, call_once in self.__packet_callbacks:
                if mtype.value == packet_type:
                    func(ident, data)
                    if call_once:
                        self.remove_packet_callback(ident)
                    logger.debug(f"exec callback: ident:{ident} mtype:{mtype} "
                                 f"func:{func} call_once:{call_once}")
        except Exception as er:
            logger.warning("error running callback: ", er)
        return

    def remove_packet_callback(self, ident: str):
        for packet_callback in self.__packet_callbacks:
            if packet_callback[0] == ident:
                self.__packet_callbacks.remove(packet_callback)
        return

    def packets_callbacks(self):
        """
        :return: returns all callbacks
        """
        return [(ident, msgtype) for ident, msgtype, _, _
                in self.__packet_callbacks]

    def add_packet_callback(self, ident: str, msgtype: MsgType, func,
                            call_once=True, replace=False):
        """
        This method is mainly used for debugging purposes

        :param ident: Callback's name
        :param msgtype: A MsgType used for catching a specific packet, None to catch all
        :param func: The function called. Must have these 2 params: ident, data
        :param call_once: If True, the callback is deleted after being called once. Default is True
        :param replace: Replaces current callback, if it exists. Default is False
        :return: None
        """
        t = (ident, msgtype, func, call_once)
        if ident in [i[0] for i in self.__packet_callbacks]:
            if not replace:
                raise Exception("ident already exists")
            else:
                self.remove_packet_callback(ident)
                self.__packet_callbacks.append(t)
                return
        if t not in self.__packet_callbacks:
            self.__packet_callbacks.append(t)
        return

    def __filter_duplicate_net_data(self, data):
        if len(self.__last_udp_data) > NebChatcode.duplicate_list_size:
            self.__last_udp_data.pop(0)
        if data not in self.__last_udp_data:
            self.__last_udp_data.append(data)
            return False
        return True

    def _nick_from_id(self, uid):
        if uid in self.userlist:
            return self.userlist[uid]
        return ""

    def has_pending_online_users_request(self):
        """
        Needed to have a precise way to know when the info has arrived

        :return: bool
        """
        if time.time() - self.__pendent_user_status_request[0] \
                >= NebChatcode.check_player_state_timeout \
                and self.__pendent_user_status_request[1]:
            self.__pendent_user_status_request = 0, False
            logger.info(f"account: {self.uid} user_status_request timeout")
            return False
        if not self.connected:
            self.__pendent_user_status_request = 0, False
            return False
        return self.__pendent_user_status_request[1]

    def room_max_ppl(self, chatname):
        for x in self.room_list:
            if self.room_list[x]['name'] == chatname:
                return self.room_list[x]['limit']
        return NebChatcode.hidden_room_max_ppl  # if joining in a hidden room

    def available_rooms(self):
        for x in self.room_list:
            if self.room_list[x]['current'] < self.room_list[x]['limit']:
                yield self.room_list[x]['name']

    def __make_null_room_state(self):
        self.__room_state = 0, "", 0, 0
        self.room_list.clear()
        self.userlist.clear()
        return

    def _send_data(self, data):
        """
        Sends raw data to the game server, the data is assumed to be properly
        aligned and formatted according to the game's packets' structures
        """
        try:
            self.__sock.send(data)
        except InterruptedError as er:
            logger.warning(f"error sending data. {er}")
            self.disconnect()
        except OSError as er:
            logger.warning(f"error sending data. {er}")
            self.disconnect()
        return

    def __reconfig_socket(self):
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__sock.settimeout(NebChatcode.connection_timeout)

    def disconnect(self, gentle=False):
        """
        :param gentle:
        :return:
        |
        | DISCONNECT                        :1byte
        | server_response_bytes1            :4bytes
        | server_response_bytes2            :4bytes
        | connection_session_token_random   :4bytes

        """
        if gentle and self.connected:  # if it timed out, then its not gentle
            data = struct.pack("!BIII",
                               MsgType.DISCONNECT.value,
                               self.__conn_id_1,
                               self.__conn_id_2,
                               self.__connection_id)
            self.__sock.send(data)
        try:
            self.__sock.shutdown(socket.SHUT_RDWR)
            self.__sock.close()
        except Exception:
            pass
        self.connection_state = ConnState.DISCONNECTED
        self.__make_null_room_state()
        if self.connected:
            self.connected = self.__busy = False
        return

    def _parse_lobby_list_result(self, data):
        """
        :param data:
        :return:
        |
        | GROUP_LOBBY_LIST_RESULT           :1byte
        | number_of_rooms                   :1byte
        | repeat:
        |     room_id                       :4bytes
        |     people_in_room                :1byte
        |     max_people                    :1byte
        |     raw_bytes                     :2bytes:  [00 00]
        |     room_name_length              :2bytes
        |     room_name_utf8_bytes          :room_name_length bytes

        """
        rooms, = struct.unpack_from("!b", data, 1)
        # skip first 2 bytes, they are useless for me
        offset = 2
        fresh_list = {}  # ([str, int, int])
        for i in range(0, rooms):
            room_id, current, maxppl, _, slen = struct.unpack_from("!IBBHH", data, offset)
            offset += 10
            room_name, = struct.unpack_from(f"!{slen}s", data, offset)
            offset += slen
            fresh_list[room_id] = {
                'name': from_jstr(room_name).decode("utf-8", "replace"),
                'current': current,
                'limit': maxppl
            }
        self.on_lobby_update(fresh_list)
        self.room_list = fresh_list
        return

    def _parse_lobby_join_result(self, data):
        """
        :param data:
        :return:
        |
        | GROUP_LOBBY_JOIN_RESULT           :1byte
        | join result                       :4bytes
        | join type                         :1byte
        | mode                              :1byte
        | size                              :1byte
        | maxppl                            :4bytes
        | curppl                            :4bytes
        | roomid                            :4bytes
        | roomnamelen                       :2bytes
        | roomname                          :roomnamelen bytes
        | unknown_bool                      :1byte
        | user_id                           :4bytes

        """
        offset = 1
        join_result, = struct.unpack_from("!I", data, offset)
        if join_result == JoinResult.SUCCESS.value:
            offset += 4
            _, _, _, _, _, rid, namelen = struct.unpack_from("!bbbIIIH",
                                                             data, offset)
            offset += 17
            chatname, = struct.unpack_from(f"!{namelen}s", data, offset)
            chatname = from_jstr(chatname).decode("utf8", "replace")
        else:
            logger.info(f"join failed: {JoinResult(join_result)}")
            self.__room_state = join_result, "", 0, 0
            # self.__connection_state = ConnState.ATTEMPT_TO_JOIN_FAIL
            return False
        if rid in self.room_list:
            maxppl = self.room_list[rid]['limit']
        else:
            maxppl = self.hidden_room_max_ppl
        self.__room_state = join_result, chatname, maxppl, rid
        return True

    def _parse_lobby_create_result(self, data):
        """
        :return:
        |
        | GROUP_LOBBY_CREATE_RESULT
        | JoinResult
        | userid

        """
        return

    def _parse_chat_result(self, data):
        """
        :param data:
        :return:
        |
        | GROUP_LOBBY_UPDATE                :1byte
        | raw bytes                         :4bytes:  [BF 80 00 00]
        | unknown                           :1byte
        | repeat:
        |     string_nickname_length        :2bytes
        |     string_nickname               :string_nickname_length bytes
        |     id                            :4bytes
        |     colors_count                  :1byte
        |     color_pallete_bytes           :colors_count bytes

        """
        nchats, = struct.unpack_from("!b", data, 5)
        offset = 6
        fresh_list = {}  # {user_id: player_nick}
        for i in range(0, nchats):
            slen, = struct.unpack_from("!H", data, offset)
            offset += 2
            nick, pallete_len = struct.unpack_from(f"!{slen}sB", data, offset)
            offset += slen + 1
            pallete_data, player_id = struct.unpack_from(f"!{pallete_len}sI",
                                                         data, offset)
            offset += pallete_len + 4
            fresh_list[player_id] = from_jstr(nick).decode("utf8", "replace")
        self.userlist = fresh_list
        # BUGFIX: rejoin accidentally
        if self.__room_state[1] == b"":
            self.__room_state = JoinResult.SUCCESS.value, \
                                self.__last_room_name,\
                                self.room_max_ppl(self.__last_room_name), 0
        return

    def _parse_chat_message(self, data):
        """
        :param data:
        :return:
        |
        | PUBLIC_CHAT_MESSAGE               :1byte
        | server_response_bytes1            :4bytes
        | string_nickname_length            :2bytes
        | string_nickname                   :string_nickname_length bytes
        | string_message_length             :2bytes
        | string_message                    :string_message_length bytes
        | player_id                         :4bytes
        | value -1                          :4bytes:  [80 00 00 00]
        | message_id                        :4bytes

        """
        offset = 1
        unused, nicklen = struct.unpack_from("!IH", data, offset)
        offset += 6
        nickname, msglen = struct.unpack_from(f"!{nicklen}sH", data, offset)
        offset += nicklen + 2
        message, player_id, nul, msgid = struct.unpack_from(f"!{msglen}sIII", data, offset)

        themessage = (from_jstr(nickname).decode("utf-8", "replace"),
                      player_id,
                      from_jstr(message).decode("utf-8", "replace"),
                      msgid)

        if player_id != self.uid:
            self.stats.recv_from_public += 1
        self.on_public_msg(*themessage)
        return

    def _parse_clan_message(self, data):
        """
        :param data:
        :return:
        |
        | CLAN_CHAT_MESSAGE                 :1byte
        | server_response_bytes1            :4bytes
        | nick_len                          :2bytes
        | nickname                          :nick_len bytes
        | msg_len                           :2bytes
        | message                           :msg_len bytes
        | user_clan_role                    :1byte
        | player_id                         :4bytes
        | value -1                          :4bytes:  [80 00 00 00]
        | message_id                        :4bytes

        """
        offset = 1
        _, nicklen = struct.unpack_from("!IH", data, offset)
        offset += 6
        nickname, msglen = struct.unpack_from(f"!{nicklen}sH", data, offset)
        offset += nicklen + 2
        message, clan_role, player_id, _, msgid = struct.unpack_from(
            "!" + str(msglen) + "sBIII", data, offset)
        if player_id == 0xffffffff:
            player_id = 0
        themessage = (from_jstr(nickname).decode("utf-8", "replace"),
                      player_id,
                      from_jstr(message).decode("utf-8", "replace"),
                      clan_role,
                      msgid)

        if not player_id == self.uid:
            self.stats.recv_from_clan += 1
        self.on_clan_msg(*themessage)
        return

    def _parse_private_message(self, data):
        """
        :param data:
        :return:
        |
        | PRIVATE_CHAT_MESSAGE              :1byte
        | server_response_bytes1            :4bytes
        | sender_id                         :4bytes
        | receiver_id                       :4bytes
        | nick_strlen                       :2bytes
        | nick                              :strlen bytes
        | msg_strlen                        :2bytes
        | message                           :strlen bytes
        | unknown                           :4bytes
        | msg_identifier                    :4bytes

        """
        offset = 5
        sender_id, receiver_id, nicklen = struct.unpack_from("!IIH", data, offset)
        offset += 10
        sender_nick, = struct.unpack_from(f"!{nicklen}s", data, offset)
        offset += nicklen
        msglen, = struct.unpack_from("!H", data, offset)
        offset += 2
        message, _, msgid = struct.unpack_from(f"!{msglen}sII", data, offset)
        themessage = (from_jstr(sender_nick).decode("utf-8", "replace"),
                      sender_id,
                      receiver_id,
                      from_jstr(message).decode("utf-8", "replace"),
                      msgid)

        if not sender_id == self.uid:
            self.stats.recv_from_private += 1
        self.on_private_msg(*themessage)
        return

    def _parse_friend_message(self, data):
        """
        :param data:
        :return:
        |
        | FRIEND_CHAT_MESAGE                :1byte
        | server_response_bytes1            :4bytes
        | sender_id                         :4bytes
        | receiver_id                       :4bytes
        | msg_strlen                        :2bytes
        | message                           :strlen bytes
        | unknown                           :4bytes:  [80 00 00 00]
        | message_id                        :4bytes

        """
        offset = 5
        sender_id, receiver_id, msglen = struct.unpack_from("!IIH", data, offset)
        offset += 10
        message, = struct.unpack_from(f"!{msglen}s", data, offset)
        offset += msglen
        _, msgid = struct.unpack_from("!II", data, offset)
        themessage = (sender_id,
                      receiver_id,
                      from_jstr(message).decode("utf-8", "replace"),
                      msgid)

        if not sender_id == self.uid:
            self.stats.recv_from_friend += 1
        self.on_friend_msg(*themessage)
        return

    def _parse_check_online_users(self, data):
        """
        :param data:
        :return:
        |
        | ACCOUNT_STATUS_RESULT             :1byte
        | online_users_count                :1byte
        | repeat:
        |     user_id                       :4bytes
        |     PlayerStatusType              :1byte
        |     unknown_byte                  :1byte
        |     Server number                 :2bytes

        """
        offset = 1
        count, = struct.unpack_from("!B", data, offset)
        offset += 1
        self.__online_users_result.clear()
        self.__pendent_user_status_request = 0, False
        for i in range(0, count):
            uid, status, _, server = struct.unpack_from("!IBBH", data, offset)
            offset += 8
            # for some reason it is needed
            # i dont really know why the creator of the game uses bitwise
            status = status & 127
            self.__online_users_result[uid] = status
        self.on_online_users_result(self.__online_users_result)
        return

    def _disconnect_if_too_inactive(self):
        if self.stats.inactivity_time > NebChatcode.limit_of_inactivity_in_secs:
            logger.info("Disconnecting due to inactivity")
            self.stats.update_inactivity_time(True)
            self.disconnect(True)
            return True
        self.stats.update_inactivity_time()
        return False

    def __clear_old_userlist(self, old_userlist):
        if old_userlist:
            for i in old_userlist:
                self.on_user_leave(i, old_userlist[i],
                                   self.__last_room_name)
        old_userlist.clear()

    def _server_reply_parser(self):
        self.stats.update_inactivity_time(True)
        self.stats.update_uptime(True)
        old_userlist = {}  # used to know who joined or left
        while self.connected:
            time.sleep(NebChatcode.server_parser_update_interval)
            try:
                data = self.__sock.recv(NebChatcode.buffsize)
                if self.enable_extra_filtering:
                    if self.__filter_duplicate_net_data(data):
                        continue
            except OSError as e:
                logger.debug(f"disconnected. {e}")
                self.__clear_old_userlist(old_userlist)
                self.disconnect()
                break
            self.stats.update_uptime()
            if self._disconnect_if_too_inactive():
                break

            packet_type, = struct.unpack_from("!b", data)
            self.__process_packets_callbacks(packet_type, data)
            if len(data) > 2:
                if packet_type == MsgType.GROUP_LOBBY_JOIN_RESULT.value:
                    if self._parse_lobby_join_result(data):
                        self.on_self_enter(self.uid, self.nickname,
                                           self.__room_state[1])
                    else:
                        self.on_self_enter_error(self.__room_state[0])

                elif packet_type == MsgType.GROUP_LOBBY_LIST_RESULT.value:
                    self.connection_state = ConnState.CONNECTED_AWAITING
                    self._parse_lobby_list_result(data)
                    self.__clear_old_userlist(old_userlist)

                elif packet_type == MsgType.GROUP_LOBBY_UPDATE.value:
                    self._parse_chat_result(data)
                    self.connection_state = ConnState.CONNECTED_INROOM
                    joins = [i for i in self.userlist if i not in old_userlist]
                    leaves = [i for i in old_userlist if i not in self.userlist]
                    if joins or leaves:
                        self.stats.update_statistics(False)
                    for i in joins:
                        self.on_user_join(i, self.userlist[i], self.__room_state[1])
                    for i in leaves:
                        self.on_user_leave(i, old_userlist[i], self.__last_room_name)

                    old_userlist = self.userlist.copy()

                elif packet_type == MsgType.PUBLIC_CHAT_MESSAGE.value:
                    self.stats.update_inactivity_time(True)
                    self.stats.update_statistics(True)
                    self._parse_chat_message(data)

                elif packet_type == MsgType.PRIVATE_CHAT_MESSAGE.value:
                    self.stats.update_inactivity_time(True)
                    self.stats.update_statistics(True)
                    self._parse_private_message(data)

                elif packet_type == MsgType.FRIEND_CHAT_MESAGE.value:
                    self.stats.update_inactivity_time(True)
                    self.stats.update_statistics(False)
                    self._parse_friend_message(data)

                elif packet_type == MsgType.CLAN_CHAT_MESSAGE.value:
                    self.stats.update_inactivity_time(True)
                    self.stats.update_statistics(False)
                    self._parse_clan_message(data)

                elif packet_type == MsgType.GAME_UPDATE.value:
                    logger.info("GAME_UPDATE - disconnecting")
                    self.disconnect(True)

                elif packet_type == MsgType.GAME_DATA.value:
                    logger.info(f"{self.uid} joined in game accidentally, disconnecting")
                    self.disconnect(True)

                elif packet_type == MsgType.TOURNEY_STATUS_UPDATE.value:
                    pass  # error prone

                elif packet_type == MsgType.CONNECT_RESULT_2.value:
                    pass  # error prone

                elif packet_type == MsgType.GROUP_LOBBY_LEAVE.value:
                    self.__last_room_name = self.__room_state[1]
                    self.__room_state = JoinResult.SUCCESS.value, "", 0, 0
                    self.on_self_leave(self.uid, self.nickname, self.__last_room_name)

                elif packet_type == MsgType.ACCOUNT_STATUS_RESULT.value:
                    self._parse_check_online_users(data)
                else:
                    try:
                        logger.warning(f"unhandled server reply: {MsgType(packet_type)}")
                    except ValueError:
                        logger.warning(f"unhandled unknown server reply: {str(packet_type)}")
                    logger.warning(f"received bytes: {str(data)}")
        return

    def send_user_online_check(self, uids):
        """
        :param uids:
        :return:
        |
        | ACCOUNT_STATUS_REQUEST            :1byte
        | server_response_bytes1            :4bytes
        | number_of_users_ids               :1byte
        | repeat number_of_users_ids times:
        |    user_id                       :4bytes

        """
        if self.__pendent_user_status_request[1]:
            # return if there is already a pendent user's status request
            return
        count = len(uids)
        fmt = "!bIB" + "I" * count
        data = struct.pack(fmt, MsgType.ACCOUNT_STATUS_REQUEST.value,
                           self.__conn_id_1,
                           count,
                           *uids)

        self._send_data(data)
        self.__pendent_user_status_request = time.time(), True
        return

    def send_chat_message(self, text):
        """
        :param text:
        :return:
        |
        | PUBLIC_CHAT_MESSAGE               :1byte
        | server_response_bytes1            :4bytes
        | unknown_bytes                     :2bytes:  [00 00]
        | string_message_length             :2bytes
        | string_message                    :string_message_length bytes
        | raw_bytes                         :4bytes:  [ff ff ff ff]
        | raw_bytes                         :2bytes:  [00 00]
        | raw_bytes                         :4bytes:  [00 00 00 00]
        | raw_bytes                         :2bytes:  [00 00]

        """
        sendtext = to_jstr(text.encode("utf-8"))
        txtlen = len(sendtext)
        fmt = "!bIHH{0}sIII".format(txtlen)
        data = struct.pack(fmt,
                           MsgType.PUBLIC_CHAT_MESSAGE.value,
                           self.__conn_id_1,
                           0,
                           txtlen,
                           sendtext,
                           0xffffffff,
                           0,
                           0)

        self._send_data(data)
        self.stats.sent_to_public += 1
        return

    def send_private_message(self, text, receiver_id):
        """
        :param text:
        :param receiver_id:
        :return:
        |
        | PRIVATE_CHAT_MESSAGE              :1byte
        | server_response_bytes1            :4bytes
        | sender_id                         :4bytes:  [FF FF FF FF]
        | receiver_id                       :4bytes
        | null_str_len                      :2bytes
        | msg_strlen                        :2bytes
        | message                           :strlen bytes
        | null_bytes                        :4bytes:  [00 00 00 00]
        | null_bytes                        :4bytes:  [00 00 00 00]

        """
        sendtext = to_jstr(text.encode("utf-8"))
        txtlen = len(sendtext)
        fmt = f"!bIIIHH{txtlen}sII"
        data = struct.pack(fmt,
                           MsgType.PRIVATE_CHAT_MESSAGE.value,
                           self.__conn_id_1,
                           0xFFFFFFFF,
                           receiver_id,
                           0,
                           txtlen,
                           sendtext,
                           0,
                           0)

        self._send_data(data)
        self.stats.sent_to_private += 1
        return

    def send_clan_message(self, text):
        """
        :param text:
        :return:
        |
        | CLAN_CHAT_MESSAGE                 :1byte
        | server_response_bytes1            :4bytes
        | unknown bytes                     :2bytes:  [00 00]
        | msg_len                           :2bytes
        | message                           :msg_len bytes
        | unknown byte                      :1byte
        | fixed bytes                       :4bytes:  [FF FF FF FF]
        | unknown bytes                     :4bytes:  [00 00 00 00]
        | unknown bytes                     :4bytes:  [00 00 00 00]

        """
        sendtext = to_jstr(text.encode("utf-8"))
        txtlen = len(sendtext)
        fmt = f"!bIHH{txtlen}sBIII"
        data = struct.pack(fmt,
                           MsgType.CLAN_CHAT_MESSAGE.value,
                           self.__conn_id_1,
                           0,
                           txtlen,
                           sendtext,
                           0,
                           0xffffffff,
                           0,
                           0)

        self._send_data(data)
        self.stats.sent_to_clan += 1
        return

    def send_friend_message(self, text, receiver_id):
        """
        :param text:
        :param receiver_id:
        :return:
        |
        | FRIEND_CHAT_MESAGE                :1byte
        | server_response_bytes1            :4bytes
        | sender_id                         :4bytes   [FF FF FF FF]
        | receiver_id                       :4bytes
        | msg_strlen                        :2bytes
        | message                           :strlen bytes
        | null_bytes                        :4bytes:  [00 00 00 00]
        | null_bytes                        :4bytes:  [00 00 00 00]

        """
        sendtext = to_jstr(text.encode("utf-8"))
        txtlen = len(sendtext)
        fmt = f"!bIIIH{txtlen}sII"
        data = struct.pack(fmt,
                           MsgType.FRIEND_CHAT_MESAGE.value,
                           self.__conn_id_1,
                           0xFFFFFFFF,
                           receiver_id,
                           txtlen,
                           sendtext,
                           0,
                           0)

        self._send_data(data)
        self.stats.sent_to_friend += 1
        return

    def send_room_create_request(self, name, nick, maxppl, hidden):
        """
        :param name: room name
        :param nick: user nickname
        :param maxppl: limit of people in the room
        :param hidden: visible in public or not
        :return:
        |
        | GROUP_LOBBY_CREATE_REQUEST        :1byte
        | server_response_bytes1            :4bytes
        | hidden?                           :1byte
        | max_people                        :1byte
        | unknown                           :1byte
        | game_mode=FFA                     :1byte:   [00]
        | game_size=NORMAL                  :1byte:   [02]
        | create_type=CHAT                  :1byte:   [03]
        | AI_mode=EASY                      :1byte:   [00]
        | room_name_len                     :2bytes
        | room_name                         :room_name_len  bytes
        | str_nick_len                      :2bytes
        | str_nick                          :str_nick_len  bytes
        | raw_float                         :4bytes:  [41 7A 66 66]
        | unknown_short                     :2bytes
        | unknown_short                     :2bytes
        | unknown_bool                      :1byte

        """
        unencoded_name = name

        colorlen = len(nick)
        name = to_jstr(name.encode())
        nick = to_jstr(nick.encode())

        hidden = 1 if hidden else 0

        fmt = f"!bIbbbbbbbH{len(name)}sH{len(nick)}sBBBBBBBBBH{len(nick)}s"
        logger.debug(f"creating{' hidden' if hidden else ''} room '{name}' "
                     f"using nick '{nick}' with {maxppl} "
                     "limit of people")
        data = struct.pack(fmt,
                           MsgType.GROUP_LOBBY_CREATE_REQUEST.value,
                           self.__conn_id_1,
                           hidden,
                           maxppl,
                           0, 0, 2, 3, 0,
                           len(name),
                           name,
                           len(nick),
                           nick,
                           0x41, 0x7A, 0x66, 0x66, 0x01, 0xF8, 0x00, 0x3C, 0x01,
                           colorlen,
                           NebChatcode._random_nick_colors(colorlen))

        self._send_data(data)
        self.__room_state = 0, unencoded_name, maxppl, 1
        self.__last_room_name = unencoded_name
        return

    @staticmethod
    def _random_nick_colors(nicklen):
        nickcolor = random.randint(0, 255)
        return bytes([nickcolor for _ in range(nicklen)])

    def send_room_join_request(self, room=None, nick=random_chars()):
        """
        join in a room, choose a random one if room == None
        :param room: room name
        :param nick: user nickname
        :return:
        |
        | GROUP_LOBBY_JOIN_REQUEST          :1byte
        | string_length_room_name           :2bytes
        | string_room_name                  :string_length_room_name bytes
        | string_nickname_length            :2bytes
        | string_nickname                   :string_nickname_length bytes

        """
        if nick == " ":
            nick = self.nickname
        else:
            self.nickname = nick
        if room is None:
            rooms = list(self.available_rooms())
            if not rooms:
                logger.warning("no rooms available to join randomly")
                return
            room = random.choice(rooms)
        colorlen = len(nick)
        nick = to_jstr(nick.encode("utf-8"))
        nicklen = len(nick)
        self.__last_room_name = room
        room = to_jstr(room.encode("utf-8"))
        roomlen = len(room)
        fmt = f"!bIH{roomlen}sH{nicklen}sb{colorlen}s"
        data = struct.pack(fmt,
                           MsgType.GROUP_LOBBY_JOIN_REQUEST.value,
                           self.__conn_id_1,
                           roomlen,
                           room,
                           nicklen,
                           nick,
                           colorlen,
                           NebChatcode._random_nick_colors(colorlen))
        self.connection_state = ConnState.ATTEMPTING_TO_JOIN
        self._send_data(data)
        logger.debug(f"send_room_join_request bytes: {binascii.hexlify(data)}")
        return

    def send_lobby_list_request(self):
        """
        :return: None
        |
        | GROUP_LOBBY_LIST_REQUEST          :1byte
        | server_response_bytes1            :4bytes
        | raw_bytes                         :2bytes: [00 00]
        | raw_byte                          :1byte:  [14]
        | chat_type                         :1byte

        """
        data = struct.pack("!bIHBBIB",
                           MsgType.GROUP_LOBBY_LIST_REQUEST.value,
                           self.__conn_id_1,
                           0,
                           0x0a,  # figure out wtf is this param, version??
                           JoinType.CHAT,
                           0xffffffff, 0xff)
        self._send_data(data)
        return

    def send_lobby_leave_request(self):
        """
        :return: None
        |
        | GROUP_LOBBY_LEAVE                 :1byte
        | server_response_bytes1            :4bytes
        """
        data = struct.pack("!BI", MsgType.GROUP_LOBBY_LEAVE.value, self.__conn_id_1)
        self._send_data(data)
        self.__make_null_room_state()

    def __keep_alive_sender(self, reply, server):
        """
        :param reply:
        :param server:
        :return:
        |
        | KEEP_ALIVE                        :1byte
        | server_response_bytes1            :4bytes
        | server_response_bytes2            :4bytes
        | server_ip_inverted                :4bytes

        """
        time.sleep(1)
        try:
            # get server connection ids
            self.__conn_id_1, self.__conn_id_2 = struct.unpack_from('II', reply, 6)
            self.__conn_id_1 = socket.htonl(self.__conn_id_1)  # big endian
            self.__conn_id_2 = socket.htonl(self.__conn_id_2)
        except Exception as er:
            logger.warning("error in keep_alive_sender:", er)
            logger.debug("data dump:\n", reply)
            self.disconnect()
            return

        data = struct.pack("!bII4s",
                           MsgType.KEEP_ALIVE.value,
                           self.__conn_id_1,
                           self.__conn_id_2,
                           socket.inet_aton(server)[::-1])  # reverse server ip

        if self.connected:
            # this looks very ugly, im just imitating the app
            self._send_data(data)
        else:
            logger.warning("disconnected right after connecting")
            return
        time.sleep(0.1)
        self.send_lobby_list_request()
        time.sleep(1)

        while self.connected:
            self._send_data(data)
            time.sleep(1)

        self.__reconfig_socket()
        return

    def __connect_proc(self, data, server):
        """
        :param data: CONNECT_REQUEST packet data
        :param server: ipv4 server string
        :return:
        |
        | CONNECT_RESULT_2                  :1byte
        | unknown_random_int                :4bytes
        | connection_result                 :1byte
        | server_response_bytes1=token1     :4bytes
        | server_response_bytes2=token2     :4bytes
        | unknown_int                       :4bytes: [ff ff ff ff]
        | unknown_float                     :4bytes: [01 0e 9a eb]
        | unknown_byte                      :1byte:  [3f]
        | unknown_unknown                   :4bytes  [80 00 00 10]
        """

        try:
            self.__reconfig_socket()
            self.__sock.connect((server, self.__port))
            self._send_data(data)
            recvdata = self.__sock.recv(NebChatcode.buffsize)
        except Exception as e:
            logger.warning(f"error in connect_proc: {e}")
            self.disconnect()
            self.__busy = False
            return
        logger.debug(f"connected. received bytes: {binascii.hexlify(recvdata)}")
        self.connected = True

        logger.debug("starting thread for keep_alive_sender")
        t1 = threading.Thread(target=self.__keep_alive_sender, args=(recvdata, server))
        t1.start()
        t1.setName(f"{self.uid}-keep_alive_sender")

        logger.debug("starting thread for server_reply_parser")
        t2 = threading.Thread(target=self._server_reply_parser)
        t2.start()
        t2.setName(f"{self.uid}-server_reply_parser")
        self.__busy = False
        return

    def connect(self, nickname, ticket, server):
        """
        :param nickname: user nickname
        :param ticket: connection ticket
        :param server: ipv4 server string
        :return:

        |
        | CONNECT_REQUEST                   :1byte
        | raw_bytes                         :4bytes:  [00 00 00 00]
        | connection_session_token_random   :4bytes
        | raw_bytes                         :2bytes:  [00 01]
        | raw_bytes                         :4bytes:  [ff ff ff ff]
        | str_len                           :2bytes:  [00 00]
        | str_account_token [00]            :str_len bytes
        | is_online                         :1byte:   [00]
        | unknown_pattern_bytes             :2bytes:  [01 17]
        | raw_bool                          :1byte:   [00]
        | unknown_pattern                   :2bytes:  [01 23]
        | nick_str_len                      :2bytes
        | string_lobby_nickname             :nick_str_len  bytes
        | color_string_length               :2bytes
        | nick_color_pallete_vals           :nick_str_len  bytes

        """
        nicklen = len(to_jstr(nickname.encode()))
        self.__connection_id = random.randint(0, 0xffffffff)
        if ticket != 0xffffffff:
            str_id = ticket  # str(self.account.ticket)
            fmt = f"!bIIHIH{len(str_id)}sIHH{nicklen}sBIB{nicklen}s"
            data = struct.pack(fmt,
                               MsgType.CONNECT_REQUEST.value,
                               0, self.__connection_id,
                               0x0901, 0xffffffff,
                               len(str_id), str_id.encode(),
                               0x00014F01, 0x1a,
                               nicklen, nickname.encode(),
                               0xFF, 0,
                               nicklen, NebChatcode._random_nick_colors(nicklen))  # nick color

            if not self.__busy:
                self.__busy = True
                self.connection_state = ConnState.ATTEMPTING_TO_CONNECT
                t = threading.Thread(target=self.__connect_proc, args=(data, server))
                t.setName(f"{self.uid}-connect_proc")
                t.start()
        return
