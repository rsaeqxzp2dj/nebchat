import logging

from sample.misc.utils import random_chars
from .account import Account
from .chatcode import NebChatcode

logger = logging.getLogger(__name__)


class GameInterface:
    def __init__(self, ticket):
        self.account = Account(ticket)
        self.chat = NebChatcode(self.account.uid)
        self.__ticket = ticket

    @property
    def ticket(self):
        return self.__ticket

    @property
    def id(self):
        return self.account.uid

    def connect(self, nick=random_chars()):
        self.chat.connect(nick, self.__ticket, self.chat.server)

    def disconnect(self, gentle=True):
        self.chat.disconnect(gentle)

    def __del__(self):
        logger.debug("deleting GameInterface")
        self.chat.disconnect(True)
