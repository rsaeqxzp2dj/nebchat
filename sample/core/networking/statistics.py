import time


class Statistics:
    time_history_depth = 10

    def __init__(self):
        self.reconnections = -1
        self.sent_to_public = 0
        self.sent_to_private = 0
        self.sent_to_friend = 0
        self.sent_to_clan = 0
        self.recv_from_public = 0
        self.recv_from_private = 0
        self.recv_from_friend = 0
        self.recv_from_clan = 0
        self.uptime = 0
        self.inactivity_time = 0
        self.total_inactivity_time = 0
        self.total_uptime = 0
        self.__msg_per_sec = 0
        self.__events_per_sec = 0
        self.__events = []
        self.__messages = []
        self.__last_event = time.time()
        self.__last_message = time.time()
        self.__init_uptime = 0
        self.__last_inactivity_time = time.time()

    def update_inactivity_time(self, reset=False):
        if reset:
            self.inactivity_time = 0
            self.__last_inactivity_time = time.time()
        else:
            self.inactivity_time = time.time() - self.__last_inactivity_time
        return

    def update_uptime(self, reset_uptime=False):
        """
        Increments the timer on each call
        Must be called on each connect attempt with reset_uptime=True
        :param reset_uptime:
        :return:
        """
        if reset_uptime:
            self.__init_uptime = time.time()
            self.uptime = 0
        else:
            self.uptime = time.time() - self.__init_uptime
        return

    def update_statistics(self, is_message):
        """
        Updates events per second or messages per second statistic data
        :param is_message: should be set to True on room-dependent chat messages, and
        False for user join/leave room events. Friend or Clan messages does not apply to is_message
        because both Friend and Clan messages are not supposed to be linked within a room
        :return:
        """
        lst = self.__messages if is_message else self.__events
        last = self.__last_message if is_message else self.__last_event
        current_time = time.time()

        lst.insert(0, current_time - last)
        if len(lst) > Statistics.time_history_depth:
            lst.pop(-1)

        if is_message:
            self.__last_message = current_time
        else:
            self.__last_event = current_time
        return

    def clear_per_sec_statistics(self):
        self.__last_message = time.time()
        self.__last_event = time.time()
        self.__messages.clear()
        self.__events.clear()
        return

    @property
    def avg_msgs_per_second(self):
        return (1 / sum(self.__messages)) * len(self.__messages)

    @property
    def avg_events_per_second(self):
        return (1 / sum(self.__events)) * len(self.__events)

    @property
    def sent_messages(self):
        return self.sent_to_private + self.sent_to_friend + self.sent_to_clan

    @property
    def recv_messages(self):
        return self.recv_from_private + self.recv_from_friend + self.recv_from_clan
