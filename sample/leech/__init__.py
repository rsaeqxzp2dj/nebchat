import logging

LEECH_REPORT = 11
logging.addLevelName(LEECH_REPORT, "LEECH")


def leech(self, message, *args, **kws):
    self.log(LEECH_REPORT, message, *args, **kws)


logging.Logger.leech_report = leech
