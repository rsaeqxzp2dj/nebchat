import hashlib
import logging
import queue
import bz2
import json
import base64
import threading
from . import LEECH_REPORT
from sample.misc import utils
from sample.core.manager import Manager
from sample.core.manager import Events

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class DataLeeching:
    cache_file = utils.user_data("cache")
    profiles = queue.Queue()

    def __init__(self, manager: Manager, autostart=False):
        self.manager = manager
        self.__thread = None
        self.__stalled = []
        self.__account = ""
        self.__op_count = {}
        self.__running = False
        self.__forced_to_stop = False
        self.__recent: {int: bytes} = {}
        self._load_cache()
        if autostart:
            self.start()
        self.__set_listeners()

    @staticmethod
    def enable_verbose_logging(verbose):
        if verbose:
            logger.setLevel(LEECH_REPORT)
        else:
            logger.setLevel(logging.INFO)

    def _save_cache(self):
        with open(self.cache_file, "wb") as f:
            a85 = {z: base64.b85encode(self.__recent[z]).decode()
                   for z in self.__recent}
            dump = json.dumps(a85, ensure_ascii=False)
            logger.debug(f"dump: {dump}")
            f.write(bz2.compress(dump.encode()))

    def _load_cache(self):
        with open(self.cache_file, "rb") as f:
            dump = bz2.decompress(f.read())
            a85 = json.loads(dump, encoding='utf-8')
            logger.debug(f"a85: {a85}")
            self.__recent = {int(z): base64.b85decode(a85[z].encode())
                             for z in a85}

    # noinspection PyUnusedLocal
    def __handle_user_join(self, uid, nick, room):
        self.profiles.put(uid)

    def __set_listeners(self, unset=False):
        for account in self.manager.accounts:
            params = (Events.user_join, account, self.__handle_user_join)
            if not self.manager.has_listener(*params):
                self.manager.register_listener(*params)
            else:
                if unset:
                    self.manager.remove_listener(*params)

    def update_listeners(self):
        # set_listeners should not be called from outside
        # only Dataleeching.stop() should be used to remove the listeners
        self.__set_listeners()

    def stop(self, force=False):
        self.__set_listeners(True)
        self.__running = False
        self.__forced_to_stop = force
        self._save_cache()
        if force:
            self.profiles.put(None)

    def start(self):
        logger.leech_report("initiaizing")
        self.__thread = threading.Thread(target=self.loop)
        self.__thread.setName("leeching_thread")
        self.__running = True
        self.__thread.start()

    @property
    def account(self):
        return self.__account

    @property
    def operations(self):
        return self.__op_count[self.__account]

    def select_account(self):
        for account in self.manager.accounts:
            gi = self.manager.accounts[account]
            if gi.account.api_checkin():
                self.__account = account
                if self.__op_count.get(account) is None:
                    self.__op_count[account] = 0
                return account
        self.__account = ""

    def loop(self):
        self.select_account()
        while self.__running:
            self.step()
        logger.leech_report("shutting down")

    def gather_data(self, uid):
        uid = int(uid)
        if 22000000 < uid < 1:
            logger.leech_report(f"invalid profile id: {uid}")
            return
        gi = self.manager.accounts[self.__account]
        data = gi.account.get_player_profile(int(uid))
        if data is None:
            logger.leech_report(f"profile {uid} returned null data")
            return
        stats = data["stats_data"]
        profile = data["profile_data"]
        profile_hash = hashlib.md5(str(profile['profile']).encode()).digest()
        if self.__recent.get(uid) == profile_hash:
            return
        self.manager.save_player_profile_data(
            stats['AccountID'],
            stats['AccountName'],
            profile['profile'],
            profile['customSkinID'],
            stats['ValidCustomSkinIDs'],
            stats['XP'],
            stats['ClanName'],
            stats['CurrentCoins']
        )
        self.__recent[uid] = profile_hash
        self.__op_count[self.__account] += 1
        logger.leech_report(f"saved profile data: {uid}")

    def step(self):
        uid = self.profiles.get()
        if self.__forced_to_stop:
            return
        if self.__account not in self.manager.accounts:
            self.__account = ""
        if not self.__account:
            if not self.select_account():
                logger.warning(f"no valid account available. {uid} stalled")
                self.__stalled.append(uid)
                return
        else:
            for stalled_uid in self.__stalled:
                self.profiles.put(stalled_uid)
        if uid is None:
            return
        self.gather_data(uid)
