import logging
import time
import sys

from sample.bot.bot import Bot
from sample.misc import utils
from sample.leech.data_leeching import DataLeeching
from .core.manager import Manager
from .ui.qt5.mainwindow import MainWindow

now = time.strftime("%Y-%m-%d_%H-%M-%S")
logging.basicConfig(
    format='%(asctime)s %(name)s %(funcName)s %(threadName)-s %(levelname)s: %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[logging.StreamHandler(),
              logging.FileHandler(utils.user_data(f"log_{now}", "logs"), mode='w')])

help_msg = """
options:
\t-no-leeching: Disable leeching.
\t-no-bot: Disable bot. (No effect)
\t-no-gui: Run without Qt5. Note that this options makes the software unusable
"""


class Main:
    def __init__(self, args):
        if "--help" in args:
            print(help_msg)
            return
        self.manager = Manager()
        self.manager.load_session()
        use_leeching = "-no-leeching" not in args
        use_bot = "-no-bot" not in args
        if "-no-gui" not in args:
            self.ui = MainWindow(self.manager, use_leeching, use_bot)
        else:
            if use_leeching:
                self.leeching = DataLeeching(self.manager, True)
            if use_bot:
                self.bot = Bot(self.manager)


if __name__ == "__main__":
    Main(sys.argv)
