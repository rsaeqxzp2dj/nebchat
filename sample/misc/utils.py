from enum import Enum
import logging
import os
import re
import random
import webbrowser

import resources.PyQt5UI
import resources.database

logger = logging.getLogger(__name__)
links_re = re.compile("(https?://www\.|https?://|www\.)(.*)\.(a[c-z]|b(iz|r)|c("
                      "[ah]|om)|de|e(du|s)|fr|gov|in|info|int|jobs|jp|ly|m(il|o"
                      "bi|x)|n(ame|et|l|o)|org|ru|se|tel|u[ks])", re.IGNORECASE)
dot_re = re.compile("(.*)dot(.*)")


class RandomNickOptions(Enum):
    Male = 0
    Female = 1
    Both = 2
    Neither = 3


def random_chars(m=5, x=16):
    """
    returns alphanumeric characters with mixed case
    :param m: minimum characters
    :param x: maximum characters
    :return: random characters
    """
    chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return "".join((random.choice(chars) for _ in range(random.randint(m, x))))


# todo: put this on room_manager
def random_nick(option=RandomNickOptions.Neither):
    male_nicks = user_data('male', 'nicknames', True, "x\n")
    female_nicks = user_data('female', 'nicknames', True, "y\n")
    choice = human = {
        RandomNickOptions.Female: True,
        RandomNickOptions.Male: False,
        RandomNickOptions.Both: random.choice([True, False]),
        RandomNickOptions.Neither: random_chars()
    }[option]
    # life is a joke
    if isinstance(human, bool):  # and men are animals
        with open(female_nicks if human else male_nicks) as f:
            return random.choice(f.readlines())[:-1]  # remove \n
    return choice


def s2b(s, nullable=True):
    """
    String to Bool conversion
    :param s: string to convert into bool
    :param nullable: returns None if its not "true" nor "false"
    :return:
    """
    if nullable:
        return {"true": True, "false": False}.get(str(s).lower())
    else:
        return True if str(s).lower() == "true" else False


def encode_links(text, link_replacement):
    return links_re.sub(rf"\1\2{link_replacement}\3", text)


def replace_deletable(text, dot_replacement="dоt"):
    return dot_re.sub(rf"\1{dot_replacement}\2", text)


def open_user_data_folder():
    webbrowser.open(os.path.join(os.path.expanduser("~"), "nebchat"))


def user_data(file, folder="", create_file=False, init_content='',
              get_filename_anyway=True):
    """
    Retrieves with or without creating the file stored in the current user data
    path
    :param file: file name
    :param folder: folder name, i.e.: ~/nebchat/themes/filename
    :param create_file: try to create dest file if doesn't exists
    :param init_content: initial content when creating file
    :param get_filename_anyway: get filename even if it doesn't exists
    :return: full path to the specified file
    """
    path = os.path.join(os.path.expanduser("~"), "nebchat", folder)
    config = os.path.join(path, file)
    if not os.path.exists(path):
        try:
            os.makedirs(path, exist_ok=1)
        except Exception as e:
            logger.error(f"failed attempt to create folders {path}:", e)
            return
    if create_file and not os.path.isfile(config):
        try:
            with open(config, 'w') as f:
                f.write(init_content)
                return config
        except Exception as e:
            logger.error(f"failed attempt to create content for {config}:", e)
    elif get_filename_anyway:
        return config
    else:
        return path
    return


def user_data_folder(*extend, file=""):
    """
    Works similar to user_data, but it can create paths without depth limit,
    but it can't create the destination file.
    :param extend: any number of sub-folders to create
    :param file: the name of the final destination file
    :return:
    """
    path = os.path.join(os.path.expanduser("~"), "nebchat", *extend)
    try:
        os.makedirs(path, exist_ok=1)
    except Exception as e:
        logger.error(f"failed attempt to create folders {path}:", e)
        return
    path = os.path.join(path, file)
    return path


def get_qt5_resource(resource):
    return os.path.join(resources.PyQt5UI.__path__[0], resource)


def get_db_resource(resource):
    res = os.path.join(resources.database.__path__[0], resource)
    with open(res) as f:
        return f.read()
