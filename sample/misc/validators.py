import logging
import re

logger = logging.getLogger(__name__)


def uid_from_ticket(ticket):
    """
    takes user id from ticket
    :param ticket: ticket string
    :return: str with user id
    """
    m = re.match(r"^(\d{1,8}),\d{19}(-\d{1,2})?,.{172}$", ticket)
    return m.group(1) if m else 0


def is_ticket_valid(ticket):
    """
    :param ticket: ticket string
    :return: True on success, False or failure
    """
    return int(uid_from_ticket(ticket)) > 0


def parse_facebook_token(token_str):
    """
    Tries to get the token used for account sign-in
    (see sample.core.networking.account)
    The result is not 100% accurate, this token must be tested to be valid
    :param token_str: URL response with token, or the token itself
    :return: str containing the token, None if not found
    """
    m = re.search(r'(?:access_token=)?(EAAWm.*)(?:&expires.*)|(?:access_token=)?(EAAWm.*)$', token_str)
    if not m:
        return None
    elif not m.group(1):
        return m.group(2)
    else:
        return m.group(1)


def validate_session_data(data: dict, test_sid: str, skip_test_sid=False):
    """
    ensures that data conforms to the following structure:

    {"sessions": {
        "0": { "tickets": [], "notes": ""}},
    "default": "0"}

    :param data: dict containing session data
    :param test_sid: session id to be tested
    :param skip_test_sid: do not try to find test_sid
    :return: False if any error occurs, True otherwise
    """
    if not data:
        return False
    if "sessions" not in data:
        logger.error("cannot see any sessions from file")
        logger.debug(f"session file dump: {data}")
        return False
    if "default" not in data:
        logger.error("default session key not found")
        return False
    elif not data['default'] or data['default'] not in data['sessions']:
        logger.error("default session not found")
        return False
    sessions = data['sessions']  # sessions = dict
    for sid in sessions:
        if not any(e in data['sessions'][sid] for e in ["tickets", "notes"]):
            logger.error(f"session {sid} has some missing keys")
            return False
        if not isinstance(data['sessions'][sid]['notes'], str):
            logger.error(f"session {sid} has invalid data in notes")
            return False
        if "tickets" not in data['sessions'][sid]:
            logger.error(f"session {sid} doesnt contains tickets session")
            return False
        tickets = data['sessions'][sid]['tickets'].copy()  # not ref
        for i in tickets:
            if not is_ticket_valid(i):
                logger.error(f"session {sid} contains an invalid ticket")
                logger.debug(f"invalid ticket: {i}")
                return False
    if not skip_test_sid:
        if test_sid not in data['sessions']:
            logger.error(f"session id {test_sid} not available")
            return False
    return True
