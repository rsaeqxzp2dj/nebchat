import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtWidgets import QMessageBox
from PyQt5.uic import loadUi

from sample.misc import utils
from sample.misc import validators
from .add_account import AddAccount
from sample.ui.qt5.custom_classes.runnable import async_call

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# i was forced to put these decorators outside of the class to avoid having to
# disable code inspections. it looks ugly but works out of the box
def request(func):
    """
    This function wrapper simplifies and avoid repeating code for standard
    requests. Everytime a request is done, all buttons must be disabled (in
    order to avoid multiple requests at the same time, leading to race
    conditions and more problems than I'm willing to solve), it is also possible
    that an id is inside the list, but isnt really in the manager, in this case
    the request is ignored.
    After the request is done, all buttons are re-enabled on standard_result
    """
    def wrapped(*args, **kwargs):
        # get local variables from wrapped function. this is always ok
        self, uid = args[0], args[1]

        logger.debug(f"{func.__name__}({str(*args[1:])[:50]})")
        if not self._try_get_uid(uid):
            return
        self._disable_all_buttons(True)
        func(*args, **kwargs)
    return wrapped


def result(func):
    """
    Re-enables all disabled buttons and handles errors in requests.
    All errors are by default shown in the standard error output.
    """
    def wrapped(*args, **kwargs):
        # get local variables from wrapped function. this is always ok
        self, account_data = args[0], args[1]

        logger.debug(f"{func.__name__}: {account_data}")
        msgbox = QMessageBox(self)
        if not account_data:
            self._disable_all_buttons(False)
            msgbox.critical(msgbox, func.__name__, "An error has occurred. "
                                                   "Invalid account?",
                            QMessageBox.Ok)
            logger.error("An error has occurred")
            return
        if isinstance(account_data, dict):
            error = account_data.get('Error')  # sometimes account_data is None
            if error:
                msgbox.critical(msgbox, "Error", error, QMessageBox.Ok)
                logger.error(error)
        func(*args, **kwargs)  # call wrapped function
        self._disable_all_buttons(False)
    return wrapped


class AccountManager(QDialog):
    signal_checkin = pyqtSignal(object)
    signal_get_alerts = pyqtSignal(object)
    signal_set_name = pyqtSignal(object)
    signal_set_profile = pyqtSignal(object)
    signal_get_profile = pyqtSignal(object)
    signal_get_stats = pyqtSignal(object)
    account_added = pyqtSignal(object)
    account_removed = pyqtSignal(object)

    def __init__(self, parent, manager, *args):
        super(AccountManager, self).__init__(*args, parent=parent)
        # todo: emails
        # todo: friend list
        self.manager = manager
        self.current_account_id = 0
        res = utils.get_qt5_resource("account_manager.ui")
        loadUi(res, self)
        self.update_list()
        self.signal_checkin.connect(self.on_account_checkin_result)
        self.signal_get_alerts.connect(self.on_account_get_alerts_result)
        self.signal_set_name.connect(self.on_account_set_name_result)
        self.signal_set_profile.connect(self.on_account_set_profile_result)
        self.signal_get_profile.connect(self.on_account_get_profile_result)
        self.signal_get_stats.connect(self.on_account_get_player_status_result)
        self.__pending_ops = 0

    def _disable_all_buttons(self, disabled):
        num = 1 if disabled else -1
        self.__pending_ops += num
        if self.__pending_ops and not disabled:
            return
        self.btnAddAccount.setDisabled(disabled)
        self.btnChangeName.setDisabled(disabled)
        self.btnChangeProfile.setDisabled(disabled)
        self.btnLuckyWheel.setDisabled(disabled)
        self.btnVideoReward.setDisabled(disabled)
        self.btnAccountRefresh.setDisabled(disabled)
        self.btnDeleteAccount.setDisabled(disabled)
        self.btnRefreshEmails.setDisabled(disabled)
        self.btnDeleteEmail.setDisabled(disabled)
        self.btnSendEmail.setDisabled(disabled)
        self.btnRefreshFriends.setDisabled(disabled)
        self.btnAddFriend.setDisabled(disabled)
        self.lstAccounts.setDisabled(disabled)

    def __get_user_data(self, uid):
        self.current_account_id = uid
        self.req_account_checkin(uid)
        self.req_account_get_alerts(uid)
        self.req_account_get_player_status_profile(uid)
        self.req_account_get_profile(uid)

    @pyqtSlot(QListWidgetItem, name="on_lstAccounts_itemClicked")
    def on_lst_accounts_item_changed(self, item: QListWidgetItem):
        uid = item.text()
        self.__get_user_data(uid)

    @pyqtSlot(name="on_btnAccountRefresh_clicked")
    def on_btn_account_refresh_clicked(self):
        self.__get_user_data(self.current_account_id)

    @pyqtSlot(name="on_btnChangeName_clicked")
    def on_btn_change_name_clicked(self):
        self.req_account_set_name(self.current_account_id)

    @pyqtSlot(name="on_btnChangeProfile_clicked")
    def on_btn_change_profile_clicked(self):
        self.req_account_set_profile(self.current_account_id)

    @pyqtSlot(QListWidgetItem, name="on_lstAccounts_itemDoubleClicked")
    def on_lst_accounts_item_double_clicked(self, item: QListWidgetItem):
        self.on_lst_accounts_item_changed(item)

    @pyqtSlot(name="on_btnAddAccount_clicked")
    def on_btn_add_account_clicked(self):
        win_add_account = AddAccount(self, self.manager)
        win_add_account.account_added.connect(self.on_add_account)
        win_add_account.show()

    @pyqtSlot(name="on_btnDeleteAccount_clicked")
    def on_btn_delete_account_clicked(self):
        item = self.lstAccounts.takeItem(self.lstAccounts.currentRow())
        if item:
            uid = item.text()
            self.account_removed.emit(uid)
            self.manager.remove_account_by_id(uid)

    def update_list(self):
        self.lstAccounts.clear()
        for i in self.manager.accounts:
            self.lstAccounts.addItem(i)

    def on_add_account(self, ticket):
        uid = validators.uid_from_ticket(ticket)
        self.lstAccounts.addItem(uid)
        self.account_added.emit(uid)

    def _try_get_uid(self, uid):
        account = self.manager.accounts.get(uid)
        if not account:
            logger.error(f"account {uid} not actived")
            return
        else:
            return account

    @result
    def on_account_checkin_result(self, account_data):
        self.lblAccountPlasma.setText(str(account_data.get('Coins')))

    @request
    def req_account_checkin(self, uid):
        account = self._try_get_uid(uid)
        if not account:
            return
        async_call(account.account.api_checkin, self.signal_checkin)

    @result
    def on_account_get_alerts_result(self, account_data):
        clan_name = account_data.get('ClanName')
        clan_name = clan_name if not clan_name else "None"
        self.lblAccountClan.setText(clan_name)
        self.lblAccountFriendRequests.setText(str(account_data.get('HasFriendRequests')))
        self.lblAccountNewMails.setText(str(account_data.get('NewMail')))
        self.lblAccountClanRequests.setText(str(account_data.get('HasClanInvites')))

    @request
    def req_account_get_alerts(self, uid):
        account = self._try_get_uid(uid)
        async_call(account.account.api_get_alerts, self.signal_get_alerts)

    @result
    def on_account_set_name_result(self, account_data):
        pass

    @request
    def req_account_set_name(self, uid):
        account = self._try_get_uid(uid)
        nick = self.txtAccountName.text()
        async_call(account.account.api_set_name, self.signal_set_name, nick)

    @result
    def on_account_set_profile_result(self, account_data):
        self.txtAccountProfile.setPlainText(account_data.get('profile'))

    @request
    def req_account_set_profile(self, uid):
        account = self._try_get_uid(uid)
        profile_txt = self.txtAccountProfile.toPlainText()
        async_call(account.account.api_set_player_profile,
                   self.signal_set_profile, profile_txt)

    @result
    def on_account_get_profile_result(self, account_data):
        # for some reason this is not a dict
        self.txtAccountProfile.setPlainText(account_data["profile"])

    @request
    def req_account_get_profile(self, uid):
        account = self._try_get_uid(uid)
        async_call(account.account.api_get_player_profile,
                   self.signal_get_profile, uid)

    @result
    def on_account_get_player_status_result(self, account_data):
        self.txtAccountName.setText(account_data.get('AccountName'))
        self.lblAccountID.setText(str(account_data.get('AccountID')))

    @request
    def req_account_get_player_status_profile(self, uid):
        account = self._try_get_uid(uid)
        async_call(account.account.api_get_player_stats,
                   self.signal_get_stats, uid)
