import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QDialogButtonBox
from PyQt5.uic import loadUi

from sample.misc import utils
from sample.misc import validators
from sample.ui.qt5.custom_classes.runnable import async_call

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class AddAccount(QDialog):
    """
    This dialog lets the user add a new account by using a facebook token or
    an account ticket. The input must be valid before being added, a internet
    connection is required in order to verify is the login ticket is valid to be
    used, if the user logged out, the ticket might not be valid anymore, in this
    case, the user will need to adiquire a new login ticket.

    Everytime this class is used it must connect the signal account_added.
    """
    account_added = pyqtSignal(str)
    signal_request_result = pyqtSignal(object)

    def __init__(self, parent, manager, *args):
        super(AddAccount, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("add_account.ui")
        loadUi(res, self)
        self.manager = manager
        self.accepted_input = None
        self.is_fb_token = False
        self.__ignore_result = False  # if user cancels while doing the request
        self.__busy = False
        self.signal_request_result.connect(self.__on_request_result)

    @property
    def _busy(self):
        return self.__busy

    @_busy.setter
    def _busy(self, is_busy: bool):
        self.btnsAction.button(QDialogButtonBox.Ok).setEnabled(not is_busy)
        self.txtTicketString.setEnabled(not is_busy)
        self.__busy = is_busy

    def set_msg(self, color, message):
        """
        Sets status label message and color
        :param color: foreground color
        :param message: display text
        """
        self.lblStatus.setStyleSheet(f"color: {color};")
        self.lblStatus.setText(message)

    def check_ticket(self):
        """
        Pre-validation step, called on each interaction with the PlainTextBox
        """
        string = self.txtTicketString.toPlainText()
        self.is_fb_token = False
        fb_token = validators.parse_facebook_token(string)
        if fb_token:
            self.set_msg("#50D050", "Looks like a valid facebook token")
            self.accepted_input = fb_token
            self.is_fb_token = True
        elif validators.is_ticket_valid(string):
            self.set_msg("#50D050", "Looks like a valid ticket")
            self.accepted_input = string
        else:
            self.set_msg("red", "This is an invalid input")
            self.accepted_input = None

    def __process_request(self):
        """
        Runs in another thread
        """
        if self.is_fb_token:
            return self.manager.test_token(self.accepted_input)
        else:
            return self.manager.test_ticket(self.accepted_input)

    def __on_request_result(self, result):
        """
        Really add the account if everything is ok
        Called from request_result signal emittion
        """
        is_valid, result = result
        if self.__ignore_result:
            return
        self._busy = False
        if is_valid:
            ret = self.manager.add_account(result)
            if ret:
                self.account_added.emit(result)
                self.close()
                return
            else:
                logger.warning(f"add_account({result}) bypassed a verification step.")
                result = "Account being used."
        self.set_msg("red", result)

    @pyqtSlot(name="on_btnsAction_accepted")
    def on_ok(self):
        if not self._busy:
            self.__ignore_result = False
            self._busy = True
            self.set_msg("chocolate", "Validating ticket...")
            async_call(self.__process_request, self.signal_request_result)

    @pyqtSlot(name="on_btnsAction_rejected")
    def on_cancel(self):
        self.__ignore_result = True
        self.close()

    @pyqtSlot(name="on_txtTicketString_textChanged")
    def on_txt_ticket_edited(self):
        self.check_ticket()
