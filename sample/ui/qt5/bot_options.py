from sample.bot.bot import Bot
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi
from sample.misc import utils

import json


class BotOptions(QDialog):
    FILE = "bot_options.json"

    def __init__(self, parent, bot: Bot, *args):
        super(BotOptions, self).__init__(parent=parent, *args)
        res = utils.get_qt5_resource("bot_options.ui")
        loadUi(res, self)
        self.bot = bot
        self.__add_nicks_options()
        self.load_options()

    def __add_nicks_options(self):
        self.cbRndNickType.addItems([i.name for i in utils.RandomNickOptions])

    def save_options(self):
        data = {
            "min_room_size": self.spMinLimit.value(),
            "min_to_join": self.spMinJoin.value(),
            "min_to_leave": self.spMinLeave.value(),
            "is_nick_static": self.rbStaticNick.isChecked(),
            "static_nick_value": self.txtStaticNick.text(),
            "random_nick_value": self.cbRndNickType.currentIndex(),
            "account_selection_sequencial": self.rbSeqAccountSel.isChecked(),
            "room_selection_sequencial": self.rbSeqRoomSel.isChecked(),
            "is_update_constant": self.rbConstInterval.isChecked(),
            "update_interval": self.spInterval.value(),
            "update_variation": self.spVariation.value()
        }
        with open(utils.user_data(self.FILE), "w") as f:
            json.dump(data, f, indent=2, ensure_ascii=False)

    def load_options(self):
        try:
            with open(utils.user_data(self.FILE)) as f:
                data = json.load(f)
        except FileNotFoundError:
            return
        except json.JSONDecodeError:
            return
        try:
            self.spMinLimit.setValue(data['min_room_size'])
            self.spMinJoin.setValue(data['min_to_join'])
            self.spMinLeave.setValue(data['min_to_leave'])
            if data['account_selection_sequencial']:
                self.rbSeqAccountSel.click()
            else:
                self.rbRndAccountSel.click()

            if data['room_selection_sequencial']:
                self.rbSeqRoomSel.click()
            else:
                self.rbRndRoomSel.click()

            if data['is_nick_static']:
                self.rbStaticNick.click()
            else:
                self.rbRndNick.click()
            self.txtStaticNick.setText(data['static_nick_value'])
            self.cbRndNickType.setCurrentIndex(data['random_nick_value'])
            if data['is_update_constant']:
                self.rbConstInterval.click()
            else:
                self.rbRndInterval.click()
            self.spInterval.setValue(data['update_interval'])
            self.spVariation.setValue(data['update_variation'])
        except KeyError:
            return

    @pyqtSlot(int, name="on_cbRndNickType_currentIndexChanged")
    def on_rnd_nick_type_choice(self, index):
        self.bot.random_nick_type = utils.RandomNickOptions(index)

    @pyqtSlot(name="on_btnsAction_rejected")
    def on_cancel(self):
        self.close()

    @pyqtSlot(name="on_btnsAction_accepted")
    def on_save(self):
        self.save_options()

    @pyqtSlot(int, name="on_spMinLeave_valueChanged")
    def on_min_leave_value_changed(self, value):
        Bot.MIN_PEOPLE_TO_STAY = value

    @pyqtSlot(int, name="on_spMinJoin_valueChanged")
    def on_min_join_value_changed(self, value):
        Bot.MIN_PEOPLE_TO_JOIN = value

    @pyqtSlot(int, name="on_spMinLimit_valueChanged")
    def on_min_room_limit_value_changed(self, value):
        Bot.MIN_ROOM_SIZE = value

    @pyqtSlot(int, name="on_spInterval_valueChanged")
    def on_spin_interval_changed(self, value):
        self.spVariation.setMaximum(value - 1)
        self.bot.set_interval(value)

    def __set_static_nick(self):
        self.bot.random_nick = False
        txt = self.txtStaticNick.text().strip()
        self.txtStaticNick.setText(txt)
        if not txt:
            txt = self.txtStaticNick.placeholderText()
        self.bot.static_nick = txt

    @pyqtSlot(name="on_txtStaticNick_returnPressed")
    def on_static_nick_edited(self):
        self.__set_static_nick()

    @pyqtSlot(name="on_rbStaticNick_clicked")
    def on_static_nick_opt_click(self):
        self.__set_static_nick()

    @pyqtSlot(name="on_rbConstInterval_clicked")
    def on_const_interval(self):
        self.bot.set_interval(self.spInterval.value())

    @pyqtSlot(name="on_rbRndInterval_clicked")
    def on_random_interval(self):
        self.bot.set_interval(self.spInterval.value(), self.spVariation.value())

    @pyqtSlot(name="on_rbSeqAccountSel_clicked")
    def on_rb_sequential_account_sel_clicked(self):
        self.bot.random_accounts = False

    @pyqtSlot(name="on_rbRndAccountSel_clicked")
    def on_rb_random_account_sel_clicked(self):
        self.bot.random_accounts = True

    @pyqtSlot(name="on_rbSeqRoomSel_clicked")
    def on_rb_sequential_room_sel_clicked(self):
        self.bot.random_rooms = False

    @pyqtSlot(name="on_rbRndRoomSel_clicked")
    def on_rb_random_room_sel_clicked(self):
        self.bot.random_rooms = True
