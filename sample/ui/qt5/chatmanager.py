import logging

from PyQt5.QtCore import QModelIndex
from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QProgressBar
from PyQt5.uic import loadUi

from sample.core.manager import Events
from sample.core.networking.chatcode import ConnState
from sample.misc import utils
from sample.ui.qt5.custom_classes import events
from sample.ui.qt5 import configurations as configs
from .custom_widgets.window import Window

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ChatListItem(QObject):
    def __init__(self, uid, nick, room_name, maximum):
        super(ChatListItem, self).__init__()
        self.uid = QStandardItem(uid)
        self.uid.setEditable(False)
        self.account_id = uid
        self.nick = QStandardItem(nick)
        self.room_name = QStandardItem(room_name)
        self.progressbar_people = QProgressBar()
        self.progressbar_people.setFormat(f"%v/{maximum}")
        self.progressbar_people.setMaximum(maximum)
        self.progressbar_people.setMinimum(0)
        self.userlist: {str: (QStandardItem, QStandardItem)} = {}

    def update(self, new_userlist: {int: str}):
        self.progressbar_people.setValue(len(new_userlist))
        for i in new_userlist:
            if i not in self.userlist:
                if self.account_id == str(i):
                    continue
                self.userlist[i] = (QStandardItem(str(i)),
                                    QStandardItem(new_userlist[i]))
                self.uid.appendRow(self.userlist[i])

        for i in self.userlist.copy():
            if i not in new_userlist:
                self.uid.removeRow(self.userlist[i][0].row())
                self.userlist.pop(i)


class ChatManager(Window):
    COLMN_ACCOUNT_ID = 0

    open_chat_signal = pyqtSignal(str, int)  # uid, option

    def __init__(self, parent, account_manager, qapp, *args):
        super(ChatManager, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("chatmanager.ui")
        loadUi(res, self)
        self.app = qapp
        self.manager = account_manager
        self.__set_table_headers()
        self.__register_listeners_for_all_accounts()
        self.__set_chat_container_options()
        self._current_sel = None
        self._items: {str: ChatListItem} = {}

    def cleanup(self):
        # remove listeners
        for uid in self.manager.accounts:
            self.on_account_removed(uid)

    def __set_listener(self, uid, register):
        listeners_params = [
            (Events.self_enter, uid, self._on_self_enter),
            (Events.self_leave, uid, self._on_self_leave),
            (Events.user_join, uid, self._on_user_join),
            (Events.user_leave, uid, self._on_user_leave)]
        func = self.manager.register_listener if register else self.manager.remove_listener
        for params in listeners_params:
            func(*params)

    def __register_listeners_for_all_accounts(self):
        for account_id in self.manager.accounts:
            self.__set_listener(account_id, True)

    def __set_table_headers(self):
        items = ["Account id", "Nick", "Room name", "People"]
        model = QStandardItemModel()
        self.trChats.setModel(model)
        model.setHorizontalHeaderLabels(items)
        self.trChats.header().resizeSection(0, 120)
        self.trChats.header().resizeSection(1, 150)
        self.trChats.header().resizeSection(2, 150)

    def __set_chat_container_options(self):
        self.cbOpenAs.addItems([i.name for i in configs.ChatContainer])

    def _on_user_join(self, *args):
        self.app.postEvent(self, events.ChatUserJoin(*args))

    def _on_user_leave(self, *args):
        self.app.postEvent(self, events.ChatUserLeave(*args))

    def _on_self_enter(self, *args):
        self.app.postEvent(self, events.RoomEnter(*args))

    def _on_self_leave(self, *args):
        self.app.postEvent(self, events.RoomLeave(*args))

    def customEvent(self, event):
        if isinstance(event, events.ChatUserJoin):
            self.update_userlists(*event.data)
        if isinstance(event, events.ChatUserLeave):
            self.update_userlists(*event.data)
        if isinstance(event, events.RoomEnter):
            self.add_account_to_table(*event.data)
        if isinstance(event, events.RoomLeave):
            self.remove_account_from_table(*event.data)

    def add_account_to_table(self, uid, nickname, room):
        chat = self.manager.accounts[uid].chat
        maxppl = chat.room_status['max_people']
        item = ChatListItem(uid, nickname, room, maxppl)
        model = self.trChats.model()
        stditem = QStandardItem()
        model.appendRow([item.uid, item.nick, item.room_name, stditem])
        index = model.indexFromItem(stditem)
        self.trChats.setIndexWidget(index, item.progressbar_people)
        item.update(chat.userlist)
        self._items[uid] = item

    def remove_account_from_table(self, uid, _, __):
        model = self.trChats.model()
        model.takeRow(self._items[uid].uid.row())
        self._items.pop(uid)
        if uid == self._current_sel:
            self.widgetControls.setEnabled(False)

    def update_userlists(self, _, __, room):
        for uid in self._items:
            chat = self.manager.accounts[uid].chat
            if room == chat.room_status['name']:
                self._items[uid].update(chat.userlist)

    # custom slot called from outside
    # to avoid having to search for the disconnected account with
    # ConnStatusChange, this works better for removing a disconnected account
    def on_account_disconnect(self, uid):
        if uid not in self._items:
            return
        self.remove_account_from_table(uid, None, None)

    # custom slot called from outside
    def on_account_added(self, uid):
        self.__set_listener(uid, True)

    # custom slot called from outside
    def on_account_removed(self, uid):
        self.__set_listener(uid, False)

    @pyqtSlot(QModelIndex, name="on_trChats_clicked")
    def on_chat_clicked(self, index):
        self.widgetControls.setEnabled(True)
        model = self.trChats.model()
        item = model.itemFromIndex(index)
        if item.parent():
            item = item.parent()
        self._current_sel = model.item(item.row(), self.COLMN_ACCOUNT_ID).text()

    @pyqtSlot(name="on_btnLeave_clicked")
    def on_btn_leave_clicked(self):
        if self._current_sel not in self._items:
            logger.error(f"{self._current_sel} not found")
            return
        self.manager.accounts[self._current_sel].chat.send_lobby_leave_request()

    @pyqtSlot(name="on_btnOpenChat_clicked")
    def on_open_chat_clicked(self):
        if self._current_sel not in self._items:
            logger.error(f"{self._current_sel} not found")
            return
        container = configs.ChatContainer[self.cbOpenAs.currentText()].value
        self.open_chat_signal.emit(self._current_sel, container)
