from PyQt5.QtCore import QSettings
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi
from enum import Enum
from sample.misc import utils
from .theme_editor import ThemeEditor
from .notification_rules import NotificationRules
from sample.core.networking.chatcode import NebChatcode
from .highlight_style import HighlightStyle
from .bot_options import BotOptions


class ChatContainer(Enum):
    Window = 0
    Tab = 1
    Dock = 2


class EncodeLinksOptions(Enum):
    url_encode = 0
    one_dot_leader = 1

    @property
    def name(self):
        if self.value == 0:
            return "%2E"
        elif self.value == 1:
            return "․"


class Configurations(QDialog):
    CFG_FILE = "settings.ini"
    DEFAULT_PRELOAD_MSGS = 100
    DEFAULT_INACTIVITY_TIME = 10

    def __init__(self, parent, tray, leeching, bot, *args):
        super(Configurations, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("configurations.ui")
        loadUi(res, self)
        self.settings = QSettings(utils.user_data(self.CFG_FILE),
                                  QSettings.IniFormat)
        self.theme_editor = ThemeEditor(self)
        self.highlight_style = HighlightStyle(self)
        self.bot_options = BotOptions(self, bot)
        self.notification_rules = NotificationRules(self, tray)
        self.leeching = leeching if leeching else self.__disable_leeching_opt()
        self.bot = bot if bot else self.__disable_bot_opt()
        self.__set_default_chat_container_options()
        self.__load_settings()
        self.lblPath.setText(utils.user_data_folder())

    def __set_default_chat_container_options(self):
        self.cbJoinForm.addItems([i.name for i in ChatContainer])
        self.cbNickTypes.addItems([i.name for i in utils.RandomNickOptions])
        self.cbEncodeLinkOpt.addItems([i.name for i in EncodeLinksOptions])

    def __save_settings(self):
        self.settings.setValue("preload_quantity", self.load_last_messages)
        self.settings.setValue("default_join", self.join_form.value)
        self.settings.setValue("random_nick_type", self.random_nick_type.value)
        self.settings.setValue("theme", self.theme_editor.current_theme)
        self.settings.setValue("autoload_theme", self.autoload_theme)
        self.settings.setValue("inactivity", self.inactivity_time)
        self.settings.setValue("split_msgs", self.split_msg)
        self.settings.setValue("replace_deletable", self.replace_deletable)
        self.settings.setValue("encode_links", self.encode_links)
        self.settings.setValue("link_replacement", self.link_replacement)
        self.settings.setValue("highlights", self.highlights)
        self.settings.setValue("autorandomize_nick", self.highlights)
        self.settings.setValue("enable_leeching", self.enable_leeching)
        self.settings.setValue("enable_bot", self.enable_bot)

    def __load_settings(self):
        from sample.misc.utils import s2b
        self.__set_load_last_messages(self.settings.value("preload_quantity"))
        v = self.settings.value("default_join")
        self.cbJoinForm.setCurrentIndex(int(v) if v else 0)
        v = self.settings.value("random_nick_type")
        self.cbNickTypes.setCurrentIndex(int(v) if v else 0)
        v = self.settings.value("autoload_theme")
        self.cbAutoloadTheme.setChecked(s2b(v) if v is not None else True)
        if s2b(v):
            self.theme_editor.load_theme(self.settings.value("theme"))
        v = self.settings.value("inactivity")
        self.spDisconnectOnInactivity.setValue(
            int(v) if v else self.DEFAULT_INACTIVITY_TIME)
        v = self.settings.value("split_msgs")
        self.cbSplitMsg.setChecked(s2b(v) if v is not None else True)
        v = self.settings.value("replace_deletable")
        self.cbReplaceDeletable.setChecked(s2b(v) if v is not None else False)
        v = self.settings.value("encode_links")
        self.cbEncodeLinks.setChecked(s2b(v) if v is not None else True)
        v = self.settings.value("link_replacement")
        self.cbEncodeLinkOpt.setCurrentIndex(
            EncodeLinksOptions(v).value if v else 0)
        v = self.settings.value("highlights")
        self.cbHighlights.setChecked(s2b(v) if v is not None else True)
        v = self.settings.value("autorandomize_nick")
        self.cbAutoRandomNick.setChecked(s2b(v) if v is not None else True)
        if self.leeching:
            v = self.settings.value("enable_leeching")
            self.cbEnableleeching.setChecked(s2b(v) if v is not None else True)
        if self.bot:
            v = self.settings.value("enable_bot")
            self.cbEnableBot.setChecked(s2b(v) if v is not None else True)

    @property
    def enable_bot(self):
        return self.cbEnableBot.isChecked()

    @property
    def enable_leeching(self):
        return self.cbEnableleeching.isChecked()

    def __disable_bot_opt(self):
        self.cbEnableBot.setEnabled(False)
        self.btnBotOptions.setEnabled(False)

    @pyqtSlot(bool, name="on_cbEnableBot_toggled")
    def on_cb_bot_toggled(self, state):
        if self.bot:
            if state:
                self.bot.start()
            else:
                self.bot.stop()

    def __disable_leeching_opt(self):
        self.cbEnableleeching.setEnabled(False)
        self.cbleechingVerbose.setEnabled(False)

    @pyqtSlot(bool, name="on_cbEnableleeching_toggled")
    def on_cb_leeching_toggled(self, state):
        if self.leeching:
            if state:
                self.leeching.start()
            else:
                self.leeching.stop()

    @property
    def auto_randomize_nick(self):
        return self.cbAutoRandomNick.isChecked()

    @property
    def join_form(self) -> ChatContainer:
        return ChatContainer[self.cbJoinForm.currentText()]

    @pyqtSlot(name="on_btnBotOptions_clicked")
    def on_btn_bot_options(self):
        self.bot_options.show()
        self.bot_options.load_options()

    @pyqtSlot(int, name="on_spDisconnectOnInactivity_valueChanged")
    def on_disconnect_on_inactivity_value_changed(self, value):
        NebChatcode.limit_of_inactivity_in_secs = value * 60

    @pyqtSlot(name="on_btnThemeEditor_clicked")
    def on_btn_theme_editor_clicked(self):
        self.theme_editor.show()

    @pyqtSlot(name="on_btnsActions_rejected")
    def on_close(self):
        self.close()

    @pyqtSlot(name="on_btnsActions_accepted")
    def on_save(self):
        self.__save_settings()

    @pyqtSlot(name="on_btnOpen_clicked")
    def on_open_cfg_folder(self):
        utils.open_user_data_folder()

    @pyqtSlot(name="on_btnRules_clicked")
    def on_btn_rules_clicked(self):
        self.notification_rules.show()

    @pyqtSlot(name="on_btnEditHighlight_clicked")
    def on_btn_edit_hightlight_clicked(self):
        self.highlight_style.show()
        self.highlight_style.load()

    @property
    def highlights(self):
        return self.cbHighlights.isChecked()

    @property
    def notifications(self):
        return self.cbNotifications.isChecked()

    @property
    def random_nick_type(self) -> utils.RandomNickOptions:
        return utils.RandomNickOptions[self.cbNickTypes.currentText()]

    @property
    def autoload_theme(self):
        return self.cbAutoloadTheme.isChecked()

    @property
    def encode_links(self):
        return self.cbEncodeLinks.isChecked()

    @property
    def inactivity_time(self):
        return self.spDisconnectOnInactivity.value()

    @property
    def link_replacement(self):
        return EncodeLinksOptions(self.cbEncodeLinkOpt.currentIndex())

    @property
    def replace_deletable(self):
        return self.cbReplaceDeletable.isChecked()

    @property
    def split_msg(self):
        return self.cbSplitMsg.isChecked()

    @property
    def load_last_messages(self):
        if not self.cbLoadLastMsgs.isChecked():
            return 0
        else:
            return self.spLoadQuantity.value()

    def save_window(self, name, data):
        self.settings.setValue(name, data)

    def restore_window(self, name):
        return self.settings.value(name)

    def __set_load_last_messages(self, v):
        v = int(v) if v is not None else self.DEFAULT_PRELOAD_MSGS
        if v:
            self.cbLoadLastMsgs.setChecked(True)
            self.spLoadQuantity.setValue(v)
        else:
            self.cbLoadLastMsgs.setChecked(False)
            self.spLoadQuantity.setValue(self.DEFAULT_PRELOAD_MSGS)
