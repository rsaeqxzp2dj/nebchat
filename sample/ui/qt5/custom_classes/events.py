from PyQt5.QtCore import QEvent


class BaseEvent(QEvent):
    ROOM_LIST_UPDATE = 1001
    STATUS_CHANGE = 1002
    ROOM_ENTER = 1003
    ROOM_LEAVE = 1004
    ROOM_ENTER_ERROR = 1005
    CHAT_USER_JOIN = 1006
    CHAT_USER_LEAVE = 1007
    PUBLIC_MSG = 1008
    PRIVATE_MSG = 1009
    FRIEND_MSG = 1010
    CLAN_MSG = 1011

    def __init__(self, data, event):
        super(BaseEvent, self).__init__(event)
        self.registerEventType(event)
        self.data = data


class RoomUpdate(BaseEvent):
    def __init__(self, data):
        super(RoomUpdate, self).__init__(data, BaseEvent.ROOM_LIST_UPDATE)


class ConnStatusChange(BaseEvent):
    def __init__(self, *data):
        super(ConnStatusChange, self).__init__(data, BaseEvent.STATUS_CHANGE)


class RoomError(BaseEvent):
    def __init__(self, data):
        super(RoomError, self).__init__(data, BaseEvent.ROOM_ENTER_ERROR)


class RoomLeave(BaseEvent):
    def __init__(self, *data):
        super(RoomLeave, self).__init__(data, BaseEvent.ROOM_ENTER_ERROR)


class RoomEnter(BaseEvent):
    def __init__(self, *data):
        super(RoomEnter, self).__init__(data, BaseEvent.ROOM_ENTER)


class ChatUserJoin(BaseEvent):
    def __init__(self, *data):
        super(ChatUserJoin, self).__init__(data, BaseEvent.CHAT_USER_JOIN)


class ChatUserLeave(BaseEvent):
    def __init__(self, *data):
        super(ChatUserLeave, self).__init__(data, BaseEvent.CHAT_USER_LEAVE)


class PublicMsg(BaseEvent):
    def __init__(self, *data):
        super(PublicMsg, self).__init__(data, BaseEvent.PUBLIC_MSG)


class PrivateMsg(BaseEvent):
    def __init__(self, *data):
        super(PrivateMsg, self).__init__(data, BaseEvent.PRIVATE_MSG)


class FriendMsg(BaseEvent):
    def __init__(self, *data):
        super(FriendMsg, self).__init__(data, BaseEvent.FRIEND_MSG)


class ClanMsg(BaseEvent):
    def __init__(self, *data):
        super(ClanMsg, self).__init__(data, BaseEvent.CLAN_MSG)
