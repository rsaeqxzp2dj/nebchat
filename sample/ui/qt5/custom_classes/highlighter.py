from PyQt5.QtGui import QSyntaxHighlighter
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QBrush
from PyQt5.QtGui import QTextCharFormat
from PyQt5.QtGui import QTextFormat
import re


class Highlighter(QSyntaxHighlighter):
    foreground = None
    background = None
    underline_color = QColor(0, 171, 223)
    underline_style = QTextCharFormat.SingleUnderline
    underline = False
    bold = True
    italic = False

    def __init__(self, doc):
        super(Highlighter, self).__init__(doc)
        self._regex = re.compile("nein nein nein")
        self._format = QTextCharFormat()
        self.set_foreground_color(self.foreground)
        self.set_background_color(self.background)
        self.set_underline(self.underline, self.underline_color,
                           self.underline_style)
        self.set_italic(self.italic)
        self.set_bold(self.bold)

    def set_bold(self, state):
        underline_style = self._format.underlineStyle()
        font = self._format.font()
        font.setBold(state)
        self._format.setFont(font)
        self._format.setUnderlineStyle(underline_style)

    def set_italic(self, state):
        underline_style = self._format.underlineStyle()
        font = self._format.font()
        font.setItalic(state)
        self._format.setFont(font)
        self._format.setUnderlineStyle(underline_style)

    def set_underline(self, state, color, style):
        self._format.setFontUnderline(state is True)
        if not isinstance(color, QColor):
            self._format.clearProperty(QTextFormat.TextUnderlineColor)
        else:
            self._format.setUnderlineColor(color)
        if not style or not state:
            self._format.setUnderlineStyle(QTextCharFormat.NoUnderline)
        else:
            self._format.setUnderlineStyle(style)

    def set_foreground_color(self, color):
        if not isinstance(color, QColor):
            self._format.clearForeground()
        else:
            self.foreground = color
            self._format.setForeground(QBrush(self.foreground))

    def set_background_color(self, color):
        if not isinstance(color, QColor):
            self._format.clearBackground()
        else:
            self.background = color
            self._format.setBackground(QBrush(self.background))

    def highlightBlock(self, text):
        for i in self._regex.finditer(text):
            length = len(text[i.start():i.end()].encode())
            self.setFormat(i.start(), length, self._format)

    def set_regex_uid(self, uid):
        self._regex = re.compile(r".{1,64}\[" + str(uid) + r"\]:.*$")

    def set_regex_nick(self, nick):
        self._regex = re.compile(nick + r"\[\d{1,8}\]:.*$")
