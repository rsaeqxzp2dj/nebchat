from PyQt5.QtCore import QRunnable
from PyQt5 import QtCore


def async_call(func, signal, *args, **kwargs):
    """
    Asynchronously runs a task

    :param func:
    :param signal:
    :param args:
    :param kwargs:
    :return:
    """
    worker = Worker(func, signal, *args, **kwargs)
    # noinspection PyArgumentList
    QtCore.QThreadPool.globalInstance().start(worker)
    # noinspection PyArgumentList
    QtCore.QThreadPool.globalInstance().setMaxThreadCount(32)
    return worker


class Worker(QRunnable):
    INSTANCES = []

    def __init__(self, func, signal, *args, **kwargs):
        QRunnable.__init__(self)
        self.setAutoDelete(True)
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.signal = signal
        Worker.INSTANCES.append(self)

    def run(self):
        self.signal.emit(self.func(*self.args, **self.kwargs))
        self.cleanup()

    def cleanup(self):
        Worker.INSTANCES.remove(self)
