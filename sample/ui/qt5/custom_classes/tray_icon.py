import logging
import re
import json
from collections import namedtuple

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QSystemTrayIcon

from sample.misc import utils

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SysTrayIcon(QSystemTrayIcon):
    rules_options = namedtuple("rule_options", [
        "case_sensitive",  # bool
        "regex",  # bool
        "enabled",  # bool
        "rule"  # str
    ])
    filename = "notifications_rules.json"
    _re_type = type(re.compile(""))

    def __init__(self, parent):
        super(SysTrayIcon, self).__init__()
        self.setParent(parent)
        self.setIcon(QIcon(utils.get_qt5_resource("icon.png")))
        self.notification_rules: {str: SysTrayIcon.rules_options} = {}
        self.load_rules()

    def save_rules(self):
        data = {}
        n = self.notification_rules.copy()
        for i in n:
            data[i] = {'case_sensitive': n[i].case_sensitive,
                       'regex': n[i].regex,
                       'enabled': n[i].enabled,
                       'rule': n[i].rule}
        with open(utils.user_data(self.filename), "w") as f:
            json.dump(data, f, indent=2)

    def load_rules(self):
        try:
            self.notification_rules.clear()
            with open(utils.user_data(self.filename)) as f:
                r = json.load(f)
            for i in r:
                opt = SysTrayIcon.rules_options(
                    r[i]['case_sensitive'],
                    r[i]['regex'],
                    r[i]['enabled'],
                    r[i]['rule'])
                self.notification_rules[i] = opt
        except FileNotFoundError:
            pass
        except json.JSONDecodeError:
            logger.error("error parsing json")
            pass

    def set_rule(self, rule_name, options: rules_options):
        """
        Add or update an existing notification rule
        :param rule_name:
        :param options:
        :return:
        """
        self.notification_rules[rule_name] = options
        self.save_rules()

    def remove_rule(self, rule_name):
        """
        remove an existing rule_name
        :param rule_name:
        :return:
        """
        print(self.notification_rules)
        if rule_name not in self.notification_rules:
            logger.error(f"cannot remove inexisting rule: {rule_name}")
            return
        self.notification_rules.pop(rule_name)
        self.save_rules()

    def test_message(self, message: str):
        """
        Returns true if message has a matching notification rule
        :param message:
        :return:
        """
        for rule in self.notification_rules:
            rule_options = self.notification_rules[rule]
            if not rule_options.enabled:
                return
            sensitive = rule_options.case_sensitive
            if rule_options.regex:
                if re.search(rule_options.rule, message,
                             flags=0 if sensitive else re.IGNORECASE):
                    return True
            else:
                if sensitive:
                    if rule_options.rule in message:
                        return True
                else:
                    if rule_options.rule.lower() in message.lower():
                        return True
