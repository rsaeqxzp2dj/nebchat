import logging

from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QTableView

from sample.core.manager import Events
from sample.core.networking.chatcode import ConnState
from sample.ui.qt5.custom_classes.events import ConnStatusChange

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class StateItem(QObject):
    STR_STATE = {
        ConnState.CONNECTED_INROOM:
            ("Conencted: In room", "Disconnect", True),
        ConnState.DISCONNECTED:
            ("Disconnected", "Connect", False),
        ConnState.CONNECTED_AWAITING:
            ("Connected: Idle", "Disconnect", True),
        ConnState.ATTEMPTING_TO_CONNECT:
            ("Trying to connect...", "Connect", True),
        ConnState.ATTEMPTING_TO_JOIN:
            ("Connected: Joining.....", "Disconnect", True),
        ConnState.NOT_CONNECTED:
            ("Not connected", "Connect", False)
    }
    room_change = pyqtSignal(str)

    def __init__(self, uid):
        super(StateItem, self).__init__()
        self.func_connect = lambda: None
        self.func_disconnect = lambda: None
        self.uid = QStandardItem(uid)
        self.state = QStandardItem("Not connected")
        self.last_room = QStandardItem(" ")
        self.current_room = QStandardItem(" ")
        self.btn_action = QPushButton("Connect")
        self.room_change.connect(self.set_room)

    def customEvent(self, event):
        if isinstance(event, ConnStatusChange):
            self.update_state(*event.data)

    # noinspection PyUnresolvedReferences
    def update_state(self, state):
        self.state.setText(self.STR_STATE[state][0])
        self.btn_action.setText(self.STR_STATE[state][1])
        self.btn_action.pressed.connect(lambda: "let me disconnect everything")
        self.btn_action.pressed.disconnect()
        if self.STR_STATE[state][2]:
            self.btn_action.pressed.connect(self.func_disconnect)
        else:
            self.btn_action.pressed.connect(self.func_connect)

    # i dont know why it works when emitted from chatcode's thread
    def set_room(self, txt):
        if self.current_room.text():
            self.last_room.setText(self.current_room.text())
        self.current_room.setText(txt)

    @property
    def standard_items(self):
        return [self.uid, self.state, self.current_room, self.last_room]


class AccountsStateWidget(QTableView):
    account_connect = pyqtSignal(str)
    account_disconnect = pyqtSignal(str)

    def set_headers(self):
        items = ["Account id", "State", "Current room", "Last room", "Action"]
        model = QStandardItemModel()
        self.setModel(model)
        model.setHorizontalHeaderLabels(items)
        self.horizontalHeader().resizeSection(1, 150)
        self.horizontalHeader().resizeSection(0, 80)
        self.verticalHeader().setVisible(False)

    def __init__(self, parent, manager, qapp):
        super(QTableView, self).__init__(parent=parent)

        self.manager = manager
        self.app = qapp
        self.setStyleSheet("QTableView::item{border-bottom : 1px solid black;}")
        self.set_headers()
        self.setShowGrid(False)
        self.setVerticalScrollMode(self.ScrollPerPixel)
        self.setHorizontalScrollMode(self.ScrollPerPixel)
        self.setEditTriggers(self.NoEditTriggers)
        self.setSelectionMode(self.SingleSelection)
        self.setSelectionBehavior(self.SelectRows)
        self.setAlternatingRowColors(True)
        self._items: {int: [StateItem]} = {}
        self.__listeners: {(Events, int): function} = {}
        self.__init_list()

    def __init_list(self):
        for aid, gi in self.manager.accounts.items():
            self.__add_table_item(aid, gi)

    # called on startup and when a new account is added
    def __setup_listener(self, item: StateItem, uid: int):
        def on_status_change(state):
            self.app.postEvent(item, ConnStatusChange(state))
            if state == ConnState.CONNECTED_INROOM:
                gi = self.manager.accounts[str(uid)]
                item.room_change.emit(gi.chat.room_status['name'])
            else:
                item.room_change.emit("")
            if state == ConnState.CONNECTED_AWAITING:
                self.account_connect.emit(uid)
            elif state == ConnState.DISCONNECTED:
                self.account_disconnect.emit(uid)

        event = Events.connection_change
        self.manager.register_listener(event, uid, on_status_change)
        self.__listeners[(event, uid)] = on_status_change

    def __add_table_item(self, uid, gi):
        self._items[uid] = item = StateItem(uid)

        item.func_connect = gi.connect
        item.func_disconnect = gi.disconnect
        item.update_state(gi.chat.connection_state)
        self.__setup_listener(item, uid)
        items = item.standard_items + [QStandardItem()]
        self.model().appendRow(items)
        self.setIndexWidget(items[-1].index(), item.btn_action)

    def __del_table_item(self, uid):
        self.model().takeRow(self._items[uid].uid.row())

    def update_items(self, _):
        for uid in self._items:
            gi = self.manager.accounts[uid]
            self._items[uid].set_room(gi.chat.room_status["name"],
                                      gi.chat.room_status["last_room"])
            self._items[uid].update_state(gi.chat.connected)

    # custom slot called from outside
    def on_account_added(self, uid):
        if uid in self._items:
            logger.error(f"adding account {uid} when it was already added...?")
            return
        self.__add_table_item(uid, self.manager.accounts[uid])
        pass

    # custom slot called from outside
    def on_account_removed(self, uid):
        if uid not in self._items:
            logger.error(f"removing inexistent account {uid}")
            return
        event = Events.connection_change
        listener = self.__listeners.pop((event, uid))
        self.manager.remove_listener(event, uid, listener)
        self.__del_table_item(uid)
        self._items.pop(uid)
