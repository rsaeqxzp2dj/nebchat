from collections import namedtuple
from xml.sax.saxutils import escape

from PyQt5.QtCore import QModelIndex
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QRegExp
from PyQt5.QtCore import QPoint
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIntValidator
from PyQt5.QtGui import QTextDocument
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QMenu
from PyQt5.QtWidgets import QWidget
from PyQt5.uic import loadUi

from sample.ui.qt5.custom_classes.highlighter import Highlighter
from sample.core.manager import Events
from sample.core.networking.chatcode import ConnState
from sample.misc import utils
from sample.ui.qt5.custom_classes import events
from sample.ui.qt5.profile import PlayerProfile

ChatOptions = namedtuple("ChatOptions",  [
    "encode_links",  # www.website.com -> www.website<link_replacement>com
    "replace_deletable",  # replace words like anedote with utf-8 similar chars
    "split_messages",  # split message at 100 or 200 characters
    "link_replacement",  # replacement for encode_links
    "notifications",  # show notifications from this chat
    "preload",  # pre load old messages
    "highlight"  # highlight current user
])


class UserlistItem:
    def __init__(self, nick: str, uid: str):
        self.nickname = QStandardItem(nick)
        self.uid = QStandardItem(uid)


class ChatWidget(QWidget):
    COLOR_PV = "#00df00"
    COLOR_FRIEND = "#5aa6ff"
    COLOR_CLAN = "#ff2d2d"
    COLOR_SPECIAL = "#2eb2aa"
    COLUMN_NICK = 0
    COLUMN_ID = 1
    FORMAT_PUBLIC_MSG = "{nick}[{uid}]:{msg}"
    FORMAT_PRIVATE_MSG = "{nick}[{sid}->{rid}]:{msg}"
    FORMAT_FRIEND_MSG = "[{sid}->{rid}]:{msg}"
    FORMAT_CLAN_MSG = "{nick}[{uid}][{role}]:{msg}"
    FORMAT_NOTIFICATION_TITLE = "{room}"
    FORMAT_NOTIFICATION_BODY = "{nick}[{uid}]:{msg}"
    FORMAT_USERLIST_IN_REPLY = "{nick}[{uid}] IN"
    FORMAT_USERLIST_OUT_REPLY = "{nick}[{uid}] OUT"

    # tell the parent that this child is dying
    leaving_room = pyqtSignal()
    notify_private = pyqtSignal(str, str)
    # the parent must connect this signal to work
    # notification only works if any rules matches with the test message
    notify_public = pyqtSignal(
        str,  # test message
        str,  # message title
        str   # message body
    )

    def __init__(self, parent, uid, manager, qapp, opts, *args):
        super(ChatWidget, self).__init__(parent, *args)
        self.uid = uid
        # bugfix: self.parent() returns QStackedWidget when chat is open as tab
        self.__parent = parent
        self.manager = manager
        self.options: ChatOptions = opts
        self.app = qapp
        self.gi = self.manager.accounts[uid]
        self.room = self.gi.chat.room_status['name']
        self._max_people = self.gi.chat.room_status['max_people']
        res = utils.get_qt5_resource("chat.ui")
        loadUi(res, self)
        self.cbUserID.setValidator(QIntValidator())
        self.findTextWidget.setVisible(False)
        self.widgetMsgControls.setVisible(False)
        self.txtFind.setClearButtonEnabled(True)
        self._set_table_headers()
        self._listeners = [
            (Events.connection_change, self.uid, self.on_connection_change),
            (Events.self_leave, self.uid, self.on_self_leave),
            (Events.user_join, self.uid, self.on_user_join),
            (Events.user_leave, self.uid, self.on_user_leave),
            (Events.public_msg, self.uid, self.on_public_msg),
            (Events.private_msg, self.uid, self.on_private_msg),
            (Events.friend_msg, self.uid, self.on_friend_msg),
            (Events.clan_msg, self.uid, self.on_clan_msg)
        ]
        if self.options.highlight:
            self.__highlighter = Highlighter(self.txtMessages.document())
        else:
            self.__highlighter = None
        self._last_msg = ""
        self.__set_listeners()
        self._total_msgs = 0
        self.__preload_old_messages()
        self._userlist_items: {int: UserlistItem} = {}
        self.__context_uid = self.uid
        self.__create_menus()

    @property
    def split_messages(self):
        return self.actionSplit_messages.isChecked()

    @property
    def replace_deletable(self):
        return self.actionEncode_links.isChecked()

    @property
    def encode_links(self):
        return self.actionSplit_messages.isChecked()

    @property
    def reply_on_userlist_change(self):
        return self.actionReply_userlist.isChecked()

    @property
    def show_notifications(self):
        return self.actionShow_notifications.isChecked()

    def cleanup(self):
        for args in self._listeners:
            self.manager.remove_listener(*args)

    def __set_listeners(self):
        for args in self._listeners:
            self.manager.register_listener(*args)

    def __create_menus(self):
        self.actionEncode_links.setChecked(self.options.encode_links)
        self.actionShow_notifications.setChecked(self.options.notifications)
        self.actionReplace_deletable.setChecked(self.options.replace_deletable)
        self.actionSplit_messages.setChecked(self.options.split_messages)
        self.actionEnable_highlight.setChecked(self.options.highlight)
        self.__options_menu = QMenu("options")
        self.__options_menu.addActions([
            self.actionLeave_room, self.actionClear_chat,
            self.actionCopy_room_name, self.actionReply_userlist,
            self.actionShow_notifications, self.actionEnable_highlight])
        self.__submenu = QMenu("More")
        self.__submenu.addAction(self.actionEncode_links)
        self.__submenu.addAction(self.actionReplace_deletable)
        self.__submenu.addAction(self.actionSplit_messages)
        self.__options_menu.addMenu(self.__submenu)
        self.tbtnOptions.setMenu(self.__options_menu)
        self.__userlist_menu = QMenu("blocking")
        self.__userlist_menu.addActions([self.actionBlock, self.actionUnblock])
        self.__userlist_menu.addSeparator()
        self.__userlist_menu.addActions([self.actionProfile, self.actionPM])
        self.__options_menu.addAction(self.actionFind)

    def _set_table_headers(self):
        items = ["Nick", "User id"]
        model = QStandardItemModel()
        model.setHorizontalHeaderLabels(items)
        self.tbPeople.setModel(model)
        self.tbPeople.horizontalHeader().resizeSection(0, 150)
        self.tbPeople.horizontalHeader().resizeSection(1, 80)
        self.tbPeople.verticalHeader().setVisible(False)

    def __preload_old_messages(self):
        last_date = None
        preload = self.options.preload
        for d in self.manager.get_last_x_messages(self.room, preload):
            datetime, *rest = d
            last_date = datetime
            self.append_public_message(*rest)
        if last_date:
            m = (f"<font color={self.COLOR_SPECIAL}>"
                 f"[Last message sent at: {last_date}]</font><br />")
            self.txtMessages.appendHtml(m)

    def reload_userlist(self):
        for uid in self._userlist_items:
            self.__del_from_userlist(uid, None, None)
        userlist = ((uid, self.gi.chat.userlist[uid], 0)
                    for uid in self.gi.chat.userlist)
        for data in userlist:
            self.__add_to_userlist(*data)
        self.update_counters()

    def __add_to_userlist(self, uid, nick, _):
        if self.reply_on_userlist_change:
            self.send_message(self.FORMAT_USERLIST_IN_REPLY.format(
                uid=uid, nick=nick
            ))
        item = UserlistItem(nick, str(uid))
        self._userlist_items[uid] = item
        model = self.tbPeople.model()
        model.appendRow([item.nickname, item.uid])

    def __del_from_userlist(self, uid, nick, __):
        if self.reply_on_userlist_change:
            self.send_message(self.FORMAT_USERLIST_OUT_REPLY.format(
                uid=uid, nick=nick
            ))
        try:
            self.tbPeople.model().takeRow(self._userlist_items[uid].uid.row())
        except KeyError:
            self.reload_userlist()

    def on_connection_change(self, *args):
        self.app.postEvent(self, events.ConnStatusChange(*args))

    def on_self_leave(self, *args):
        self.app.postEvent(self, events.RoomLeave(*args))

    def on_user_join(self, *args):
        self.app.postEvent(self, events.ChatUserJoin(*args))

    def on_user_leave(self, *args):
        self.app.postEvent(self, events.ChatUserLeave(*args))

    def on_public_msg(self, *args):
        self.app.postEvent(self, events.PublicMsg(*args))

    def on_private_msg(self, *args):
        self.app.postEvent(self, events.PrivateMsg(*args))

    def on_friend_msg(self, *args):
        self.app.postEvent(self, events.FriendMsg(*args))

    def on_clan_msg(self, *args):
        self.app.postEvent(self, events.ClanMsg(*args))

    def customEvent(self, event):
        if isinstance(event, events.PublicMsg):
            self.append_public_message(*event.data)
            self.update_counters()
        elif isinstance(event, events.PrivateMsg):
            self.append_private_message(*event.data)
        elif isinstance(event, events.FriendMsg):
            self.append_friend_msg(*event.data)
        elif isinstance(event, events.ClanMsg):
            self.append_clan_msg(*event.data)
        elif isinstance(event, events.ChatUserJoin):
            self.__add_to_userlist(*event.data)
            self.update_counters()
        elif isinstance(event, events.ChatUserLeave):
            self.__del_from_userlist(*event.data)
            self.update_counters()
        elif isinstance(event, events.RoomLeave):
            self.self_close()
        elif isinstance(event, events.ConnStatusChange):
            if event.data[0] in [ConnState.CONNECTED_AWAITING,  # left room
                                 ConnState.DISCONNECTED]:
                self.self_close()

    def append_public_message(self, nick, uid, msg, msgid):
        message = self.FORMAT_PUBLIC_MSG.format(
            nick=nick, uid=uid, msg=msg, msgid=msgid)
        escaped = escape(message)
        self.txtMessages.appendHtml(f"<font>{escaped}</font>")
        self.show_notification(msg, message, True)
        self._total_msgs += 1

    def append_private_message(self, nick, sid, rid, msg, msgid):
        message = self.FORMAT_PRIVATE_MSG.format(
            nick=nick, sid=sid, rid=rid, msg=msg, msgid=msgid)
        escaped = escape(message)
        self.txtMessages.appendHtml(
            f"<font color={self.COLOR_PV}>{escaped}</font>")
        self.show_notification("", message, False)

    def append_friend_msg(self, sid, rid, msg, msgid):
        message = self.FORMAT_FRIEND_MSG.format(
            sid=sid, rid=rid, msg=msg, msgid=msgid)
        escaped = escape(message)
        self.txtMessages.appendHtml(
            f"<font color={self.COLOR_FRIEND}>{escaped}</font>")
        self.show_notification("", message, False)

    def append_clan_msg(self, nick, uid, msg, role, msgid):
        message = self.FORMAT_CLAN_MSG.format(
            nick=nick, uid=uid, role=role, msg=msg, msgid=msgid)
        escaped = escape(message)
        self.txtMessages.appendHtml(
            f"<font color={self.COLOR_CLAN}>{escaped}</font>")
        self.show_notification(msg, message, True)

    def self_close(self):
        self.leaving_room.emit()

    def show_notification(self, message, formatted, public):
        if self.show_notifications and not self.isActiveWindow():
            if public:
                self.notify_public.emit(message, self.room, formatted)
            else:
                self.notify_private.emit(self.room, formatted)

    def update_counters(self):
        self.lblPeople.setText(
            f"{str(self.tbPeople.model().rowCount())}/{self._max_people}")
        self.lblMessageCounter.setText(str(self._total_msgs))

    def send_message(self, predefined=None):
        to_uid = self.cbUserID.currentText()
        if not to_uid:
            self.cbUserID.setCurrentText("0")
            self.rbPublic.click()
            return
        text = predefined
        if not text:
            text = self.txtMessage.text()
            self._last_msg = text
        if self.split_messages:
            n = 100
            if self.rbPrivate.isChecked():
                n = 200
            message, remaining = self.split_at_n(text, n)
        else:
            message = text
            remaining = ""
        if self.encode_links:
            message = utils.encode_links(message, self.options.link_replacement)
        if self.replace_deletable:
            message = utils.replace_deletable(message)
        if not predefined:
            self.txtMessage.setText(remaining)
        if self.rbPublic.isChecked():
            self.gi.chat.send_chat_message(message)
        elif self.rbPrivate.isChecked():
            self.gi.chat.send_private_message(message, int(to_uid))
        elif self.rbFriend.isChecked():
            self.gi.chat.send_friend_message(message, int(to_uid))
        elif self.rbClan.isChecked():
            self.gi.chat.send_clan_message(message)

    def set_blocking_visibility(self, uid):
        if not uid:
            self.actionUnblock.setVisible(False)
            self.actionBlock.setVisible(False)
            return
        block = True
        if uid in self.manager.blacklist:
            block = False
        self.actionUnblock.setVisible(not block)
        self.actionBlock.setVisible(block)

    def select_for_private_message(self, player_id):
        self.cbUserID.setCurrentText(str(player_id))
        self.rbPrivate.click()
        self.btnPlus.setChecked(True)

    def select_for_profile_view(self, player_id):
        profile = PlayerProfile(self.__parent, self.manager, self.uid, self.app,
                                player_id=player_id)
        profile.show()

    def __find(self, backwards, sensitive, regex):
        flags = QTextDocument.FindFlags()
        if backwards:
            flags |= QTextDocument.FindBackward
        if sensitive:
            flags |= QTextDocument.FindCaseSensitively
        if regex:
            regex = QRegExp(self.txtFind.text(), sensitive)
            self.txtMessages.find(regex, flags)
        else:
            self.txtMessages.find(self.txtFind.text(), flags)

    def find_next(self, sensitive, regex):
        self.__find(False, sensitive, regex)

    def find_previous(self, sensitive, regex):
        self.__find(True, sensitive, regex)

    @pyqtSlot(name="on_btnPrevious_clicked")
    def on_find_previous(self):
        self.find_previous(self.btnCaseSensitive.isChecked(),
                           self.btnRegex.isChecked())

    @pyqtSlot(name="on_btnNext_clicked")
    def on_find_next(self):
        self.find_next(self.btnCaseSensitive.isChecked(),
                       self.btnRegex.isChecked())

    @pyqtSlot(name="on_txtFind_returnPressed")
    def on_find_text_return_pressed(self):
        self.find_next(self.btnCaseSensitive.isChecked(),
                       self.btnRegex.isChecked())

    @pyqtSlot(name="on_txtMessage_returnPressed")
    def on_txt_message_return_pressed(self):
        self.send_message()

    @pyqtSlot(name="on_btnSend_clicked")
    def on_btn_send_message_clicked(self):
        self.send_message()

    @pyqtSlot(name="on_btnCloseFind_clicked")
    def on_btn_close_find_clicked(self):
        self.findTextWidget.setVisible(False)

    @pyqtSlot(name="on_btnRewrite_clicked")
    def on_btn_rewrite_clicked(self):
        self.txtMessage.setText(self._last_msg)

    @pyqtSlot(bool, name="on_actionEnable_highlight_toggled")
    def on_action_enable_highlight_triggered(self, state):
        if state:
            self.__highlighter = Highlighter(self.txtMessages.document())
        else:
            if self.__highlighter:
                self.__highlighter.setDocument(None)
            self.__highlighter = None

    @pyqtSlot(name="on_actionCopy_room_name_triggered")
    def on_action_copy_room_triggered(self):
        self.app.clipboard().setText(self.room)

    @pyqtSlot(name="on_actionLeave_room_triggered")
    def on_action_leave_room(self):
        self.gi.chat.send_lobby_leave_request()

    @pyqtSlot(name="on_actionFind_triggered")
    def on_action_find(self):
        self.findTextWidget.setVisible(not self.findTextWidget.isVisible())

    @pyqtSlot(name="on_actionClear_chat_triggered")
    def on_action_clear_chat_room(self):
        self._total_msgs = 0
        self.txtMessages.clear()

    @pyqtSlot(name="on_actionBlock_triggered")
    def on_action_block(self):
        self.manager.blacklist_player(self.__context_uid)

    @pyqtSlot(name="on_actionUnblock_triggered")
    def on_action_unblock(self):
        self.manager.remove_from_blacklist(self.__context_uid)

    @pyqtSlot(name="on_actionProfile_triggered")
    def on_action_view_profile(self):
        self.select_for_profile_view(self.__context_uid)

    @pyqtSlot(name="on_actionPM_triggered")
    def on_action_private_message(self):
        self.select_for_private_message(self.__context_uid)

    @pyqtSlot(QPoint, name="on_tbPeople_customContextMenuRequested")
    def on_userlist_menu_context(self, point: QPoint):
        index: QModelIndex = self.tbPeople.indexAt(point)
        if not index.isValid():
            return
        model = self.tbPeople.model()
        uid = int(model.item(index.row(), self.COLUMN_ID).text())
        self.set_blocking_visibility(uid)
        self.__context_uid = uid  # ugly, but there's no other way
        self.__userlist_menu.exec(
            self.tbPeople.viewport().mapToGlobal(point))

    @pyqtSlot(QModelIndex, name="on_tbPeople_pressed")
    def on_tb_people_pressed(self, index: QModelIndex):
        item = self.tbPeople.model().item(index.row(), self.COLUMN_ID)
        if self.__highlighter:
            self.__highlighter.set_regex_uid(item.text())
            self.__highlighter.rehighlight()

    @pyqtSlot(QModelIndex, name="on_tbPeople_activated")
    def on_tb_people_actived(self, index: QModelIndex):
        if index.column() == self.COLUMN_NICK:
            item = self.tbPeople.model().item(index.row(), self.COLUMN_ID)
            if not item:
                return
            player_id = item.text()
            self.select_for_profile_view(player_id)
        elif index.column() == self.COLUMN_ID:
            item = self.tbPeople.model().item(index.row(), self.COLUMN_ID)
            if not item:
                return
            self.select_for_private_message(item.text())

    @pyqtSlot(name="on_rbPublic_clicked")
    def on_msgtype_public_selected(self):
        self.cbUserID.setEnabled(False)

    @pyqtSlot(name="on_rbClan_clicked")
    def on_msgtype_public_selected(self):
        self.cbUserID.setEnabled(False)

    @pyqtSlot(name="on_rbFriend_clicked")
    def on_msgtype_public_selected(self):
        self.cbUserID.setEnabled(True)

    @pyqtSlot(name="on_rbPrivate_clicked")
    def on_msgtype_public_selected(self):
        self.cbUserID.setEnabled(True)

    @staticmethod
    def split_at_n(message: str, n: int = 100, truncate=False):
        if len(message) >= n:
            p = message[:n].rfind(" ")
            if p >= 0 and not truncate:
                return message[:p], message[p + 1:]  # skip space before word
            else:
                return message[:n], message[n:]
        else:
            return message, ""
