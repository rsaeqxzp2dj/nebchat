from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDockWidget


class Dock(QDockWidget):
    closing = pyqtSignal()

    def __init__(self, *args):
        super(Dock, self).__init__(*args)

    def closeEvent(self, _):
        self.closing.emit()

    def close(self):
        self.closing.emit()
        self.deleteLater()
