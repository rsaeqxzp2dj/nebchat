import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QMainWindow

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Window(QMainWindow):
    trancedent_instances = []
    closing = pyqtSignal()

    def __init__(self, parent=None, *args):
        super(Window, self).__init__(parent=None, *args)
        self.instances = []
        self.reimplemented_parent = parent
        if isinstance(parent, Window):
            self.setWindowIcon(parent.windowIcon())
            parent.instances.append(self)
            parent.closing.connect(self.close)
        else:
            self.trancedent_instances.append(self)

    def closeEvent(self, _):
        self.closing.emit()
        if self in self.instances:
            self.reimplemented_parent.remove(self)
        elif self in Window.trancedent_instances:
            Window.trancedent_instances.remove(self)

    def close(self):
        self.closing.emit()
        self.deleteLater()
