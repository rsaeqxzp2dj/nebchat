# from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from sample.misc import utils


class Graph(QDialog):
    def __init__(self, parent, *args):
        super(Graph, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("graph.ui")
        loadUi(res, self)
