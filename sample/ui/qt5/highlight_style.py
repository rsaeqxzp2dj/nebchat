from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QTextCharFormat
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QColorDialog
import json
from PyQt5.uic import loadUi
from enum import Enum
from sample.misc import utils
from .custom_classes.highlighter import Highlighter
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class UnderlineStyle(Enum):
    NoUnderline = QTextCharFormat.NoUnderline
    SingleUnderline = QTextCharFormat.SingleUnderline
    DashUnderline = QTextCharFormat.DashUnderline
    DotLine = QTextCharFormat.DotLine
    DashDotLine = QTextCharFormat.DashDotLine
    DashDotDotLine = QTextCharFormat.DashDotDotLine
    WaveUnderline = QTextCharFormat.WaveUnderline
    SpellCheckUnderline = QTextCharFormat.SpellCheckUnderline


highlight_vars = [
    "underline",
    "underline_style",
    "underline_color",
    "bold",
    "italic",
    "foreground",
    "background"
]


class HighlightStyle(QDialog):
    FILENAME = "highlight_style.json"

    def __init__(self, parent, *args):
        super(HighlightStyle, self).__init__(parent, *args)
        res = utils.get_qt5_resource("highlight_style.ui")
        loadUi(res, self)
        self._background = Highlighter.background
        self._foreground = Highlighter.foreground
        self._underline_color = Highlighter.underline_color
        self._underline_style = UnderlineStyle(Highlighter.underline_style)
        self._underline = Highlighter.underline
        self.__highlighter = Highlighter(self.txtPreview.document())
        self.__highlighter.set_regex_uid(24680)
        self.cbBold.setChecked(Highlighter.bold)
        self.cbItalic.setChecked(Highlighter.italic)
        for style in UnderlineStyle:
            self.cbStyles.addItem(style.name)
        self.load()

    def update_preview(self):
        self.__highlighter.rehighlight()

    @pyqtSlot(name="on_btnsAction_accepted")
    def on_ok(self):
        self.__set_all()
        self.close()

    def __set_all(self):
        for i in highlight_vars:
            setattr(Highlighter, i, getattr(self, i))

    def save(self):
        self.__set_all()
        try:
            with open(utils.user_data(HighlightStyle.FILENAME), "w") as f:
                d = {}
                for i in highlight_vars:
                    attr = getattr(Highlighter, i)
                    if isinstance(attr, UnderlineStyle):
                        attr = attr.value
                    elif isinstance(attr, QColor):
                        attr = attr.name()
                    d[i] = attr
                json.dump(d, f, indent=2)
        except Exception as e:
            logger.error(f"coudlnt save highlight style: {e}")

    def load(self):
        try:
            with open(utils.user_data(HighlightStyle.FILENAME)) as f:
                try:
                    data = json.load(f)
                except json.JSONDecodeError:
                    return
                for i in highlight_vars:
                    try:
                        setattr(self, i, data[i])
                    except Exception:
                        continue
        except FileNotFoundError:
            return
        except OSError:
            return
        self.__set_all()
        self.update_preview()

    @pyqtSlot(name="on_btnSave_clicked")
    def on_btn_save(self):
        self.save()

    @pyqtSlot(name="on_btnsAction_rejected")
    def on_cancel(self):
        self.close()

    @property
    def bold(self):
        return self.cbBold.isChecked()

    @bold.setter
    def bold(self, value):
        self.cbBold.setChecked(utils.s2b(value, False))

    @property
    def italic(self):
        return self.cbItalic.isChecked()

    @italic.setter
    def italic(self, value):
        self.cbItalic.setChecked(utils.s2b(value, False))

    @property
    def underline(self):
        return self.gbUnderline.isChecked()

    @underline.setter
    def underline(self, value):
        self._underline = utils.s2b(value, False)
        self.__highlighter.set_underline(self._underline, self._underline_color,
                                         self._underline_style.value)
        self.update_preview()

    @property
    def underline_style(self):
        if self.gbUnderline.isChecked():
            return self._underline_style.value
        return UnderlineStyle.NoUnderline

    @underline_style.setter
    def underline_style(self, value):
        self._underline_style = UnderlineStyle(value)
        self.__highlighter.set_underline(self._underline, self._underline_color,
                                         self._underline_style.value)
        self.update_preview()

    @property
    def underline_color(self):
        return self._underline_color

    @underline_color.setter
    def underline_color(self, value):
        value = QColor(value)
        self._underline_color = value
        self.__highlighter.set_underline(self._underline, value,
                                         self._underline_style.value)
        self.btnUnderlineColor.setStyleSheet(f"background-color: {value.name()}")
        self.update_preview()

    @property
    def background(self):
        if self.cbBackground.isChecked():
            return self._background

    @property
    def foreground(self):
        if self.cbForeground.isChecked():
            return self._foreground

    @background.setter
    def background(self, value):
        if value is None:
            self.cbBackground.setChecked(False)
            return
        self.cbBackground.setChecked(True)
        value = QColor(value)
        self._background = value
        self.btnBackground.setStyleSheet(f"background-color: {value.name()}")
        self.__highlighter.set_background_color(value)
        self.update_preview()

    @foreground.setter
    def foreground(self, value):
        if value is None:
            self.cbForeground.setChecked(False)
            return
        self.cbForeground.setChecked(True)
        value = QColor(value)
        self._foreground = value
        self.btnForeground.setStyleSheet(f"background-color: {value.name()}")
        self.__highlighter.set_foreground_color(value)
        self.update_preview()

    @pyqtSlot(bool, name="on_cbBackground_toggled")
    def on_cb_background_enable(self, state):
        if not state:
            self.btnBackground.setStyleSheet("")
            self.__highlighter.set_background_color(None)
            self.update_preview()
        else:
            self.background = QColor(self._background)

    @pyqtSlot(bool, name="on_cbForeground_toggled")
    def on_cb_foreground_enable(self, state):
        if not state:
            self.btnForeground.setStyleSheet("")
            self.__highlighter.set_foreground_color(None)
            self.update_preview()
        else:
            self.foreground = QColor(self._foreground)

    @pyqtSlot(name="on_btnBackground_clicked")
    def on_sel_background_color(self):
        # noinspection PyArgumentList
        color = QColorDialog.getColor(initial=self._background,
                                      options=QColorDialog.DontUseNativeDialog)
        if not QColor.isValid(color):
            return  # when user closes the color window
        self.background = color

    @pyqtSlot(name="on_btnForeground_clicked")
    def on_sel_foreground_color(self):
        # noinspection PyArgumentList
        color = QColorDialog.getColor(initial=self._foreground,
                                      options=QColorDialog.DontUseNativeDialog)
        if not QColor.isValid(color):
            return  # when user closes the color window
        self.foreground = color

    @pyqtSlot(name="on_btnUnderlineColor_clicked")
    def on_btn_undeline_color(self):
        # noinspection PyArgumentList
        color = QColorDialog.getColor(initial=self._underline_color,
                                      options=QColorDialog.DontUseNativeDialog)
        if not QColor.isValid(color):
            return  # when user closes the color window
        self.underline_color = color

    @pyqtSlot(bool, name="on_gbUnderline_toggled")
    def on_underline_toggle(self, state):
        self.underline = state

    @pyqtSlot(int, name="on_cbStyles_activated")
    def on_style_selection_change(self, index):
        self.underline_style = index

    @pyqtSlot(bool, name="on_cbBold_toggled")
    def on_bold_toggle(self, state):
        self.__highlighter.set_bold(state)
        self.update_preview()

    @pyqtSlot(bool, name="on_cbItalic_toggled")
    def on_italic_toggle(self, state):
        self.__highlighter.set_italic(state)
        self.update_preview()
