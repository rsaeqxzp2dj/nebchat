from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QFont
from PyQt5.uic import loadUi
import sys
from sample.misc import utils
from sample.ui.qt5.custom_widgets.window import Window


class Interpreter(Window):
    def __init__(self, parent, plocals, pglobals, *args):
        super(Interpreter, self).__init__(parent=parent, *args)
        res = utils.get_qt5_resource("interpreter.ui")
        loadUi(res, self)
        self.parent_ = parent
        self.pglobals = pglobals
        self.plocals = plocals
        font = QFont("Monospace")
        font.setStyleHint(QFont.Monospace)
        self.txtResult.setFont(font)
        self.txtCode.setFont(font)

    @staticmethod
    def flush():
        return "\n"

    def write(self, text):
        self.txtResult.insertPlainText(text)

    @pyqtSlot(name="on_actionClear_triggered")
    def on_clear_output(self):
        self.txtResult.clear()

    @property
    def scope(self):
        if self.cbParentScope.isChecked():
            return self.plocals, self.pglobals
        return locals(), globals()

    def run(self, func):
        loc, glob = self.scope
        try:
            default_out = sys.stdout
            sys.stdout = self
            r = func(self.txtCode.toPlainText(), loc, glob)
            self.write(str(r))
            sys.stdout = default_out
        except Exception as er:
            fmt = f"<font color=red background-color=black>{er}</font>\n"
            self.txtResult.appendHtml(fmt)

    @pyqtSlot(name="on_actionExec_triggered")
    def on_run(self):
        self.run(exec)

    @pyqtSlot(name="on_actionEval_triggered")
    def on_eval(self):
        self.run(eval)
