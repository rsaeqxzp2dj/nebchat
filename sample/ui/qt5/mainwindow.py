import logging
import sys

from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMessageBox
from PyQt5.uic import loadUi

from sample.bot.bot import Bot
from sample.leech.data_leeching import DataLeeching
from sample.misc.utils import get_qt5_resource
from sample.ui.qt5 import configurations as configs
from sample.ui.qt5.interpreter import Interpreter
from sample.ui.qt5.moderators import Moderators
from sample.ui.qt5.profile import PlayerProfile
from .account_manager import AccountManager
from .add_account import AddAccount
from .chatmanager import ChatManager
from .custom_classes.tray_icon import SysTrayIcon
from .custom_widgets.accounts_state_widget import AccountsStateWidget
from .custom_widgets.chat_widget import ChatOptions
from .custom_widgets.chat_widget import ChatWidget
from .custom_widgets.dock import Dock
from .custom_widgets.window import Window
from .graph import Graph
from .room_manager import RoomManager

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class MainWindow(Window):
    """
    This is the parent of all subsequent windows for the PyQt5 front-end. This
    class is inheriting Window to fix the lack of child windows not having their
    own taskbar entry on Windows.
    """
    app = QApplication(sys.argv)
    NEW_CHAT_WIN_TITLE = 'connected as "{nick}" in room "{room}"'
    NEW_CHAT_TAB_TITLE = '{room}'
    WIN_SETTINGS_NAME = {
        RoomManager: "win_room_mngr",
        ChatManager: "win_chat_mngr",
        Interpreter: "win_interpreter"
    }
    # todo: implement main window's status

    def __init__(self, manager, use_leeching, use_bot, *args):
        super(MainWindow, self).__init__(*args)
        res = get_qt5_resource("mainwindow.ui")
        loadUi(res, self)
        logger.setLevel(logging.DEBUG)
        self.account_manager = manager

        self.leech = DataLeeching(manager) if use_leeching else None
        self.bot = Bot(manager) if use_bot else None

        self.tray = SysTrayIcon(self)
        self.tray.show()

        # preload windows, i dont want multiple instances of those
        self.win_chat_mngr = ChatManager(self, manager, self.app)
        self.win_acc_mngr = AccountManager(self, manager)
        self.win_configs = configs.Configurations(self, self.tray, self.leech, self.bot)
        self.win_room_mngr = RoomManager(self, manager, self.win_configs, self.app)
        self.win_moderators = Moderators(self, manager)
        self.win_graph = Graph(self)
        self.win_interpreter = Interpreter(self, locals(), globals())

        # noinspection PyTypeChecker
        self.WIN_SETTINGS_NAME.update({MainWindow: "main_window"})
        self.restore_window(self)

        self.accounts_state = AccountsStateWidget(self, self.account_manager,
                                                  self.app)
        self.gbAccountsState.layout().addWidget(self.accounts_state)

        self.__setup_connections()

        self.show()
        sys.exit(MainWindow.app.exec_())

    def closeEvent(self, _):
        if self.leech:
            self.leech.stop(True)
        if self.bot:
            self.bot.stop()
        self.account_manager.cleanup()
        self.win_chat_mngr.cleanup()
        for instance in Window.trancedent_instances:
            instance.close()
        self.closing.emit()

    def __setup_leeching(self):
        if self.win_configs:
            pass

    def __setup_connections(self):
        self.win_acc_mngr.account_added.connect(
            self.win_chat_mngr.on_account_added)

        self.win_acc_mngr.account_removed.connect(
            self.win_chat_mngr.on_account_removed)

        self.win_acc_mngr.account_added.connect(
            self.accounts_state.on_account_added)

        # the Manager removes any listener set for a deleted account
        self.win_acc_mngr.account_added.connect(
            lambda x: self.leech.update_listeners())

        self.win_acc_mngr.account_removed.connect(
            self.accounts_state.on_account_removed)

        self.accounts_state.account_connect.connect(
            self.win_room_mngr.on_account_connect)

        self.accounts_state.account_disconnect.connect(
            self.win_room_mngr.on_account_disconnect)

        self.win_room_mngr.open_chat_signal.connect(self.open_chat)
        self.win_chat_mngr.open_chat_signal.connect(self.open_chat)
        self.accounts_state.account_disconnect.connect(
            self.win_chat_mngr.on_account_disconnect)

        self.win_room_mngr.closing.connect(self.save_pos(self.win_room_mngr))
        self.win_chat_mngr.closing.connect(self.save_pos(self.win_chat_mngr))
        self.win_interpreter.closing.connect(self.save_pos(self.win_interpreter))
        self.closing.connect(self.save_pos(self))

    def save_pos(self, instance):
        name = self.WIN_SETTINGS_NAME[instance.__class__]

        def func():
            self.win_configs.save_window(name, instance.saveGeometry())
        return func

    def restore_window(self, instance):
        name = self.WIN_SETTINGS_NAME[instance.__class__]
        windata = self.win_configs.restore_window(name)
        if windata:
            instance.restoreGeometry(windata)
        return instance

    def __chat_notify_public(self, test_msg, title, body):
        if self.tray.test_message(test_msg):
            self.tray.showMessage(title, body)

    def open_chat(self, uid, option=None):
        gi = self.account_manager.accounts[uid]
        nick = gi.chat.nickname
        room = gi.chat.room_status['name']
        cfg = self.win_configs

        # use defaults when joining room, or option when from chatmanager
        compare_to = cfg.join_form.value if option is None else option

        options = ChatOptions(cfg.encode_links, cfg.replace_deletable,
                              cfg.split_msg,  cfg.link_replacement.name,
                              cfg.notifications, cfg.load_last_messages,
                              cfg.highlights)

        chat = ChatWidget(self, uid, self.account_manager, self.app, options)
        chat.notify_private.connect(self.tray.showMessage)
        chat.notify_public.connect(self.__chat_notify_public)

        # i must use .value from these Enums here, the signal emits type int
        if compare_to == configs.ChatContainer.Window.value:
            self.open_chat_as_window(nick, room, chat)

        elif compare_to == configs.ChatContainer.Tab.value:
            self.open_chat_as_tab(nick, room, chat)

        elif compare_to == configs.ChatContainer.Dock.value:
            self.open_chat_as_dock(nick, room, chat)

    def open_chat_as_window(self, nick, room, chat):
        window = Window(parent=self)
        chat.leaving_room.connect(window.close)
        window.setCentralWidget(chat)
        window.setWindowTitle(self.NEW_CHAT_WIN_TITLE.format(
            nick=nick, room=room))
        window.closing.connect(chat.cleanup)
        window.show()
        # bugfix: duplicated userlist
        # noinspection PyCallByClass,PyTypeChecker
        QTimer.singleShot(1000, chat.reload_userlist)

    def open_chat_as_dock(self, nick, room, chat):
        dock = Dock(self.NEW_CHAT_TAB_TITLE.format(
            nick=nick, room=room), self)
        chat.leaving_room.connect(dock.close)

        dock.setWidget(chat)
        dock.closing.connect(chat.cleanup)
        self.addDockWidget(Qt.TopDockWidgetArea, dock)
        # bugfix: duplicated userlist
        # noinspection PyCallByClass,PyTypeChecker
        QTimer.singleShot(1000, chat.reload_userlist)

    def open_chat_as_tab(self, nick, room, chat):
        index = self.tbMain.addTab(chat, self.NEW_CHAT_TAB_TITLE.format(
            nick=nick, room=room))

        def on_leaving():
            chat.cleanup()
            self.tbMain.removeTab(index)
        chat.leaving_room.connect(on_leaving)
        # bugfix: duplicated userlist
        # noinspection PyCallByClass,PyTypeChecker
        QTimer.singleShot(1000, chat.reload_userlist)

    @pyqtSlot(name="on_actionPeopleData_triggered")
    def on_action_people_data_triggered(self):
        accounts = list(self.account_manager.accounts)
        if not accounts:
            msgbox = QMessageBox(self)
            msg = ("No accounts available to view users data.\n"
                   "Unfortunately the game needs an account to view other"
                   "people's data.\nAdd an account to use this feature.")
            msgbox.critical(msgbox, "error", msg, QMessageBox.Ok)
            logger.error(msg)
            return
        account = accounts[0]
        profile = PlayerProfile(self, self.account_manager, account, self.app)
        profile.show()

    @pyqtSlot(name="on_actionPython_interpreter_triggered")
    def on_py_interpreter_triggered(self):
        self.restore_window(self.win_interpreter).show()
        self.win_interpreter.activateWindow()

    @pyqtSlot(name="on_actionView_mods_triggered")
    def on_action_view_mods_triggered(self):
        self.win_moderators.show()

    @pyqtSlot(name="on_actionGraph_triggered")
    def on_action_graph_triggered(self):
        self.win_graph.show()

    @pyqtSlot(name="on_actionAccountManager_triggered")
    def on_action_account_manager_triggered(self):
        self.win_acc_mngr.show()

    @pyqtSlot(name="on_actionChangeConfig_triggered")
    def on_action_change_config_triggered(self):
        self.win_configs.show()

    @pyqtSlot(name="on_actionAvailableRooms_triggered")
    def on_action_available_rooms(self):
        self.restore_window(self.win_room_mngr).show()
        self.win_room_mngr.activateWindow()

    @pyqtSlot(name="on_actionViewChats_triggered")
    def on_action_view_chats(self):
        self.restore_window(self.win_chat_mngr).show()
        self.win_chat_mngr.activateWindow()

    @pyqtSlot(name="on_btnViewChats_clicked")
    def on_btn_view_chats_clicked(self):
        self.restore_window(self.win_chat_mngr).show()
        self.win_chat_mngr.activateWindow()

    @pyqtSlot(name="on_btnConfigurations_clicked")
    def on_btn_configutations_clicked(self):
        self.win_configs.show()

    @pyqtSlot(name="on_btnThemes_clicked")
    def on_btn_themes_clicked(self):
        self.win_configs.theme_editor.show()

    @pyqtSlot(int, name="on_tbMain_tabCloseRequested")
    def on_tb_main_close_request(self, index):
        if self.tbMain.tabText(index) == "Quick access":
            return
        self.tbMain.widget(index).cleanup()  # chat_widget
        self.tbMain.removeTab(index)

    @pyqtSlot(name="on_btnAddAccountQuick_clicked")
    def on_btn_add_account_quick_clicked(self):
        win_add_account = AddAccount(self, self.account_manager)
        win_add_account.account_added.connect(self.win_acc_mngr.on_add_account)
        win_add_account.show()

    @pyqtSlot(name="on_btnViewRooms_clicked")
    def on_btn_view_rooms_clicked(self):
        self.restore_window(self.win_room_mngr).show()
        self.win_room_mngr.activateWindow()
