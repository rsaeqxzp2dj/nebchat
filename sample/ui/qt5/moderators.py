
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal
from PyQt5.uic import loadUi
from sample.core.networking.account import Account

from sample.misc import utils
from sample.ui.qt5.custom_classes.runnable import async_call


class Moderators(QDialog):
    mod_list_update = pyqtSignal(object)

    def __init__(self, parent, manager, *args):
        super(Moderators, self).__init__(parent, *args)
        res = utils.get_qt5_resource("moderators.ui")
        loadUi(res, self)
        self.manager = manager
        self.mod_list_update.connect(self.refresh_list_result)
        self.msgbox = QMessageBox(self)
        self.set_table()
        self.request()

    def set_table(self):
        items = ["ID", "Nick", "Clan", "Level", "Last played"]
        model = QStandardItemModel()
        self.tbMods.setModel(model)
        model.setHorizontalHeaderLabels(items)
        self.tbMods.horizontalHeader().resizeSection(1, 150)
        self.tbMods.horizontalHeader().resizeSection(4, 180)
        self.tbMods.verticalHeader().setVisible(False)

    def refresh_list_result(self, mod_list):
        self.btnRefresh.setEnabled(True)
        if not isinstance(mod_list, dict):
            return
        if mod_list.get("Error"):
            self.msgbox.critical(self.msgbox, "error", mod_list.get("Error"),
                                 QMessageBox.Ok)
            return
        self.tbMods.model().setRowCount(0)
        for m in mod_list.get("Mods"):
            lvl = str(Account.convert_xp_to_level(m['XP']))
            last_played = m['LastPlayedUtc'].replace("T", "  ")
            data = str(m['Id']), m['Name'], m['ClanName'], lvl, last_played
            items = [QStandardItem(i) for i in data]
            self.tbMods.model().appendRow(items)

    def request(self):
        async_call(Account.api_get_mods, self.mod_list_update)
        self.btnRefresh.setEnabled(False)

    @pyqtSlot(name="on_btnClose_clicked")
    def on_btn_close(self):
        self.close()

    @pyqtSlot(name="on_btnRefresh_clicked")
    def on_btn_refresh(self):
        self.request()
