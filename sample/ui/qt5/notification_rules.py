import logging

from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtCore import pyqtSlot
from PyQt5.uic import loadUi

from .custom_classes.tray_icon import SysTrayIcon
from sample.misc import utils

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class NotificationRules(QDialog):
    """
    This class is a front-end for SysTrayIcon notification settings
    """

    def __init__(self, parent, tray: SysTrayIcon, *args):
        super(NotificationRules, self).__init__(parent=parent, *args)
        self.tray = tray
        self.rules = []
        res = utils.get_qt5_resource("notifications_rules.ui")
        loadUi(res, self)
        self.load_items()

    def load_items(self):
        for name in self.tray.notification_rules:
            self.lstRules.addItem(name)
            self.rules.append(name)

    @pyqtSlot(QListWidgetItem, name="on_lstRules_itemClicked")
    def on_item_clicked(self, item: QListWidgetItem):
        name = item.text()
        options = self.tray.notification_rules[name]
        self.txtRule.setPlainText(options.rule)
        self.cbEnabled.setChecked(options.enabled)
        self.cbRegex.setChecked(options.regex)
        self.cbCaseSensitive.setChecked(options.case_sensitive)

    @pyqtSlot(name="on_btnRemove_clicked")
    def on_btn_del(self):
        current = self.lstRules.currentItem()
        if not current:
            logger.error("invalid item")
            return
        text = current.text()
        if text not in self.rules:
            logger.error(f"{text} not in rules")
            return
        self.tray.remove_rule(text)
        self.rules.remove(text)
        self.lstRules.takeItem(self.lstRules.row(current))

    @pyqtSlot(name="on_btnAdd_clicked")
    def on_btn_add(self):
        input_dialog = QInputDialog(self, Qt.Dialog)
        name, ok = input_dialog.getText(self, "New rule", "Rule name:",
                                        flags=Qt.Window)
        if not ok:
            return
        if name in self.rules:
            logger.error(f"rule {name} already exists")
            return
        if not name:
            logger.error("invalid rule name")
            return
        self.rules.append(name)
        self.lstRules.addItem(name)
        options = SysTrayIcon.rules_options(False, False, True, "")
        self.tray.set_rule(name, options)

    @pyqtSlot(name="on_btnSetRule_clicked")
    def on_set_rules(self):
        options = SysTrayIcon.rules_options(
            self.cbCaseSensitive.isChecked(),
            self.cbRegex.isChecked(),
            self.cbEnabled.isChecked(),
            self.txtRule.toPlainText()
        )
        item = self.lstRules.currentItem()
        if not item:
            return
        self.tray.set_rule(item.text(), options)
