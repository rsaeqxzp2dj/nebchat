import hashlib
import json
import logging

from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QIntValidator
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtWidgets import QMessageBox
from PyQt5.uic import loadUi

from sample.core.networking.account import Account
from sample.misc import utils
from sample.ui.qt5.custom_classes.runnable import async_call
from .custom_widgets.window import Window

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

DEFAULT_FORMAT = """Nickname: {AccountName}
Level: {level}
Plasma: {CurrentCoins}
Clan: {ClanName}
Clan role: {ClanRole}
Custom skins: {custom_skins}
Purchased avatars: {avatars}
Purchased eject skins: {eject_skins}
Purchased pets: {pets}
Purchased hats: {hats}
Purchased particles: {particles}
Can use colored nicks: {PurchasedAliasColors}
EXP multiplier: {XPMultiplier}
Click type: {ClickType}
"""


def request(func):
    """
    this wrapper simplifies the need to disable buttons and avoid race
    conditions. see account_manager.py for a longer doc description.
    """

    def wrapped(*args, **kwargs):
        self = args[0]
        logger.debug(f"{func.__name__}({str(args[1:])})")
        self._set_actors_enabled(False)
        func(*args, **kwargs)

    return wrapped


def result(func):
    def wrapped(*args, **kwargs):
        self, account_data = args[0], args[1]
        logger.debug(f"{func.__name__}: {str(account_data)[:50]}...")
        msgbox = QMessageBox(self)
        if not account_data:
            logger.error(f"An error ocurred.")
            msgbox.critical(msgbox, func.__name__, "An error has occurred.",
                            QMessageBox.Ok)
            self._set_actors_enabled(True)
            return
        if isinstance(account_data, dict):
            error = account_data.get('Error')  # sometimes account_data is None
            if error:
                msgbox.critical(msgbox, "Error", error, QMessageBox.Ok)
                logger.error(error)
                self._set_actors_enabled(True)
                return
        func(*args, **kwargs)
        self._set_actors_enabled(True)

    return wrapped


class PlayerProfile(Window):
    IMAGES_FOLDER = "skins"

    get_profile = pyqtSignal(object)
    get_skin = pyqtSignal(object)
    get_profile_skin = pyqtSignal(object)

    def __init__(self, parent, account_manager, self_uid, qapp, player_id=0,
                 *args):
        super(PlayerProfile, self).__init__(parent=parent, *args)
        res = utils.get_qt5_resource("profile.ui")
        loadUi(res, self)
        self.manager = account_manager
        self.app = qapp
        self.ac: Account = account_manager.accounts[self_uid].account
        self.cbUserID.setValidator(QIntValidator())
        self.cbUserID.setCurrentText(str(player_id))
        self.get_profile.connect(self.on_get_profile_result)
        self.get_skin.connect(self.on_user_custom_skin_result)
        self.get_profile_skin.connect(self.on_profile_picture_result)
        self.pbProgress.setVisible(False)
        self.cbHistory.setVisible(False)
        self._lbl_history.setVisible(False)
        self.lstCustomSkins.addAction(self.actionCopy_image)
        self.lstCustomSkins.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.__pending_ops = 0
        self.__ops_done = 0
        self.__delayed_closing = False
        if player_id:
            self.pbProgress.setVisible(True)
            self.pbProgress.setMaximum(0)
            self.get_player_profile(player_id)

    def close(self):
        self.closeEvent(None)
        # bugfix: segfault when obj is deleted but the thread tries to emit
        # a signal from the deleted object
        if not self.__pending_ops:
            self.deleteLater()
        else:
            self.__delayed_closing = True

    @property
    def player_id(self):
        value = self.cbUserID.currentText()
        return int(value) if value else 0

    @property
    def option_save(self):
        return self.cbSave.isChecked()

    @pyqtSlot(name="on_btnViewProfile_clicked")
    def on_btn_view_profile(self):
        if self.player_id:
            self.txtProfileJson.clear()
            self.pbProgress.setVisible(True)
            self.pbProgress.setMaximum(0)
            self.get_player_profile(self.player_id)

    @pyqtSlot(name="on_actionCopy_image_triggered")
    def on_copy_image(self):
        items = self.lstCustomSkins.selectedItems()
        if not items:
            return
        item = items[0]
        self.app.clipboard().setPixmap(item.icon().pixmap(128, 128))

    def _set_actors_enabled(self, enable):
        self.__pending_ops += -1 if enable else 1
        if self.__pending_ops and enable:
            return
        elif not self.__pending_ops and enable and self.__delayed_closing:
            self.deleteLater()
        self.btnViewProfile.setEnabled(enable)
        self.cbHistory.setEnabled(enable)
        self.cbUserID.setEnabled(enable)

    def set_pretty_profile_label_data(self, data):
        extra_data = {
            "level": self.ac.convert_xp_to_level(data.get("XP")),
            "custom_skins": len(data.get("ValidCustomSkinIDs")),
            "avatars": len(data.get("PurchasedAvatars")),
            "pets": len(data.get("PurchasedPets")),
            "hats": len(data.get("PurchasedHats")),
            "particles": len(data.get("PurchasedParticles")),
            "eject_skins": len(data.get("PurchasedEjectSkins"))}
        data.update(extra_data)
        self.lblData.setText(DEFAULT_FORMAT.format(**data))

    def get_player_skins(self, data, player_id):
        skins_ids = data.get("ValidCustomSkinIDs")
        if len(skins_ids):
            self.pbProgress.setMaximum(len(skins_ids))
            self.pbProgress.setVisible(True)
            self.__ops_done = 0
            self.pbProgress.setValue(self.__ops_done)
        else:
            self.pbProgress.setVisible(False)
        self.lstCustomSkins.clear()
        for skin_id in map(str, skins_ids):
            self.user_custom_skin_request(player_id, skin_id)

    def save_profile(self, profile, stats):
        self.manager.save_player_profile_data(
            stats['AccountID'],
            stats['AccountName'],
            profile['profile'],
            profile['customSkinID'],
            stats['ValidCustomSkinIDs'],
            stats['XP'],
            stats['ClanName'],
            stats['CurrentCoins']
        )

    def save_image(self, pixmap: QPixmap, image_data, skin_id):
        """
        saves the skin on the user data folder under "skins/<user_id>" folder
        without overwritting if it was modified.
        :param pixmap: pixmap object needed to save the image
        :param image_data: original image data
        :param skin_id: skin identification
        :return: None
        """
        digest = hashlib.sha256(image_data).hexdigest()[:8]
        filename = f"{skin_id}_{digest}.png"
        filepath = utils.user_data_folder(self.IMAGES_FOLDER,
                                          str(self.player_id),
                                          file=filename)
        pixmap.save(filepath, "PNG")

    @request
    def user_custom_skin_request(self, player_id, skin_id):
        async_call(self.ac.get_player_skin, self.get_skin, player_id, skin_id)

    @request
    def get_profile_picture(self, player_id, skin_id):
        async_call(self.ac.get_player_skin, self.get_profile_skin, player_id,
                   skin_id)

    @request
    def get_player_profile(self, player_id):
        async_call(self.ac.get_player_profile, self.get_profile, player_id)

    @result
    def on_get_profile_result(self, data: dict):
        profile_data = data.get('profile_data')
        stats_data = data.get('stats_data')

        text = json.dumps(data, indent=2, ensure_ascii=False)
        self.txtProfileJson.setPlainText(text)
        self.txtProfileText.setPlainText(profile_data.get("profile"))
        self.set_pretty_profile_label_data(stats_data)
        self.get_player_skins(stats_data, self.player_id)
        self.save_profile(profile_data, stats_data)
        custom_skin_id = profile_data.get("customSkinID")
        if custom_skin_id:
            self.get_profile_picture(self.player_id, str(custom_skin_id))
        else:
            img = utils.get_qt5_resource("generic-image.png")
            self.lblProfilePic.setPixmap(QPixmap(img))

    @result
    def on_user_custom_skin_result(self, skin_data):
        skin_id, skin_data = skin_data
        self.__ops_done += 1
        self.pbProgress.setValue(self.__ops_done)
        pixmap = QPixmap()
        if not isinstance(skin_data, bytes):
            logger.error(f"invalid skin data: {type(skin_data)}")
            logger.debug(f"data dump: {skin_data}")
            return
        pixmap.loadFromData(skin_data, "WEBP")
        if self.option_save:
            self.save_image(pixmap, skin_data, skin_id)
        item = QListWidgetItem(skin_id)
        item.setIcon(QIcon(pixmap))
        self.lstCustomSkins.addItem(item)

    @result
    def on_profile_picture_result(self, skin_data):
        skin_id, skin_data_ = skin_data
        pixmap = QPixmap()
        pixmap.loadFromData(skin_data_, "WEBP")
        pixmap = pixmap.scaled(256, 256, Qt.KeepAspectRatioByExpanding)
        self.lblProfilePic.setPixmap(pixmap)
