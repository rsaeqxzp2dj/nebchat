import logging

from PyQt5.QtCore import QModelIndex
from PyQt5.QtCore import QObject
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QProgressBar
from PyQt5.QtWidgets import QInputDialog
from PyQt5.uic import loadUi

from sample.core.manager import Events
from sample.core.networking.game_values import JoinResult
from sample.misc import utils
from sample.ui.qt5.custom_classes import events
from .custom_widgets.window import Window

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class RoomListItem(QObject):
    """
    This is basically what every row in the tbRoom tableView is
    """

    def __init__(self, rid, name, maximum):
        super(RoomListItem, self).__init__()
        self.name = QStandardItem(name)
        self.rid = QStandardItem(rid)
        self.rid.setEditable(False)
        self.progressbar_people = QProgressBar()
        self.progressbar_people.setFormat(f"%v/{maximum}")
        self.progressbar_people.setMaximum(maximum)
        self.progressbar_people.setMinimum(0)

    def update(self, current_people):
        self.progressbar_people.setValue(current_people)


class RoomManager(Window):
    """
    This window is used to get in rooms manually.
    Only connected accounts appear on the accounts table, selecting an account
    will register a listener to the selected account to show up all the rooms
    that is visible to this account.
    """
    open_chat_signal = pyqtSignal(str)  # uid

    COLUMN_ROOM_NAME = 0
    COLUMN_CURRENT_PEOPLE = 1
    COLUMN_ROOM_ID = 2

    PERSIST_JOIN_INTERVAL = 1000
    PERSIST_JOIN_CHECK_INTERVAL = 500
    PERSIST_JOIN_CHECK_TO_TRY = False

    def __init__(self, main_window, manager, configs, qapp, *args):
        """
        This manager is not supposed to be instantied multiple times.
        :param main_window: main window instance
        :param manager: account manager instance
        :param configs: configuration window
        :param qapp: QApplication
        :param args: anything else
        """
        super(RoomManager, self).__init__(*args, parent=main_window)
        res = utils.get_qt5_resource("room_manager.ui")
        loadUi(res, self)
        self.app = qapp
        self._set_table_headers()
        self.manager = manager
        self.configs = configs
        self.btnGiveUp.setVisible(False)
        self.cbPersist.setVisible(False)
        self.__room_list_data: {int: RoomListItem} = {}  # room id: room data
        selmodel = self.tbRooms.selectionModel()
        selmodel.currentChanged.connect(self.on_tb_rooms_selection_change)
        self.__accounts: {str: QListWidgetItem} = {}
        self._show_userlist_change = False
        self._selected_room = ""
        self._previous_account = ""
        self._current_account = 0
        self.random_nick()
        self.__local_join_request = False  # if not local, it was the bot
        self.__register_listeners_for_all_accounts()

    def _set_table_headers(self):
        items = ["Name", "Current people", "Room id"]
        model = QStandardItemModel()
        self.tbRooms.setModel(model)
        model.setHorizontalHeaderLabels(items)
        self.tbRooms.horizontalHeader().resizeSection(0, 150)
        self.tbRooms.horizontalHeader().resizeSection(1, 250)
        self.tbRooms.verticalHeader().setVisible(False)

    @property
    def persist_join(self):
        return self.cbPersist.isChecked()

    @property
    def max_people(self):
        return self.spMaxPeople.value()

    @max_people.setter
    def max_people(self, value: int):
        self.spMaxPeople.setValue(value)

    @property
    def min_people(self):
        return self.spMinPeople.value()

    @min_people.setter
    def min_people(self, value: int):
        self.spMinPeople.setValue(value)

    @property
    def hide_full_rooms(self) -> bool:
        return self.cbHideFullRooms.checkState() == Qt.Checked

    @hide_full_rooms.setter
    def hide_full_rooms(self, state: bool):
        self.cbHideFullRooms.setCheckState(Qt.Checked if state else Qt.Unchecked)

    @property
    def hide_occupied(self) -> bool:
        return self.cbHideOccupied.checkState() == Qt.Checked

    @hide_occupied.setter
    def hide_occupied(self, state: bool):
        self.cbHideOccupied.setCheckState(Qt.Checked if state else Qt.Unchecked)

    @pyqtSlot(int, name="on_spMaxPeople_valueChanged")
    def on_max_people_value_changed(self, value):
        if value < self.min_people:
            self.min_people = value

    @pyqtSlot(int, name="on_spMinPeople_valueChanged")
    def on_min_people_value_changed(self, value):
        if value > self.max_people:
            self.max_people = value

    @pyqtSlot(QModelIndex, name="on_lstAccounts_pressed")
    def on_lst_accounts_item_clicked(self, idx: QModelIndex):
        item = self.lstAccounts.itemFromIndex(idx)
        self.setup_room_data_stuff(item.text(), self._previous_account)
        self._previous_account = item.text()

    @pyqtSlot(name="on_txtNickname_textChanged")
    def on_txt_nickname_edit(self):
        self.update_buttons()

    @pyqtSlot(name="on_txtNickname_returnPressed")
    def on_txt_nick_return_pressed(self):
        self.random_nick()

    @pyqtSlot(name="on_btnNewColors_clicked")
    def on_btn_random_nick(self):
        self.random_nick()

    def random_nick(self):
        self.txtNickname.setText(utils.random_nick(self.configs.random_nick_type))

    @pyqtSlot(name="on_btnHidden_clicked")
    def on_btn_join_hidden_clicked(self):
        # noinspection PyArgumentList
        dialog = QInputDialog(self)
        # noinspection PyArgumentList
        name, ok = dialog.getText(self, "Join in hidden room", "Room name:")
        if not (ok and name):
            return
        self.join_room(name)

    @pyqtSlot(name="on_btnJoin_pressed")
    def on_btn_join_clicked(self):
        # i hate depending upon these states btw
        chat = self.manager.accounts[self._current_account].chat
        if self._selected_room not in chat.room_list:
            logger.warning(f"error joining in room {self._selected_room}")
            return
        self.join_room(chat.room_list[self._selected_room]['name'])

    def __set_self_enter_listener(self, uid, register):
        params = (Events.self_enter, uid, self._on_self_enter)
        if register:
            self.manager.register_listener(*params)
        else:
            self.manager.remove_listener(*params)

    def __register_listeners_for_all_accounts(self):
        for account_id in self.manager.accounts:
            self.__set_self_enter_listener(account_id, True)

    def update_buttons(self):
        enable = len(self.txtNickname.text()) > 0
        if self._selected_room:
            self.btnJoin.setEnabled(enable)
        if self._current_account in self.__accounts:
            self.btnHidden.setEnabled(enable)

    def join_room(self, room):
        self.__local_join_request = True
        chat = self.manager.accounts[self._current_account].chat
        logger.debug(f"joining room {room}")
        room_enter_error = [Events.room_enter_error, self._current_account,
                            self._on_self_enter_error, True]
        if not self.manager.has_listener(*room_enter_error):
            self.manager.register_listener(*room_enter_error)
        chat.send_room_join_request(room, self.txtNickname.text())

    def on_tb_rooms_selection_change(self, current, _):
        selection = self.tbRooms.model().item(current.row(), self.COLUMN_ROOM_ID)
        if selection:
            self._selected_room = int(selection.text())
            if len(self.txtNickname.text()):
                self.btnJoin.setEnabled(True)

    # Post data onto Qt's event queue, for a thread-safe handling
    def _on_self_enter(self, *data):
        self.app.postEvent(self, events.RoomEnter(*data))
        self.manager.remove_listener(Events.room_enter_error,
                                     self._current_account,
                                     self._on_self_enter_error, True)

    def _on_self_enter_error(self, data):
        self.app.postEvent(self, events.RoomError(data))

    def _on_lobby_update(self, data):
        self.app.postEvent(self, events.RoomUpdate(data))

    def customEvent(self, event):
        if isinstance(event, events.RoomEnter):
            self.process_user_join(event)
        if isinstance(event, events.RoomError):
            self.show_room_enter_error(event.data)
        if isinstance(event, events.RoomUpdate):
            self.update_room_list(event.data)

    def process_user_join(self, event):
        uid, nick, room = event.data
        if self.__local_join_request:
            self.open_chat_signal.emit(uid)
            if self.configs.auto_randomize_nick:
                self.random_nick()
        self.remove_account_from_list(uid)
        self.__local_join_request = False

    def show_room_enter_error(self, reason):
        try:
            reason = JoinResult(reason)
        except ValueError:
            reason = JoinResult.UNKNOWN_ERROR
        r_str = {
            JoinResult.FULL: "Room is full",
            JoinResult.NAME_INVALID: "Invalid nickname",
            JoinResult.NAME_TAKEN: "Nickname taken",
            JoinResult.GAME_NOT_FOUND: "Room not found",
            JoinResult.UPDATE_AVAILABLE: "This software needs to be updated",
            JoinResult.UNKNOWN_ERROR: "Unknown error"
        }
        msgbox = QMessageBox(self)
        logger.debug(f"room_enter_error {reason}")
        try:
            msgbox.warning(msgbox, "Error", r_str[reason], QMessageBox.Ok)
        except KeyError:
            logger.error(f"unhandled room enter error reason: {reason}")
            msgbox.critical(msgbox, f"Error displaying message for {reason}",
                            "error", QMessageBox.Ok)

    def update_room_list(self, room_data: dict):
        """
        adds and updates or removes inexistent items from the room list
        also filters the list
        :param room_data: the dict from chatcode containing the data
        :return: None
        """
        for rid in room_data:
            if rid not in self.__room_list_data:
                self.__add_room_item(rid, room_data)
            self.__room_list_data[rid].update(room_data[rid]['current'])
        for rid in self.__room_list_data:
            if rid not in room_data:
                self.__remove_room_item(rid)
        self.__filter_rooms()

    def __filter_rooms(self):
        occupied_rooms = self.manager.occupied_rooms
        model = self.tbRooms.model()
        for row in range(model.rowCount()):
            item_rid = model.item(row, self.COLUMN_ROOM_ID)
            item_ppl = model.item(row, self.COLUMN_CURRENT_PEOPLE)
            pb = self.tbRooms.indexWidget(item_ppl.index())
            room_id = int(item_rid.text())
            hide = (self.hide_occupied and room_id in occupied_rooms) or (
                    self.hide_full_rooms and pb.maximum() == pb.value()) or (
                           self.max_people < pb.maximum() or
                           self.min_people > pb.value())
            self.tbRooms.setRowHidden(row, hide)

    # custom slot called from outside
    def on_account_disconnect(self, uid):
        self.remove_account_from_list(uid)

    # custom slot called from outside
    def on_account_connect(self, uid):
        if uid in self.__accounts:
            logger.debug(f"trying to add already added account {uid}")
            return
        account = QListWidgetItem(uid)
        self.__accounts[uid] = account
        self.lstAccounts.addItem(account)

    def on_account_added(self, uid):
        self.__set_self_enter_listener(uid, True)

    def on_account_removed(self, uid):
        self.__set_self_enter_listener(uid, False)

    def remove_account_from_list(self, uid):
        """
        Tries to remove an account from the list, either because it is not
        connected anymore or because joined in a room
        :param uid: account id
        :return: None
        """
        uid = str(uid)
        if uid not in self.__accounts:
            return
        listener = (Events.lobby_update, uid, self._on_lobby_update)
        if self.manager.has_listener(*listener):
            self.manager.remove_listener(*listener)
        row = self.lstAccounts.indexFromItem(self.__accounts[uid]).row()
        self.lstAccounts.takeItem(row)
        del self.__accounts[uid]

    def __add_listener(self, uid):
        self.manager.register_listener(Events.lobby_update, uid,
                                       self._on_lobby_update)

    def __del_listener(self, uid):
        self.manager.remove_listener(Events.lobby_update, uid,
                                     self._on_lobby_update)
        model = self.tbRooms.model()
        rows = model.rowCount()
        if rows:
            model.removeRows(0, rows)
        self.__room_list_data.clear()

    def setup_room_data_stuff(self, current_uid, previous_uid):
        """
        register listeners for current selected account on the account list
        and removes the listener from the previous selected account
        :param current_uid: current account id
        :param previous_uid: previous account id
        :return: None
        """
        if previous_uid:
            self.__del_listener(previous_uid)
        self.__add_listener(current_uid)
        self._current_account = current_uid
        self.update_buttons()

    def __add_room_item(self, rid, data):
        item = RoomListItem(str(rid), data[rid]['name'], data[rid]['limit'])
        model = self.tbRooms.model()
        stditem = QStandardItem()
        model.appendRow([item.name, stditem, item.rid])
        index = model.indexFromItem(stditem)
        self.tbRooms.setIndexWidget(index, item.progressbar_people)
        self.__room_list_data[rid] = item
        pass

    def __remove_room_item(self, rid):
        self.tbRooms.model().takeRow(self.__room_list_data[rid].name.row())
