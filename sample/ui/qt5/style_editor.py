from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from sample.misc import utils


class StyleEditor(QDialog):
    def __init__(self, parent, *args):
        super(StyleEditor, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("style_editor.ui")
        loadUi(res, self)

    @pyqtSlot(name="on_btnUpdate_clicked")
    def update(self):
        self.gbExample.setStyleSheet(self.txtStyle.toPlainText())

    @pyqtSlot(name="on_btnsAction_accepted")
    def on_ok(self):
        self.parent().on_style_accept.emit(self.txtStyle.toPlainText())
        self.close()

    @pyqtSlot(name="on_btnsAction_rejected")
    def on_cancel(self):
        self.close()
