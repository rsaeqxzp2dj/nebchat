import glob
import json
import logging
import os

from PyQt5.QtCore import QModelIndex
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QBrush
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QPalette
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QAbstractButton
from PyQt5.QtWidgets import QColorDialog
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QDialogButtonBox
from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtWidgets import QStyleFactory
from PyQt5.uic import loadUi

from sample.misc import utils
from .style_editor import StyleEditor

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ThemeEditor(QDialog):
    DEFAULT = "Default"
    FOLDER_NAME = "themes"
    FILE_EXT = "nbtheme"
    on_style_accept = pyqtSignal(str)

    def __init__(self, parent, *args, theme=None):
        super(ThemeEditor, self).__init__(*args, parent=parent)
        res = utils.get_qt5_resource("theme_editor.ui")
        loadUi(res, self)

        self.__style_editor = StyleEditor(self)
        self._style_str = ""
        self.on_style_accept.connect(self.on_style_editor_accept)
        self._palette = QPalette()

        self.create_tb_palette_content()

        # noinspection PyArgumentList
        self._new_theme_dialog = QInputDialog(self)
        self._new_theme_dialog.setInputMode(QInputDialog.TextInput)

        self.refresh_theme_list()
        self.cbThemeBase.addItems(QStyleFactory().keys()[::-1])

        # avoid setting theme on startup
        self.cbThemeBase.currentTextChanged.connect(self.set_theme_base)

        self.__current_theme = self.DEFAULT

        if theme:
            self.load_theme(theme)
        return

    @property
    def current_theme(self):
        return self.__current_theme

    @staticmethod
    def set_theme_base(string):
        from .mainwindow import MainWindow
        MainWindow.app.setStyle(string)
        return

    @staticmethod
    def set_app_stylesheet(stylesheet):
        from .mainwindow import MainWindow
        MainWindow.app.setStyleSheet(stylesheet)
        return

    @pyqtSlot(str, name="on_cbThemeName_currentIndexChanged")
    def on_index_changed(self, string):
        """
        Fired when a different theme is selected, the theme is applied immediately.
        Note: Fallback theme base is always Fusion.
        :param string: theme name
        :return:
        """
        if string == self.DEFAULT or not string:
            self.scrlControls.setEnabled(False)
            self.set_theme_base("Fusion")
            self._apply_pallete(self.style().standardPalette())
            return
        else:
            self.scrlControls.setEnabled(True)
            self.load_theme(string)
        return

    @pyqtSlot(name="on_btnDeleteTheme_clicked")
    def on_delete(self):
        """
        Delete current theme. Default cannot be deleted
        :return:
        """
        text = self.cbThemeName.currentText()
        index = self.cbThemeName.findText(text)
        if index > -1 and text != "Default":
            if self.delete_theme(text):
                self.cbThemeName.removeItem(index)
                index = self.cbThemeName.currentIndex()
                item = self.cbThemeName.itemText(index)
                self.cbThemeName.setCurrentText(item)
        return

    @pyqtSlot(name="on_btnAdd_clicked")
    def on_add(self):
        """
        Add new theme. Repeated names cannot be added.
        :return:
        """
        # noinspection PyArgumentList
        name, ok = self._new_theme_dialog.getText(self, "New theme", "Theme name:")
        if name not in self.cb_theme_items():
            self.cbThemeName.addItem(name)
            self.save_current_theme(name)
        return

    def cb_theme_items(self):
        """
        Helper method used to list cbThemeName items as list of strings
        :return:
        """
        return [self.cbThemeName.itemText(i) for i in range(self.cbThemeName.count())]

    @pyqtSlot(QAbstractButton, name="on_btnsActions_clicked")
    def on_action_btns(self, btn):
        if self.btnsActions.standardButton(btn) == QDialogButtonBox.Reset:
            self._palette = self.style().standardPalette()
            self._apply_pallete(self._palette)

        elif self.btnsActions.standardButton(btn) == QDialogButtonBox.Save:
            current = self.cbThemeName.currentText()
            if current != self.DEFAULT:
                self.save_current_theme(current)

        elif self.btnsActions.standardButton(btn) == QDialogButtonBox.Apply:
            self._apply_pallete(self._palette)
            if self.cbThemeName.currentText() != self.DEFAULT:
                self.save_current_theme()
        return

    def save_current_theme(self, different_name=None):
        name = different_name if different_name else self.cbThemeName.currentText()
        # there is literally NO WAY to get the current theme base from Qt
        # thats why i'm using cbThemeBase.currentText
        self.save_theme(name, self._palette, self._style_str,
                        self.cbThemeBase.currentText())
        return

    @staticmethod
    def save_theme(theme_name, palette, stylesheet, style_base):
        """
        Saves a theme containig the palette, styleshet and style base.
        :param theme_name: theme name, excluding file extension
        :param palette: QPalette
        :param stylesheet: str containing valid stylesheet (not verified)
        :param style_base: str with style base (e.g.: Fusion)
        :return:
        """
        file = utils.user_data(file=f"{theme_name}.{ThemeEditor.FILE_EXT}",
                               folder=ThemeEditor.FOLDER_NAME)

        pal_attrib = ThemeEditor.pal_attr()
        pal_groups = ThemeEditor.pal_groups()
        data_pal = {}
        for group_name in pal_groups:
            attribs = {}
            for attr_name, attr in zip(pal_attrib, pal_attrib.values()):
                color = QPalette.color(palette, pal_groups[group_name], attr)
                attribs[attr_name] = color.name()
            data_pal[group_name] = attribs

        data = {
            "palette": data_pal,
            "stylesheet": stylesheet,
            "base": style_base
        }
        with open(file, "w") as f:
            json.dump(data, f, indent=2)
        return

    def load_theme(self, name):
        """
        Tries to load and apply theme data by name, NOT by filename.
        :param name: theme name, excluding file extension
        :return:
        """
        if name not in self.get_themes():
            logger.error(f"load_theme: file not found ({name}.{self.FILE_EXT})")
            return
        theme_data = None
        try:
            with open(utils.user_data(f"{name}.{self.FILE_EXT}", self.FOLDER_NAME)) as f:
                theme_data = json.load(f)

        except json.JSONDecodeError:
            logger.error(f"error loading theme {name}: invalid theme data")
        except Exception as e:
            logger.error(f"error loading theme {name}: {e}")

        if not theme_data:
            return
        self.load_palette_data(theme_data['palette'])
        self.load_stylesheet_data(theme_data['stylesheet'])
        self.load_theme_base(theme_data["base"])
        self.__current_theme = name
        self.cbThemeName.setCurrentIndex(self.cbThemeName.findText(name))
        return

    def load_palette_data(self, pal):
        """
        Gets pallete data from json dict, sets each table row color with it
        and applies the palette
        :param pal:
        :return:
        """
        tb_model = self.tbPalette.model()
        columns, rows = {}, {}
        pal_groups = self.pal_groups()
        pal_attribs = self.pal_attr()
        # tbPalette rows and columns are made using pal_groups and pal_attribs
        # so it's safe to use this here
        for i, group in enumerate(pal_groups):
            columns[group] = i
        for i, attrib in enumerate(pal_attribs):
            rows[attrib] = i

        for group_name in pal:
            for attr_name in pal[group_name]:
                color = QColor(pal[group_name][attr_name])
                item = QStandardItem()
                item.setBackground(QBrush(color))
                tb_model.setItem(rows[attr_name], columns[group_name], item)
                self._palette.setColor(pal_groups[group_name],
                                       pal_attribs[attr_name],
                                       color)
        self._apply_pallete(self._palette)
        return

    def load_stylesheet_data(self, stylesheet):
        """
        Load and update stylesheet from __style_editor
        :param stylesheet:
        :return:
        """
        self._style_str = stylesheet
        self.__style_editor.txtStyle.setPlainText(stylesheet)
        self.set_app_stylesheet(stylesheet)
        return

    def load_theme_base(self, theme_base):
        """
        Tries to set current QStyle using theme_base's string
        :param theme_base: Style name
        :return:
        """
        if theme_base not in QStyleFactory().keys():
            logger.warning(f"theme_base {theme_base} cannot be loaded")
        else:
            self.cbThemeBase.setCurrentText(theme_base)
            self.set_theme_base(theme_base)
        return

    @pyqtSlot(name="on_btnStylesheet_clicked")
    def on_btn_stylesheet_clicked(self):
        """
        Load stylesheet onto __style_editor and show up the window
        :return:
        """
        self.__style_editor.txtStyle.setPlainText(self._style_str)
        self.__style_editor.show()
        return

    @pyqtSlot(str, name="on_style_accept")
    def on_style_editor_accept(self, style):
        """
        Called when user clicks on "OK" in __style_editor.
        :param style: str containing the stylesheet
        :return:
        """
        self._style_str = style
        return

    @staticmethod
    def _apply_pallete(pallete):
        from .mainwindow import MainWindow
        MainWindow.app.setPalette(pallete)
        return

    @staticmethod
    def get_themes():
        """
        yields each theme name found
        :return:
        """
        path = utils.user_data(f"*.{ThemeEditor.FILE_EXT}",
                               ThemeEditor.FOLDER_NAME, create_file=False)
        files = glob.glob(path)
        for file in files:
            filename = os.path.split(file)[1]
            filename_no_ext = os.path.splitext(filename)[0]
            yield filename_no_ext

    def refresh_theme_list(self):
        current = self.cbThemeName.currentText()
        self.cbThemeName.clear()
        self.cbThemeName.addItems([self.DEFAULT, *self.get_themes()])
        self.cbThemeName.setCurrentText(current)
        return

    def delete_theme(self, name):
        """
        Tries to delete an existing theme
        :param name: theme name
        :return: True on success, False on fail
        """
        if name not in self.get_themes():
            logger.error(f"cannot delete theme: file not found ({name}.{self.FILE_EXT})")
            return
        try:
            os.remove(utils.user_data(f"{name}.{self.FILE_EXT}", self.FOLDER_NAME))
            return True
        except Exception as e:
            logger.error(f"error deleting theme {name}: {e}")
            return False

    def create_tb_palette_content(self):
        """
        Initializes tbPalette's rows and columns names (setup tableView model)
        :return:
        """
        model = QStandardItemModel(len(self.pal_attr()), len(self.pal_groups()))
        model.setVerticalHeaderLabels(self.pal_attr())
        model.setHorizontalHeaderLabels(self.pal_groups())
        self.tbPalette.setModel(model)
        return

    @pyqtSlot(QModelIndex, name="on_tbPalette_clicked")
    def select_color(self, model_index: QModelIndex):
        """
        Pops-up a color selection dialog used to set current item corresponding
        to the palette's data
        :param model_index:
        :return:
        """
        model = self.tbPalette.model()

        row_txt = model.verticalHeaderItem(model_index.row()).text()
        column_txt = model.horizontalHeaderItem(model_index.column()).text()

        attr, group = self.pal_attr()[row_txt], self.pal_groups()[column_txt]

        # noinspection PyArgumentList
        color = QColorDialog.getColor(initial=QColor(str(model_index.data())),
                                      options=QColorDialog.DontUseNativeDialog)
        if not QColor.isValid(color):
            return  # when user closes the color window

        self._palette.setColor(group, attr, color)
        item = model.itemFromIndex(model_index)
        item.setBackground(QBrush(color))
        return

    @staticmethod
    def pal_attr():
        """
        :return: Dict containing all palette names with corresponding attributes
        """
        return {
            "Window":  QPalette.Window,
            "Window Text": QPalette.WindowText,
            "Base": QPalette.Base,
            "Alternate Base": QPalette.AlternateBase,
            "ToolTip Base": QPalette.ToolTipBase,
            "ToolTip Text": QPalette.ToolTipText,
            "Text": QPalette.Text,
            "Button": QPalette.Button,
            "Button Text": QPalette.ButtonText,
            "Bright Text": QPalette.BrightText,
            "Light": QPalette.Light,
            "Midlight": QPalette.Midlight,
            "Dark": QPalette.Dark,
            "Mid": QPalette.Mid,
            "Shadow": QPalette.Shadow,
            "Highlight": QPalette.Highlight,
            "Highlighted Text": QPalette.HighlightedText,
            "Link": QPalette.Link,
            "Link Visited": QPalette.LinkVisited
        }

    @staticmethod
    def pal_groups():
        """
        :return: Dict containing all palette group names with corresponding types
        """
        return {
            "Disabled": QPalette.Disabled,
            "Active": QPalette.Active,
            "Inactive": QPalette.Inactive
        }
