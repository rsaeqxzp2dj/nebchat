import unittest

from sample.core.networking.account import Account


class AccountApiTest(unittest.TestCase):
    T1 = "17734379,5248228325346591125,DtYAtJWcBwOip14mW/lQnknJYv9c7wlCmEt1Wvrd6lsBHO7VwQgO5krHKCyz0PoCPG" \
         "TxXiTpK19VVCeD4mhCUqh6+b+Ur7ixt1WyjIkB5BTVeQfysDdBp5q1AZdGMXyB/hG6HU5sM3fDHdmLvd2VAzWfUS+PNCi24" \
         "NwMkJHf4xE="

    @classmethod
    def setUpClass(cls):
        cls.account = Account(cls.T1)

    def test_profile_view(self):
        print("testing get profile")
        res = self.account.api_get_player_profile(3592835)
        print(f"result: {res}")
        self.assertIsNotNone(res)

    def test_get_profile_skins(self):
        print("testing get profile skins")
        res = self.account.get_player_skins(5389027)
        print(f"result: {res}")
        self.assertIsNotNone(res)
