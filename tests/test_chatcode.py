import random
import time
import unittest

from sample.misc.utils import random_chars
from sample.core.networking.chatcode import ConnState
from sample.core.networking.chatcode import NebChatcode
from sample.core.networking.game_values import Servers


class NebNetcodeTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.ticket = "17734379,5248228325346591125,DtYAtJWcBwOip14mW/lQnknJYv9c7wlCmEt1Wvrd6lsBHO7VwQgO5krHKCyz0PoCPG" \
                     "TxXiTpK19VVCeD4mhCUqh6+b+Ur7ixt1WyjIkB5BTVeQfysDdBp5q1AZdGMXyB/hG6HU5sM3fDHdmLvd2VAzWfUS+PNCi24" \
                     "NwMkJHf4xE="
        cls.netcode = NebChatcode()

    def test_connect_and_join(self):
        print("testing connection")
        NebChatcode.connection_timeout = 2
        self.netcode.connect(random_chars(), self.ticket, Servers.SERVER_BR)
        time.sleep(4)
        ret = self.netcode.connection_state == ConnState.CONNECTED_AWAITING
        print(f"result: {ret}")
        self.assertTrue(ret)

    def test_join(self):
        print("\ntesting room join\n")
        nick = random_chars()
        print(f"joining in a room with nickname {nick}")
        self.netcode.send_room_join_request(nick=nick)
        time.sleep(2)
        ret = self.netcode.connection_state == ConnState.CONNECTED_INROOM
        self.assertTrue(ret)
        time.sleep(20)
        self.netcode.disconnect(True)


if __name__ == '__main__':
    unittest.main()
