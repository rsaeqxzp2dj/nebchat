import logging
import os
import tempfile
import time
import unittest

from sample.misc import utils
from sample.core.database.database import Database

logging.basicConfig(format='%(asctime)s %(name)s %(funcName)s %(threadName)-s '
                           '%(levelname)s: %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S",
                    handlers=[logging.StreamHandler(),
                              logging.FileHandler(utils.user_data("log_test"),
                                                  mode='w')])


class DatabaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        Database.db_path = os.path.join(tempfile.gettempdir(), "test_database.db")
        cls.db = Database()

    def test_save_message(self):
        print("testing database message saving")

        self.db.save_public_message('jason', 123, 'popcorn', 'yay', time.time())

        ret = self.db.query('user_all_data_raw', 123)
        print(f"result: {ret}")
        self.assertTrue(len(ret) > 0)

    def test_room_events(self):
        print("testing database room events saving")

        self.db.save_room_enter('harry', 789, 'popsicle', int(time.time()))
        self.db.save_room_leave('harry', 789, 'popsicle', int(time.time()) + 10)
        ret = self.db.query('room_users', 'popsicle')
        self.assertIn((789, 'harry'), ret)
        print(f"result: {ret}")

    def test_a_save_user_data(self):
        self.db.save_user_data(123, "yao", "some people are just humans", 0,
                               [111, 5555, 8888], 15554, '', 115, 1531947566)

    @classmethod
    def tearDownClass(cls):
        # os.remove(Database.db_path)
        pass


if __name__ == '__main__':
    unittest.main()
