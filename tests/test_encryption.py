import unittest

from sample.core.database.encryption import MsgEncryptor


class EncryptionTest(unittest.TestCase):

    def setUp(self):
        self.encryptor = MsgEncryptor('password')

    def test_packing(self):
        print("testing encryption")
        data = ('123', 12345, 98765, 'sup', 0)
        packed = self.encryptor.pack_encode_msg(*data)
        unpacked = self.encryptor.unpack_decode_msg(packed)
        print(f"packed data: {packed}")
        print(f"unpacked data: {unpacked}")
        self.assertEqual(unpacked, data)

    def test_key(self):
        print("testing key test")
        key = self.encryptor.key_derivation
        print(f"result: {key}")
        self.assertTrue(self.encryptor.test_password("password", key))
