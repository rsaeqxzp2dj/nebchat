import unittest

from sample.core.msg_processing.filtering import UsersMessages


class FilteringTest(unittest.TestCase):

    def setUp(self):
        self.msgfilter = UsersMessages()
        UsersMessages.time_limit = 200

    def test_basic_filtering(self):
        print("testing basic filtering")
        self.msgfilter.filter(123, "asd")
        self.msgfilter.filter(123, "qwe")
        self.msgfilter.filter(123, "zxc")
        self.msgfilter.filter(123, "ghj")
        self.assertIsNone(self.msgfilter.filter(123, "asd"))

    def test_pool_size(self):
        print("testing msg pool size")
        UsersMessages.limit = 5
        for i in range(10):
            self.msgfilter.filter(123, str(i))
        self.assertEqual(len(self.msgfilter.msgs[123]), 5)

    def test_multiline_filtering(self):
        print("testing multiline filtering")
        self.assertIsNone(self.msgfilter.filter(555, "asd\nasd"))
