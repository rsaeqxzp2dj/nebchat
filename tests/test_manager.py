import logging
import os
import time
import unittest

from sample.core.manager import Events
from sample.core.manager import Manager
from sample.misc.utils import user_data

logger = logging.getLogger(__name__)


class ManagerTest(unittest.TestCase):
    T1 = "17734379,5248228325346591125,DtYAtJWcBwOip14mW/lQnknJYv9c7wlCmEt1Wvrd6lsBHO7VwQgO5krHKCyz0PoCPG" \
         "TxXiTpK19VVCeD4mhCUqh6+b+Ur7ixt1WyjIkB5BTVeQfysDdBp5q1AZdGMXyB/hG6HU5sM3fDHdmLvd2VAzWfUS+PNCi24" \
         "NwMkJHf4xE="

    T2 = "28845480,5248228325346591120,DtYAtJWcBwOip14mW/lQnknJYv9c7wlCmEt1Wvrd6lsBHO7VwQgO5krHKCyz0PoCPG" \
         "TxXiTpK19VVCeD4mhCUqh6+b+Ur7ixt1WyjIkB5BTVeQfysDdBp5q1AZdGMXyB/hG6HU5sM3fDHdmLvd2VAzWfUS+PNCi24" \
         "NwMkJHf4xA="

    @staticmethod
    def remove_files():
        os.remove(user_data(Manager.sessions_filename, create_file=True))

    @classmethod
    def setUpClass(cls):
        Manager.sessions_filename = "session_test.json"
        ManagerTest.remove_files()

    @classmethod
    def tearDownClass(cls):
        pass
        # ManagerTest.remove_files()

    def setUp(self):
        self.manager = Manager()
        self.manager.add_account(ManagerTest.T1, False)
        self.manager.add_account(ManagerTest.T2, False)

    def tearDown(self):
        self.manager.cleanup()
        del self.manager

    def test_test_ticket(self):
        print("testing ticket")
        self.manager.remove_account(self.T1)
        r1 = self.manager.test_ticket(self.T1)
        r2 = self.manager.test_ticket(self.T2)
        print(f"results: \nr1:{r1}r2:{r2}\n")
        self.assertTrue(r1[0] and not r2[0])

    def test_c_set_default_session(self):
        print("testing set default session")
        self.manager.save_session("0")
        self.manager.save_session("1")
        self.manager.set_default_session("0")
        self.manager.delete_session("0")
        print(f"result: {self.manager.default_session}")
        self.assertEqual(self.manager.default_session, "1")

    def test_d_delete_session(self):
        print("testing session deletion")
        self.manager.save_session("0")
        self.manager.save_session("1")
        self.manager.delete_session("0")
        print(f"result: {self.manager.sessions}")
        self.assertEqual(len(self.manager.sessions), 1)

    @staticmethod
    def worked(var, *more_vars):
        print(*more_vars, sep='|')
        var[0] = True

    def test_e_listeners(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)s %(threadName)-s %(levelname)s: %(message)s',
                            datefmt="%Y-%m-%d %H:%M:%S")
        print("testing listeners")
        var = [False]
        self.manager.register_listener(Events.user_join,
                                       '17734379',
                                       lambda u, n, r: self.worked(var, u, n, r))
        self.manager.accounts['17734379'].connect()
        time.sleep(2)
        self.manager.accounts['17734379'].chat.send_room_join_request()
        time.sleep(4)
        self.manager.accounts['17734379'].chat.disconnect()
        self.assertTrue(var[0])

    def test_f_save_load(self):
        print("testing save and load")
        self.manager.save_session("0")
        print(self.manager.sessions)
        self.manager.save_session("1")
        self.manager.load_session("0")
        print(f"result: {self.manager.accounts}")
        self.assertTrue(self.manager.accounts)
