import unittest
from time import sleep

from sample.core.networking.chatcode import Statistics


class StatisticsTest(unittest.TestCase):
    def setUp(self):
        self.stats = Statistics()

    def test_msg_per_second(self):
        msgs = 10
        secs = 0.2
        print(f"testing {msgs} messages every {secs} seconds ({1/secs} msg per sec)")
        for i in range(msgs):
            sleep(secs)
            self.stats.update_statistics(True)
        print(f"result: {self.stats.avg_msgs_per_second}")
        self.assertAlmostEqual(self.stats.avg_msgs_per_second, 1/secs, places=0)

    def test_inactivity(self):
        seconds = 1
        print(f"testing inactivity for {seconds} seconds")
        self.stats.update_inactivity_time()
        sleep(seconds)
        self.stats.update_inactivity_time()
        print(f"result: {self.stats.inactivity_time}")
        self.assertAlmostEqual(self.stats.inactivity_time, seconds, places=2)

    def test_inactivity_reset(self):
        self.stats.update_inactivity_time(True)
        self.assertAlmostEqual(self.stats.inactivity_time, 0, places=2)

    def test_uptime(self):
        seconds = 5
        print(f"testing uptime for {seconds} seconds")
        self.stats.update_uptime(True)
        sleep(seconds)
        self.stats.update_uptime()
        print(f"result: {self.stats.uptime}")
        self.assertAlmostEqual(self.stats.uptime, seconds, places=1)


if __name__ == '__main__':
    unittest.main()
